# This readme will be used to keep track of progress in the form of a changelog
# Created June 4, 2013 9:18 AM
# Created by Athikom Vorapattanapong


4/6/2013
 -Completed setting up environment for primary laptop
 -Connected to local database
 -Changed Files:
 	1. application/config/database.php
 		-changed login data for local database
 -Ongoing Issue:
 	1. Pages are displaying with error: some files not found (background files)"RESOLVED"
 -Hours:
 	3 hours

7/6/2013
 -Researched more on "Codeigniter" framework at www.youtube.com/phpacademy
 -Reviewed PHP
 -Changed Files:
 	none
 -Ongoing Issue:
 	1. Pages are displaying with error: some files not found (background files) 
 -Hours:
 	5 hours
 	
 8/6/2013
 -Successfully setup environment on secondary machine
 -Connected to databse
 -More research in framework and PHP
 -Fixed ongoing issue: base URL was not properly set in application/config/config.php
 	*localhost:8888/ivplc to localhost/ivplc
 -Changed Files:
	application/config/config.php
 -Ongoing Issue:
 	1. Pages are displaying with error: some files not found (background files) "RESOLVED"
 -Hours:
 	2 hours
 	
 9/6/2013
 -Created a new branch to work on
 -Created an edit page
 -Changed Files:
    1. Added application/controllers/edit.php
    2. Added views/edit folder
    3. Added views/edit/index.php
 -Ongoing Issue
    1. Finish up "edit submission" page
 -Hours:
    1. 2 hours
    
11/6/2013
 -Play around with database, code, website
 -Try to understand the architecture of the website and data structure
 -Changed Files:
 	1. None but some images and other files added to database
 -Ongoing Issue
 	1. Finish up "edit submission" page
 -Hours
 	1. 2 Hours
 	
12/6/2013
 -More php and codeigniter researches
 -Added some records to db to track relationships
 -Changed Files:
 	1. Added some files to db (noise files, car pics, etc)
 -Ongoing Issue
	1. Finish up "edit submission" page
 -Hours:
 	1. 1 Hour
 	
18/6/2013
 -Login page uses default password with md5 encryption before storing into database
 -Changed Files:
 	1. Database added another supervisor account for testing
 -Ongoing Issue:
 	1. Finish up "edit submission" page
 	2. "Wrong password" page
 -Hours:
 	1. 0.5 hour
 	
 1/7/2013
  -Created edit page with some user info retrieved from database
  -Forms to enter new values will be created next
  -Changed Files:
  	1. models/model_edit
  	2. views/edit_userinfo
  	3. views/index.php
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. "Wrong password" page
  -Hours:
  	1. 6 hours
  	
 2/7/2013
  -Created forms for userinfo input and attempted to update database using these forms
  -Changed Files:
  	1. models/model_edit
  	2. views/edit_userinfo (not used anymore)
  	3. views/index.php
  	4. added: views/index_success
  	5. added: views/view_database_error_update
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. "Wrong password" page
  -Hours:
  	1. 5 hours
  	
 3/7/2013
  -Database can now be successfully updated with the forms
  -Successful page loads successfully after database is updated
  -Changed Files
	1. models/model_edit
  	2. views/edit_userinfo (not used anymore)
  	3. views/index.php
  	4. added: views/edit/index_success
  	5. added: views/edit/view_database_error_update
  	6. controllers/edit
  	7. config/config/autoload (autoloading additional library: form_validation)
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. "Wrong password" page
 	3. Stlying edit submission page
 	4. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.
 	5. Reorganizing the flow of webpages
 	6. Login feature for contributors 
  -Hours:
  	1. 5.5 Hours
  	
4/7/2013
  -Introduced a new method of login
  -Members, previously known as "contributors", are now required to login to submit files or edit
  -Members and admins login from the same page
  -Changed Files
	1. models/group
  	2. views/external/view_login.php
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. "Wrong password" page	"RESOLVED"
 	3. Stlying edit submission page
 	4. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.
 	5. Reorganizing the flow of webpages
 	6. Login feature for contributors 
  -Hours:
  	1. 3 Hours
  	
5/7/2013
  -New login method working
  -Changed Files
	1. added: models/model_login_authentication
  	2. added: views/edit/view_main_profile
  	3. added: views/members/index
  	4. added: controllers/ctrl_members
  	5. controllers/external
  	6. model/model_edit
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. Stlying edit submission page
 	3. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.
 	4. Reorganizing the flow of webpages
 	5. Login feature for contributors 
  -Hours:
  	1. 6 Hours
  	
8/7/2013
  -Polish up Login method
  -Restricted page redirect for unauthorized access(direct URL access)
  -Nav now shows logged in as name and logout button
  -Changed Files
	1. models/model_login_authentication
  	2. views/members/index
  	3. controllers/ctrl_members
  	4. views/nav
  	5. cofig/routes
  	6. added:views/members/restricted
  -Ongoing Issue:
  	1. Finish up "edit submission" page
 	2. Stlying edit submission page
 	3. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.
 	4. Reorganizing the flow of webpages (partially complete)
 	5. Login feature for contributors "RESOLVED"
  -Hours:
  	1. 5 hours
  	
9/7/2013
  -Cleanup nav bar and make highlighting properly executed
  -Nav bar now have 2 sets, one for external users and one for members
    *One for users now have a link to profile and a logout function
  -submission link now redirects to login/register page, users need to login before submitting
  -Custom home page for members
  -Changed Files:
   	1. config/routes
   	2. controllers/ctrl_members
   	3. controllers/submission
   	4. controllers/external
   	5. views/edit/view_main_profile
   	6. views/nav
   	7. views/submissions/chooser
   	8. views/external/view_login_error
   	9. views/members/index
  -Ongoing Issue:
    1. Finish up "edit submission" page
 	2. Stlying edit submission page
 	3. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.
 	4. Reorganizing the flow of webpages (partially complete)
 	5. Submission controlller needs to be modified
 	6. Need to create another "chooser" for editing page
  -Hours:
  	1. 4.5 Hours
   	
10/07/2013
  -Resolved updating database problem (see "Ongoing Issue" section)
  -Profile section on nav bar now leads to edit section starting with personal info
  -Mini nav bar in edit profile section contains different editable sections(personal info, vehicle, publication)
  -Removed Chooser for edit page
  -Changed Files:
    1. controller/ctrl_members
    2. removed:views/edit/view_edit_chooser
    3. config/routes
    4. views/nav
    5. added: view/edit/view_edit_nav
  -Ongoing Issue
    1. Finish up "edit submission" page
 	2. Stlying edit submission page
 	3. Blank form field updates the database to blank fields
 		*Initial Fix: form values are taken from database so users can choose which field to change without
 			having to enter values into every field.("RESOLVED")
 	4. Reorganizing the flow of webpages (partially complete)
 	5. Submission controlller needs to be modified
 	6. Need to create another "chooser" for editing page ("DISCARDED")
  -Hours:
  	1. 4 hours
  	
*Note: multiple dates are compiled together because most of the contents are similar
  	
11/07/2013-15/07/2013
  -Partially Completed Vehicle Edit Page
  	*Completed multiple delete files
  -Resolved various problems with $_POST and $_GET for view_edit_vehicle
  -Deleting now works with vehicle
  -Page updates with removal of deleted files
  -Changed Files:
    1. Added: view_vehicle_list
    2. config/routes
    3. controllers/submission
    4. views/edit/view_edit_nav
    5. controller/edit
    6. Added:view_edit_vehicle
  -Ongoing Issue
    1. Finish up "edit submission" page
 	2. Stlying edit submission page
 	3. Reorganizing the flow of webpages (partially complete)
 	4. Submission controlller needs to be modified
 	5. Cleanup Code for performance and readablility
  -Hours:
    1. 20 Hours
    
17/07/2013-20/07/2013
  -Completed Publications
  -Completed Registration Page
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Admin Level Pages
  	2. Verification of edited contents
  	3. Component edit page javascript issue still unsolved
  	4. Styling
  	5. Code Cleanup
  -Hours:
  	1. 20 hours
  	
24/07/2013-28/07/2013
  -Completed Measurements
  -All basic website functionality is working
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Admin Level Pages
  	2. Verification of edited contents
  	3. Component edit page javascript issue still unsolved - "Reworked"
  	4. Styling
  	5. Code Cleanup
  	6. Replace contents
  -Hours:
  	1. 20 hours

29/07/2013
  -Completed basic admin integration with new system
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Admin Level Pages - "RESOLVED"
  	2. Verification of edited contents
  	3. Styling
  	4. Code Cleanup - started
  	5. Replace contents
  -Hours:
  	1. 6 hours
  	
30/07/2013-1/08/2013
  -All main features are implemented 
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Verification of edited contents - "RESOLVED"
  	2. Styling
  	3. Code Cleanup - started
  	4. Replace contents
  -Hours:
  	1. 20 hours
  	
3/08/2013-5/08/2013
  -Finish testing and fixing all major bugs
  -Will start styling with twitter bootstrap next
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Styling
  	2. Code Cleanup - started
  	3. Replace contents
  -Hours:
  	1. 24 hours
  	
6/08/2013-9/08/2013
  -Styling Completed
  -Most Code Cleanup is finished
  -Changed Files:
  	1. Check Git Repository
  -Ongoing Issue
  	1. Styling "RESOLVED"
  	2. Code Cleanup - started
  	3. Replace contents "Canceled"
  -Hours:
  	1. 24 hours
  	
Notes:
1. Submission dates change if user edit url? [PUBLICATIONS]
2. Moving over user data may cause errors
	-Current known issues
		1. readme file not available in old users - must do it through database
3. verified + view field in vehicle table (renamed mispelling) [DONE]
4. Adding vehicles set their verification to 0 but all other components will be 1 
	so approving a vehicle shows the vehicle [DONE]
5. Need to add checks before displayin any image, author, components, measurement, readme, publications [DONE]
6. On display non-verified will not show [DONE]
7. On edit non-verified will have a tag [DONE]
8. On display readmes will show "not approved" on non-verified, on edit as in 7 [DONE]
9. Add functions to return non-verified item [DONE]
10. Verify Users? [DONE]
11. Rejected Edits[DONE]
12. rejected deletes for image must delete from folder[DONE]
13. vehicle not approve gets rejected but can never be edited, must resubmit (but can be reapproved by admin)
    same with pubs [DONE]
14. Cascade vehicle, components, measurements, image, publication, author on delete[DONE]
15. handle session timeout [CANCELED]
16. handle assistant login [DONE] - note: non supervisor group members cant login
17. handle readme file replacement [DONE]



	<!DOCTYPE html>
<html>
<div class="content">
	
	<h2 class="head_title"> Choose a vehicle to edit</h2>
	<div>
		<?php echo $msg;?>
		<form action="list_vehicle" id="deleteForm" method="post">
			<ul>	
				<?php 
					if(!empty($cars)){
						foreach($cars as $field){ ?>
							<?php if($field['view'] == 1 && $field['reject'] == 0):?>
							<div class= "no_verify">
								<img class="hero" src="<?=base_url() . $field['images'][0]['url'];?>"/>
								<?php  
								echo br(1);
								echo form_checkbox('del_vehicle[]', $field['pk_vehicle_id']);
								echo anchor('edit/edit_vehicle?id='.$field['pk_vehicle_id'],$field['manufacturer'].' '.$field['model'].'<br>'.$field['year'],'id=button');
								echo '<br>';
								?>
							</div>
							<?php elseif($field['view'] == 0 && $field['reject'] == 0):?>
								<div class="no_reject">
									<?php
									echo img($field['images'][0]['url'],$image_prop=array('width'=>'300', 'height'=> '300'));
									echo br(1);
									echo form_checkbox('del_vehicle[]', $field['pk_vehicle_id']);
									echo $field['manufacturer'].' '.$field['model'].'<br>'.$field['year'];
									echo '<br>';
									?>
									<p class="under_verification"> Vehicle still under verification.</p>
								</div>
							<?php elseif($field['view'] == 0 && $field['reject'] == 1):?>
								<div class="reject">
									<?php
									echo img($field['images'][0]['url'],$image_prop=array('width'=>'150', 'height'=> '150'));
									echo br(1);
									echo form_checkbox('del_vehicle[]', $field['pk_vehicle_id']);
									echo $field['manufacturer'].' '.$field['model'].'<br>'.$field['year'];
									echo '<br>';
									?>
									<p class="under_verification"> Vehicle rejected. Please submit another vehicle.</p>
								</div>
							<?php endif; ?>
							
				<?php		}
						echo form_submit('submit', 'Delete Selected');
					}
				?>
			</ul>
		</form>
	</div>

</div>
</html>

