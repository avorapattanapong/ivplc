<div class="content" id="measurements_index">

	<h2 class="title" id="measurement_title">Measurements</h2>

	<?php if(isset($manufacturers) && $manufacturers != '') : foreach($manufacturers as $manufacturer) : ?>
		<h4 class="manufacturer"><?=$manufacturer;?></h4>

		<?php foreach($vehicles as $vehicle) : ?>	
			<?php if($vehicle['view'] == 1 && $vehicle['verified'] == 1 && $vehicle['reject'] == 0): ?>	
				<?php if($vehicle['manufacturer'] == $manufacturer) : ?>
					<a class="vehicles ext" href="<?=site_url();?>measurements/<?=$vehicle['pk_vehicle_id'];?>">
						<div class="mask">
							<?php if($vehicle['images'][0] != '') : ?>
								<img class="hero" src="<?=base_url() . $vehicle['images'][0]['url'];?>"/>
							<?php else : ?>
								<img class="hero" src="<?=base_url();?>resources/styles/images/car.png"/>
							<?php endif; ?>
						</div>
						
						<p class="model"><?=$vehicle['manufacturer'] . ' ' . $vehicle['model'];?></p>
						<p class="year"><?=$vehicle['year'];?></p>
					</a>
				<?php endif;?>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; endif; ?>
	
</div>