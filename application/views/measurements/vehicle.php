<div class="content" id="measurements_vehicle">

<?php if(isset($vehicle) && $vehicle != '') : ?>	
	<!-- Vehicle information -->

	<h2 class="title">
		<div class="row">
			<div class="col-md-4">
				<?=$vehicle['manufacturer'] . ' ' . $vehicle['model'] . '&mdash;' . $vehicle['year'];?>
			</div>
			<div class="col-md-4 col-md-offset-8">
				<div class="contri_name">
				Submitted By: <?php echo
					$vehicle['contributor'][0]['first_name'] . ' ' .
					$vehicle['contributor'][0]['last_name'] . ', ' .
					$vehicle['contributor'][0]['affiliation'] . ', ' .
					$vehicle['contributor'][0]['country'];
				?>
				</div>
			</div>
		</div>
	</h2>

	<!-- Update button -->

	<!-- Photo Gallery -->
	<div id="slider" class="nivoSlider">
		<?php foreach($vehicle['images'] as $image) : ?>
			<img src="<?=base_url() . $image['url'];?>" width="100%" height="300px" />
		<?php endforeach;?>
	</div>
		<p class="note">Please click the link below to download a readme file with instructions for the files.</p>
		
		<?php echo anchor($readme['url'], 'Vehicle Description File',array( 'id' => 'vehicle_description'))?>
		
	<!-- Vehicle measurements -->
	<p class="note">Click on a component name to download the associated noise or transfer file.</p>
	
	<div class="data">
		<?php foreach($vehicle['components'] as $component) : ?>
			<?php if($component['verified'] != 0 && $component['reject'] != 1):?>
			<div class="component">
				<?php if($component['url'] == NULL) : ?>
					<h5><?=$component['name'];?></h5>
				<?php else : ?>			
					<h5><a href="<?=base_url() . substr($component['url'],1);?>"><?=$component['name'];?></a></h5>
				<?php endif;?>
				
				<ul class="measurements">
					<?php if(isset($vehicle['measurements'])) : foreach($vehicle['measurements'] as $measurement) : ?>
						<?php if( $measurement['verified'] != 0 && $measurement['reject'] != 1):?>
						<?php if($measurement['fk_componentA_id'] == $component['pk_component_id']) : ?>
							<?php if($measurement['url'] != NULL) :?>
							<li >
								
								<a  href="<?=base_url() . substr($measurement['url'],1);?>"><?=$vehicle['components'][$measurement['fk_componentB_id']]['name'];?></a>
							</li>
							<?php endif; ?>
						<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; endif; ?>
				</ul>
			</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
<?php else : ?>

	<p>Vehicle doesn't exist. <a href="<?=site_url();?>measurements">Back</a></p>

<?php endif; ?>
	
</div>