
<div class="container-fluid" id="header">
	<a href="<?=base_url();?>" id="logo">In-Vehicle Power Line Communication Group</a>
	
	<!-- Navigations Links -->
	<ul id="nav">
		<?php 
			if($this->session->userdata('isAdmin') == 1){
				$aName = $this->session->userdata('name');
			}
			else {
				$fname = $this->session->userdata('fname');
				$lname = $this->session->userdata('lname');
			}
			?>
		<?php if($this->session->userdata('is_logged_in') == 1): //if logged in ?>
			<li class="<?=($parent == 'members home') ? 'active' : '';?>"><a href="<?=site_url();?>members">Home</a></li>
			<li class="<?=($parent == 'group') ? 'active' : '';?>"><a href="<?=site_url();?>group">Group</a></li>
			<li class="<?=($parent == 'publications') ? 'active' : '';?>"><a href="<?=site_url();?>publications">Publications</a></li>
			<li class="<?=($parent == 'measurements') ? 'active' : '';?>"><a href="<?=site_url();?>measurements">Measurements</a></li>
			<li class="<?=($parent == 'submit') ? 'active' : '';?>"><a href="<?=site_url();?>submit">Submit</a></li>
			<?php if($this->session->userdata('isAdmin') == 1):?>
				<li class="<?=($parent == 'edit') ? 'active' : '';?>"><a href="<?=site_url();?>edit">
					Profile: <?php print_r( $aName );?></a></li>
				<?php if($this->session->userdata('isSuper') == 1):?>
				<li class="<?=($parent == 'admin') ? 'active' : '';?>"><a href="<?=site_url();?>admin">Admin</a></li>
				<?php endif;?>
			<?php else: ?>
				<li class="<?=($parent == 'edit') ? 'active' : '';?>"><a href="<?=site_url();?>edit">
					Profile: <?php print_r( $fname." ".$lname);?></a></li>
					<!-- link to your own profile for editing-->
			<?php endif; ?>
			
			<li class="<?=($parent == '') ? 'active' : '';?>"><a href="<?=site_url();?>members/logout">Logout</a></li>
			
		<?php else://if not ?>
			<li class="<?=($parent == 'home') ? 'active' : '';?>"><a href="<?=site_url();?>">Home</a></li>
			<li class="<?=($parent == 'group') ? 'active' : '';?>"><a href="<?=site_url();?>group">Group</a></li>
			<li class="<?=($parent == 'publications') ? 'active' : '';?>"><a href="<?=site_url();?>publications">Publications</a></li>
			<li class="<?=($parent == 'measurements') ? 'active' : '';?>"><a href="<?=site_url();?>measurements">Measurements</a></li>
			<li class="<?=($parent == 'submit') ? 'active' : '';?>"><a href="<?=site_url();?>submit">Submit</a></li>
			<li class="<?=($parent == 'login') ? 'active' : '';?>"><a href="<?=site_url();?>login">Login</a></li>
		<?php endif;?>
	
	</ul>
</div>
