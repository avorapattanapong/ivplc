<!-- IVPLC 01/12 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xmhtml" xml:lang="en" lang="en">
<head>
	<meta name="copyright" content="UBC © 2012" />
	<meta name="description" content="The In-Vehicle Power Line Communications research group of UBC."/>
	<meta name="keywords" content="IVPLC, VPLC, ubc research, in-vehicle power line communications, power line communications, lutz lampe, roberto rosales" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/style.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/external.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/measurements.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/submissions.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/admin.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/booststrap-responsive.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/booststrap-responsive.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url();?>resources/styles/booststrap-glyphicons.css" />
	

	<script type="text/javascript" src="<?=base_url();?>resources/scripts/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url();?>resources/scripts/jquery.ui.js"></script>
	<script type="text/javascript" src="<?=base_url();?>resources/scripts/jquery.nivo.js"></script>
	<script type="text/javascript" src="<?=base_url();?>resources/scripts/bootstrap.js"></script>
	<script type="text/javascript" src="<?=base_url();?>resources/scripts/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>resources/scripts/script.js"></script>
	


	<title>In-Vehicle Power Line Communication - <?=$title;?></title>
</head>

<body class="container">