<div class="content" id="admin-group">




	<h2 class="title" id="title">Log Table</h2>
	
	<div class="container">
		<!-- SEARCH CONTROLS -->
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
	    				<h3 class="panel-title">Controls</h3>
	  				</div>
  					<div class="panel-body">
    					<div class="form-group">
    						<p class="help-block">Instructions</p>
    					</div>
    					<?php echo form_open('administrator/admin_log_table');?>

							<div class="container">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<?php echo form_label('Sort by', 'sort_list');?>
											<select class="form-control" name="sort_list">
									  			<option value="date">By Date</option>
									  			<option value="users">By Users</option>
									  			<option value="actions">By Actions</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
					  						<?php echo form_label('Number of results per page', 'num_results');?>
											<?php echo form_input(array('name'=>'num_results', 'placeholder'=>'# of results', 'class'=>'form-control'))?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php echo form_label('Select sorting methods', 'sort_method');?>
										<div class="radio">
		  									<label>
		    									<input type="radio" name="sort_method" id="optionsRadios1" value="asc" checked>
		    									Ascending
		  									</label>
										</div>
										<div class="radio">
											<label>
										  		<input type="radio" name="sort_method" id="optionsRadios2" value="dsc">
										    	Descending
										  	</label>
										</div>
									</div>
								</div>
								<div class="row">
									<?php echo form_submit(array('name'=>'submit', 'value'=>'Apply Filter', 'class'=>'btn btn-default pull-right')); ?>
								</div>
							</div>
						
  					</div>
				</div>
			</div>
		</div>
		<!-- END OF SEARCH CONTROLS-->
		
		<!-- LOGS  -->
		<div class="row">
		
		
	
		
		<h3>Activity Logs <?php echo form_submit(array('name'=>'submit', 'value'=>'Delete All Entries', 'class'=>'btn btn-danger btn-sm')); ?></h3> 
	<?php if(isset($log_table_default) && !empty($log_table_default)) : ?>	
	
		<table id="admin">
			<thead><tr>
				<th>#</th>
				<th>User</th>
				<th>Details</th>
				<th>Date/Time</th>
				<td>Options</td>
			</tr></thead>
			
			<?php $num = 1; ?>
			<tbody>		
				<?php foreach($log_table_default as $log_entry) : ?>
					<tr>
						<td>
							<!-- Entry Number-->
							<?php echo $num; ?>
							<?php $num++; ?>
						</td>
						<td>.
							<!-- User Name-->
							<?php $user_array = $this->contributors->return_contributor($log_entry['fk_contributor_id']); ?>
							<?php foreach($user_array as $user): ?>
								<?php echo $user['first_name'].' '.$user['last_name']; ?>
							<?php endforeach; ?>
						</td>
						<td>
							<!-- Activity -->
							<!-- User Creation -->
							<?php if($log_entry['activity_type'] == 'USER_CREATE'): ?>
								<?php foreach($user_array as $user): ?>
									<?php $email = $user['email']; ?>
								<?php endforeach; ?>
								A new Contributor account is created: <?php echo ' '.mailto($email,$email);?>
							
							<!-- New Vehicle or Updated vehicle info -->
							<?php elseif($log_entry['activity_type'] == 'VEHICLE_SUBMITTED' || $log_entry['activity_type'] == 'UPDATE_VEHICLE_INFO'): ?>
								<?php if($log_entry['source_table'] == 'vehicles'): ?>
									<?php $vehicle = $this->vehicles->return_vehicles($log_entry['fk_source_id']); ?>
									<?php foreach($vehicle as $sub_v): ?>
										<?php $v_model = $sub_v['model']; ?>
										<?php $v_year = $sub_v['year']; ?>
										<?php $v_manu = $sub_v['manufacturer']; ?>
									<?php endforeach; ?>
									<?php if($log_entry['activity_type'] == 'VEHICLE_SUBMITTED'): ?>
										User submitted a new vehicle:<?php echo ' '.$v_year.' '.$v_manu.' '.$v_model; ?>
									<?php elseif($log_entry['activity_type'] == 'UPDATE_VEHICLE_INFO'): ?>
										User updated the details of the vehicle:<?php echo ' '.$v_year.' '.$v_manu.' '.$v_model; ?>
									<?php endif; ?>
								<?php else: ?>
									<?php echo 'Error: wrong source table'; ?>
								<?php endif; ?>
								
							<!-- New Publication or Updated Publication Info -->
							<?php elseif($log_entry['activity_type'] == 'PUB_SUBMITTED' || $log_entry['activity_type'] == 'UPDATE_PUB_INFO'): ?>
								<?php if($log_entry['source_table'] == 'publications'): ?>
									<?php $publication = $this->publications->return_publications_withID($log_entry['fk_source_id']); ?>
									<?php foreach($publication as $pub): ?>
										<?php $pub_title = $pub['title']; ?>
										<?php $pub_year = $pub['date']; ?>
										<?php $pub_url = prep_url($pub['url']); ?>
									<?php endforeach; ?>
									<?php if($log_entry['activity_type'] == 'PUB_SUBMITTED'): ?>
										User submitted a new publication: <?php echo ' '.anchor_popup($pub_url, $pub_title).' ('.$pub_year.') '; ?>
									<?php elseif($log_entry['activity_type'] == 'UPDATE_PUB_INFO'): ?>
										User updated the details of the publication: <?php echo ' '.anchor_popup($pub_url, $pub_title).' ('.$pub_year.') '; ?>
									<?php endif; ?>
								<?php else: ?>
									<?php echo 'Error: wrong source table'; ?>
								<?php endif; ?>
								
							<!-- Content Replaced, Added, Deleted -->
							<?php elseif($log_entry['activity_type'] == 'REPLACED' || $log_entry['activity_type'] == 'ADDED' || $log_entry['activity_type'] == 'DELETED'): ?>
								<!-- First pull the right data set -->
								<?php if($log_entry['source_table'] == 'images'): ?>
									<?php $item = $this->images->return_image($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/car_images/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'components'): ?>
									<?php $item = $this->components->return_component($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/noise_files/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'measurements'): ?>
									<?php $item = $this->measures->return_measurement($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/transfer_functions/', '',$item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'vehicle_description'): ?>
									<?php $item = $this->model_readme->return_readme_by_id($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/readme_files/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php else: ?>
									<?php $item = "Error"; ?>
								<?php endif; ?>
								
								<!-- Then output the info in the right category -->
								<?php if($item != "Error"): ?>
									<?php foreach($item as $log_item): ?>
										<?php if($log_entry['activity_type'] == 'REPLACED'): ?>
											<?php if($log_entry['source_table'] == 'components'): ?>
												User has replaced the noise file: <?php echo ' '.anchor(base_url() . substr($log_item['url'],1), $item_name).' ';?> in <?php echo ' '.$log_item['name'].' ';?> component of the vehicle with an updated version.
											<?php else: ?>
												User has replaced the file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> with an updated version.
											<?php endif; ?>
										<?php elseif($log_entry['activity_type'] == 'ADDED'): ?>
											<?php if($log_entry['source_table'] == 'components'): ?>
												User has added the noise file: <?php echo ' '.anchor(base_url() . substr($log_item['url'],1), $item_name).' ';?> to <?php echo ' '.$log_item['name'].' '?> component of the vehicle.
											<?php else: ?>
												User has added the file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> to <?php echo  ' '.$log_entry['source_table'].' ';?>.
											<?php endif; ?>
										<?php elseif($log_entry['activity_type'] == 'DELETED'): ?>
											<?php if($log_entry['source_table'] == 'components'): ?>
												User has deleted the noise file: <?php echo ' '.$item_name.' ';?> from <?php echo ' '.$log_item['name'].' ';?> component of the vehicle.
											<?php else: ?>
												User has added the file: <?php echo ' '.$item_name.' ';?> from <?php echo ' '.$log_entry['source_table'].' ';?>.
											<?php endif; ?>
										<?php endif; ?>	
									<?php endforeach; ?>
								<?php else: ?>
									Error: source table does not exist.
								<?php endif; ?>
								
							<!-- Update account info for user and admin-->
							<?php elseif($log_entry['activity_type'] == 'UPDATE_USER_INFO'): ?>
								<?php if($log_entry['source_table'] == 'contributors'): ?>
									User has updated his/her profile details.
								<?php else: ?>
									Error: wrong source table.
								<?php endif; ?>
							
							<?php elseif($log_entry['activity_type'] == 'UPDATE_ADMIN_INFO'): ?>
								<?php if($log_entry['source_table'] == 'group'): ?>
									Supervisor has updated his/her profile details.
								<?php else: ?>
									Error: wrong source table.
								<?php endif; ?>
							
							<!-- Reject, Approved or Delete by the Admin-->
							<?php elseif($log_entry['activity_type'] == 'ADMIN_APPROVE' || $log_entry['activity_type'] == 'ADMIN_REJECT'|| $log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
								<?php $item = "okay"; ?>
								<?php if($log_entry['source_table'] == 'vehicles'): ?>
									<?php $vehicle = $this->vehicles->return_vehicles($log_entry['fk_source_id']); ?>
									<?php foreach($vehicle as $sub_v): ?>
										<?php $v_model = $sub_v['model']; ?>
										<?php $v_year = $sub_v['year']; ?>
										<?php $v_manu = $sub_v['manufacturer']; ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'publications'): ?>
									<?php $publication = $this->publications->return_publications_withID($log_entry['fk_source_id']); ?>
									<?php foreach($publication as $pub): ?>
										<?php $pub_title = $pub['title']; ?>
										<?php $pub_year = $pub['date']; ?>
										<?php $pub_url = prep_url($pub['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'images'): ?>
									<?php $item = $this->images->return_image($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/car_images/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'components'): ?>
									<?php $item = $this->components->return_component($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/noise_files/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'measurements'): ?>
									<?php $item = $this->measures->return_measurement($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/transfer_functions/', '',$item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php elseif($log_entry['source_table'] == 'vehicle_description'): ?>
									<?php $item = $this->model_readme->return_readme_by_id($log_entry['fk_source_id']); ?>
									<?php foreach($item as $item_get_name): ?>
									<?php $item_name = str_replace('/uploads/readme_files/', '', $item_get_name['url']); ?>
									<?php endforeach; ?>
								<?php else: ?>
									<?php $item = "Error"; ?>
								<?php endif; ?>
								
								<?php if($item != "Error") : ?>
									<?php foreach($item as $log_item): ?>
										<?php if($log_entry['source_table'] == 'vehicles'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the publication: <?php echo ' '.$v_year.' '.$v_manu.' '.$v_model; ?>
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the publication: <?php echo ' '.$v_year.' '.$v_manu.' '.$v_model; ?>
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the publication: <?php echo ' '.$v_year.' '.$v_manu.' '.$v_model; ?>
											<?php endif; ?>
										<?php elseif($log_entry['source_table'] == 'publications'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the publication: <?php echo ' '.anchor_popup($pub_url, $pub_title).' ('.$pub_year.') '; ?>
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the publication: <?php echo ' '.anchor_popup($pub_url, $pub_title).' ('.$pub_year.') '; ?>
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the publication: <?php echo ' '.$pub_title.' ('.$pub_year.') '; ?>
											<?php endif; ?>
										<?php elseif($log_entry['source_table'] == 'images'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the image: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the image: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the image: <?php echo ' '.$item_name.' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php endif; ?>
										<?php elseif($log_entry['source_table'] == 'components'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the component: <?php echo ' '.anchor(base_url() . substr($log_item['url'],1), $item_name).' ';?> in <?php echo ' '.$log_item['name'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the component: <?php echo ' '.anchor(base_url() . substr($log_item['url'],1), $item_name).' ';?> in <?php echo ' '.$log_item['name'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the component: <?php echo ' '.$item_name.' ';?> in <?php echo ' '.$log_item['name'].' ';?>
											<?php endif; ?>
										<?php elseif($log_entry['source_table'] == 'measurements'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the measurement file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the measurement file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the measurement file: <?php echo ' '.$item_name.' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php endif; ?>
										<?php elseif($log_entry['source_table'] == 'vehicle_description'): ?>
											<?php if($log_entry['activity_type'] == 'ADMIN_APPROVE'): ?>
												Supervisor approved the readme file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_REJECT'): ?>
												Supervisor rejected the readme file: <?php echo ' '.anchor(base_url().substr($log_item['url'],1), $item_name).' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php elseif($log_entry['activity_type'] == 'ADMIN_DELETE'): ?>
												Supervisor deleted the readme file: <?php echo ' '.$item_name.' ';?> from <?php echo  ' '.$log_entry['source_table'].' ';?> 
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php else: ?>
									Error: source table does not exist.
								<?php endif; ?>
		
							<?php endif; ?>
	
						</td>
						<td><?php echo $log_entry['date_time']; ?></td>
						<td>
							<?=form_open('admin/measurements','',$hidden=array('vehicle' => 1));?>
								<?=form_hidden('pk_log_id', $log_entry['pk_log_id']);?>
								<div id="delete"><?=form_submit('submit', 'delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>

		<div class="col-md-offset-4">
			<?php echo $this->pagination->create_links(); ?>
		</div>

	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
		</div>
		
		
	</div>
	<!-- END OF CONTAINER-->
</div>