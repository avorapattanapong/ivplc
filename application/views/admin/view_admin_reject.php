<div class="content" id="admin-measurements">
	<h2 class="title" id"title">Rejected Measurements, Vehicles, and Publications</h2>
	
	<p class="note">Warning: Deleting a Vehicle, Publication, Componenent will also delete any Components,
		Authors, and Measurements associated with the deleted content.
The purpose of this page is for Admin to be able to reapprove any rejections or delete them permanently.</p>

	<!-- Review Submissions -->
	<h3>Rejected Vehicle Submissions</h3>
	<?php if(isset($RejectVehicles) && $RejectVehicles[0] != '') : ?>	
		<!-- listing ApproveVehicles or ie. Vehicles-waiting-to-be-approved--> 
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Year</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectVehicles as $vehicle) : ?>
					<tr>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$vehicle['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $vehicle['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$vehicle['manufacturer'];?></td>
						<td><?=$vehicle['model'];?></td>
						<td><?=$vehicle['year'];?></td>
						<td><a href="mailto: <?=$vehicle['contributor'][0]['email'];?>"><?=$vehicle['contributor'][0]['first_name'] . ' ' . $vehicle['contributor'][0]['last_name'];?></a></td>
						<td>
							<?=form_open('admin/measurements','',$hidden=array('vehicle' => 1));?>
								<?=form_hidden('pk_vehicle_id', $vehicle['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $vehicle['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing rejected.</p>
	<?php endif; ?>
	
	<!-- Manage Data Edits -->
	<h3>Rejected User Submission Edits</h3>
	<!-- Edits by users listed by type-->
	<h4>Components</h4>
	<?php if(isset($RejectComponents) && !empty($RejectComponents)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Component Name</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectComponents as $component) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($component['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$component['name'];?></td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_component_id'=>$component['pk_component_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing rejected.</p>
	<?php endif; ?>
	
	<h4>Vehicle Description</h4>
	<p class="note">Note: Vehicle Description cannot be deleted as there must be 1 per vehicle</p>
	<?php if(isset($RejectReadme) && !empty($RejectReadme)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Link to File</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectReadme as $readme) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($readme['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
							<!-- Need to use foreach because though return_vehicle function will
								return only 1 vehicle, the query is stored in an array-->
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td>
							<a class="ext" href="<?=site_url().$readme['url'];?>">
								Vehicle Description File
							</a>
						</td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_desc_id' => $readme['pk_desc_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing rejected.</p>
	<?php endif; ?>
	
	<h4>Measurements</h4>
	<?php if(isset($RejectMeasurements) && !empty($RejectMeasurements)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Component 1</th>
				<th>Component 2</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectMeasurements as $measurement) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($measurement['fk_vehicle_id']);?>
					<?php $comp = $this->components->return_components_indexed();?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$comp[$measurement['fk_componentA_id']]['name'];?></td>
						<td><?=$comp[$measurement['fk_componentA_id']]['name'];?></td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden=array('pk_measurement_id'=>$measurement['pk_measurement_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing rejected.</p>
	<?php endif; ?>
	
	<h4>Images</h4>
	<?php if(isset($RejectImages) && !empty($RejectImages)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Image</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectImages as $image) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($measurement['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td>
							<div class="mask">
								<img class="hero" src="<?=base_url() . $image['url'];?>"/></td>
							</div>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_image_id'=>$image['pk_image_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing rejected.</p>
	<?php endif; ?>
	
	<!-- Manage Publications-->
	<h3>Rejected Publication Submissions/Edited Publications</h3>
	<?php if(isset($ApprovePublications) && $ApprovePublications[0] != '') : ?>
	
		<table id="admin">
			<thead><tr>
				<th>Contributor</th>
				<th>Title</th>
				<th>Publisher</th>
				<th>Publication Year</th>
				<th>Authors</th>
				<th>Options</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectPublications as $pub) : ?>
					<tr>
						<td>
							<p><a href="mailto:<?=$pub['contributor'][0]['email'];?>"><?=$pub['contributor'][0]['first_name'];?> <?=$pub['contributor'][0]['last_name'];?></a></p>
						</td>
						<td>
							<a class="ext" href="<?=$pub['url'];?>"><!-- publication URL -->
							<p><?=$pub['title'];?></p> <!-- Title -->
							</a>
						</td>
						<td><p>
							<?=$pub['affiliation'];?></br>
						</p></td><!-- Publisher-->
						
						<td><p>
							<?=$pub['date'];?>
						</p></td><!-- Date of publication -->
						
						<td><p>
							<?=implode($pub['authors'], ', ');?>
						</p></td><!-- Authors -->
						<td>
						<?=form_open('admin/publications','',$hidden = array('pubs' => 1));?>
							<?=form_hidden('pk_pub_id', $pub['pk_pub_id']);?>
							<div id="approve"><?=form_submit('submit', 'Approve');?></div>
							<div id="delete"><?=form_submit('submit', 'Delete');?></div>
						<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
	
		<p>Nothing rejected.</p>
	
	<?php endif; ?>
	
	
	<h3>Authors</h3>
	<?php if(isset($RejectAuthors) && $RejectAuthors[0] != '') : ?>
	
		<table id="admin">
			<thead><tr>
				<th>Contributor</th>
				<th>Name</th>
				<th>Title</th>
				<th>Publisher</th>
				<th>Authors</th>
				<th>Options</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($RejectAuthors as $author) : ?>
					<?php $tempPub = $this->publications->return_publications_withID($author['fk_pub_id']);?>
					<tr>
						<?php foreach($tempPub as $publication){ ?>
							<td>
								<p><a href="mailto:<?=$publication['contributor'][0]['email'];?>"><?=$publication['contributor'][0]['first_name'];?> <?=$publication['contributor'][0]['last_name'];?></a></p>
							</td>
							
							<td><p>
								<?=$author['first_name'].'.'.' '.$author['last_name'];?>
							</p></td><!-- Date of publication -->
							
							<td>
								<a class="ext" href="<?=$publication['url'];?>"><!-- publication URL -->
								<p><?=$publication['title'];?></p> <!-- Title -->
								</a>
							</td>
							<td><p>
								<?=$publication['affiliation'];?></br>
							</p></td><!-- Publisher-->
							
							
							
							<td><p>
								<?=implode($publication['authors'], ', ');?>
							</p></td><!-- Authors -->
							<td>
							<?=form_open('admin/publications','',$hidden=array('pk_author_id' => $author['pk_author_id']));?>
								<?=form_hidden('pk_pub_id', $publication['pk_pub_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
							</td>
						<?php }?>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
	
		<p>Nothing rejected.</p>
	
	<?php endif; ?>
</div>