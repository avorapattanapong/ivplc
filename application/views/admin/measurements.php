<div class="content" id="admin-measurements">
	<h2 class="title" id="title">Measurements Requiring Approval</h2>

	<!-- Review Submissions -->
	<h3>New Vehicle Submissions</h3>
	<?php if(isset($ApproveVehicles) && $ApproveVehicles[0] != '') : ?>	
		<!-- listing ApproveVehicles or ie. Vehicles-waiting-to-be-approved--> 
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Year</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveVehicles as $vehicle) : ?>
					<tr>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$vehicle['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $vehicle['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$vehicle['manufacturer'];?></td>
						<td><?=$vehicle['model'];?></td>
						<td><?=$vehicle['year'];?></td>
						<td><a href="mailto: <?=$vehicle['contributor'][0]['email'];?>"><?=$vehicle['contributor'][0]['first_name'] . ' ' . $vehicle['contributor'][0]['last_name'];?></a></td>
						<td>
							<?=form_open('admin/measurements','',$hidden=array('vehicle' => 1));?>
								<?=form_hidden('pk_vehicle_id', $vehicle['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $vehicle['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
	
	<!-- Manage Data Edits -->
	<h3>New User Submission Edits</h3>
	<!-- Edits by users listed by type-->
	<h4>Components</h4>
	<?php if(isset($ApproveComponents) && !empty($ApproveComponents)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Component Name</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveComponents as $component) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($component['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$component['name'];?></td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_component_id'=>$component['pk_component_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
	
	<h4>Vehicle Description</h4>
	<?php if(isset($ApproveReadme) && !empty($ApproveReadme)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Link to File</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveReadme as $readme) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($readme['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
							<!-- Need to use foreach because though return_vehicle function will
								return only 1 vehicle, the query is stored in an array-->
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td>
							<a class="ext" href="<?=site_url().$readme['url'];?>">
								Vehicle Description File
							</a>
						</td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_desc_id' => $readme['pk_desc_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
	
	<h4>Measurements</h4>
	<?php if(isset($ApproveMeasurements) && !empty($ApproveMeasurements)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Component 1</th>
				<th>Component 2</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveMeasurements as $measurement) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($measurement['fk_vehicle_id']);?>
					<?php $comp = $this->components->return_components_indexed();?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$comp[$measurement['fk_componentA_id']]['name'];?></td>
						<td><?=$comp[$measurement['fk_componentA_id']]['name'];?></td>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden=array('pk_measurement_id'=>$measurement['pk_measurement_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
	
	<h4>Images</h4>
	<?php if(isset($ApproveImages) && !empty($ApproveImages)) : ?>	
		<table id="admin">
			<thead><tr>
				<th>Link to Vehicle</th>
				<th>Image</th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Contributor</th>
				<td>Options</td>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveImages as $image) : ?>
					<?php $tempVehicle = $this->vehicles->return_vehicles($measurement['fk_vehicle_id']);?>
					<tr>
						<?php foreach( $tempVehicle as $tv ){?>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>admin/measurements/<?=$tv['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $tv['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td>
							<div class="mask">
								<img class="hero" src="<?=base_url() . $image['url'];?>"/></td>
							</div>
						<td><?=$tv['manufacturer'];?></td>
						<td><?=$tv['model'];?></td>
						<td><a href="mailto: <?=$tv['contributor'][0]['email'];?>"><?=$tv['contributor'][0]['first_name'] . ' ' . $tv['contributor'][0]['last_name'];?></a></td>
						<td>
							<?php }?>
							<?=form_open('admin/measurements','',$hidden = array('pk_image_id'=>$image['pk_image_id']));?>
								<?=form_hidden('pk_vehicle_id', $tv['pk_vehicle_id']);?>
								<?=form_hidden('fk_contributor_id', $tv['contributor'][0]['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>Nothing awaiting approval.</p>
	<?php endif; ?>
	
	<!-- Manage Existing Vehicles -->
	<h2>Existing Vehicles</h2><!-- List all approved vehicles -->
	<?php if(isset($vehicles) && $vehicles[0] != '') : ?>
		<table id="admin">
			<thead><tr>
				<th></th>
				<th>Manufacturer</th>
				<th>Model</th>
				<th>Year</th>
				<th>Contributor</th>
				<th>Options</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($vehicles as $vehicle) : ?>
					<tr>
						<td>
							<div class="mask">
								<a class="ext" href="<?=site_url();?>measurements/<?=$vehicle['pk_vehicle_id'];?>">
									<img class="hero" src="<?=base_url() . $vehicle['images'][0]['url'];?>"/>
								</a>
							</div>
						</td>
						<td><?=$vehicle['manufacturer'];?></td>
						<td><?=$vehicle['model'];?></td>
						<td><?=$vehicle['year'];?></td>
						<td><a href="mailto: <?=$vehicle['contributor'][0]['email'];?>"><?=$vehicle['contributor'][0]['first_name'] . ' ' . $vehicle['contributor'][0]['last_name'];?></a></td>
						<td>
							<?=form_open('admin/measurements');?>
								<?=form_hidden('pk_vehicle_id', $vehicle['pk_vehicle_id']);?>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
		<p>No vehicles approved.</p>
	<?php endif; ?>
	
	<!-- Add new manufacturers -->
	<h3>Add New Manufacturer</h3>
	<div id="adminform">
		<?=form_open_multipart('admin/measurements', array('id'=>'add_manu'));?>
			<?=form_label('Manufacturer', 'manu');?>
			<?=form_input(array('name'=>'manu', 'maxlength'=>'20'));?>
			<div id="add_manu">
			<?=form_submit(array('name'=>'submit', 'value'=>'Add'));?>
			</div>
		<?=form_close();?>
	</div>
</div>