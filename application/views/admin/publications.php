<div class="content" id="admin-publications">

	<h2 class="title" id="title">Publications Administration</h2>

	<!-- List all unapproved publications-->
	<h3>New Publication Submissions/Edited Publications</h3>
	<?php if(isset($ApprovePublications) && $ApprovePublications[0] != '') : ?>
	
		<table id="admin">
			<thead><tr>
				<th>Contributor</th>
				<th>Title</th>
				<th>Publisher</th>
				<th>Publication Year</th>
				<th>Authors</th>
				<th>Options</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApprovePublications as $pub) : ?>
					<tr>
						<td>
							<p><a href="mailto:<?=$pub['contributor'][0]['email'];?>"><?=$pub['contributor'][0]['first_name'];?> <?=$pub['contributor'][0]['last_name'];?></a></p>
						</td>
						<td>
							<a class="ext" href="<?=$pub['url'];?>"><!-- publication URL -->
							<p><?=$pub['title'];?></p> <!-- Title -->
							</a>
						</td>
						<td><p>
							<?=$pub['affiliation'];?></br>
						</p></td><!-- Publisher-->
						
						<td><p>
							<?=$pub['date'];?>
						</p></td><!-- Date of publication -->
						
						<td><p>
							<?=implode($pub['authors'], ', ');?>
						</p></td><!-- Authors -->
						<td>
						<?=form_open('admin/publications','',$hidden = array('pubs' => 1));?>
							<?=form_hidden('pk_pub_id', $pub['pk_pub_id']);?>
							<div id="approve"><?=form_submit('submit', 'Approve');?></div>
							<div id="delete"><?=form_submit('submit', 'Reject');?></div>
						<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
	
		<p>Nothing awaiting approval.</p>
	
	<?php endif; ?>
	
	
	<h3>Added Authors</h3>
	<?php if(isset($ApproveAuthors) && $ApproveAuthors[0] != '') : ?>
	
		<table id="admin">
			<thead><tr>
				<th>Contributor</th>
				<th>Name</th>
				<th>Title</th>
				<th>Publisher</th>
				<th>Authors</th>
				<th>Options</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($ApproveAuthors as $author) : ?>
					<?php $tempPub = $this->publications->return_publications_withID($author['fk_pub_id']);?>
					<tr>
						<?php foreach($tempPub as $publication){ ?>
							<td>
								<p><a href="mailto:<?=$publication['contributor'][0]['email'];?>"><?=$publication['contributor'][0]['first_name'];?> <?=$publication['contributor'][0]['last_name'];?></a></p>
							</td>
							
							<td><p>
								<?=$author['first_name'].'.'.' '.$author['last_name'];?>
							</p></td><!-- Date of publication -->
							
							<td>
								<a class="ext" href="<?=$publication['url'];?>"><!-- publication URL -->
								<p><?=$publication['title'];?></p> <!-- Title -->
								</a>
							</td>
							<td><p>
								<?=$publication['affiliation'];?></br>
							</p></td><!-- Publisher-->
							
							
							
							<td><p>
								<?=implode($publication['authors'], ', ');?>
							</p></td><!-- Authors -->
							<td>
							<?=form_open('admin/publications','',$hidden=array('pk_author_id' => $author['pk_author_id']));?>
								<?=form_hidden('pk_pub_id', $publication['pk_pub_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
							</td>
						<?php }?>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
	
		<p>Nothing awaiting approval.</p>
	
	<?php endif; ?>
	
	
	<!-- List all approved publications -->
	<h3>Existing Publications</h3>
	<?php if(isset($publications) && $publications[0] != '') : ?>
	
		<table id="admin">
			<thead><tr>
				<th>Contributor</th>
				<th>Title</th>
				<th>Publisher</th>
				<th>Publication Year</th>
				<th>Authors</th>
				<th>Delete</th>
			</tr></thead>
	
			<tbody>		
				<?php foreach($publications as $pub) : ?>
					<tr>
						<td>
							<p><a href="mailto:<?=$pub['contributor'][0]['email'];?>"><?=$pub['contributor'][0]['first_name'];?> <?=$pub['contributor'][0]['last_name'];?></a></p>
						</td>
						<td>
							<a class="ext" href="<?=$pub['url'];?>"><!-- publication URL -->
							<p><?=$pub['title'];?></p> <!-- Title -->
							</a>
						</td>
						<td><p>
							<?=$pub['affiliation'];?></br>
						</p></td><!-- Publisher-->
						
						<td><p>
							<?=$pub['date'];?>
						</p></td><!-- Date of publication -->
						
						<td><p>
							<?=implode($pub['authors'], ', ');?>
						</p></td><!-- Authors -->
						<td>
						<?=form_open('admin/publications');?>
							<?=form_hidden('pk_pub_id', $pub['pk_pub_id']);?>
							<div id="delete"><?=form_submit('submit', 'Delete');?></div>
						<?=form_close();?>
						</td>
					</tr>
				<?php endforeach;?>		
			</tbody>
		</table>
	<?php else : ?>
	
		<p>No publications approved.</p>
	
	<?php endif; ?>
	
</div>