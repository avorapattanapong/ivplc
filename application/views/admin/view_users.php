<div class="content" id="admin-group">
	
	<h2 class="title" id="title">Contributors</h2>
	<div id="admin">	
	<?php if(isset($users) && !empty($users)) : ?>	
		<!-- listing of contributors categorized in to-be-verified contributors and existing contributors--> 
		<h3>New Contributors</h3>

			<table id="admin">
			<thead><tr>
				<th>Name</th>
				<th>Email</th>
				<th>Affiliation</th>
				<th>City</th>
				<th>Country</th>
				<th>Options</th>
			</tr></thead>

			<tbody>		
				<?php $check = 0;?>
				<?php foreach($users as $contributor) : ?>	
					<?php if($contributor['verified'] == 0):?>
						<?php $check = 1;?>
						<tr>
						<td><!-- Member name -->
							<p><?=$contributor['first_name'].' '.$contributor['last_name'];?></p>
						</td>
						<td><!-- email -->
							<p><a href="mailto:<?=$contributor['email'];?>"><?=$contributor['email'];?></a></p>					
						</td>
						<td><!-- Member Affiliation -->	
							<p><?=$contributor['affiliation'];?></p>
						</td>
						<td><!-- Member City -->	
							<p><?=$contributor['city'];?></p>
						</td>
						<td><!-- Member Country -->	
							<p><?=$contributor['country'];?></p>
						</td>
						<td>
							<?=form_open('admin/users');?>
								<?=form_hidden('pk_contributor_id', $contributor['pk_contributor_id']);?>
								<div id="approve"><?=form_submit('submit', 'Approve');?></div>
								<div id="delete"><?=form_submit('submit', 'Reject');?></div>
							<?=form_close();?>
						</td>
						</tr>
					<?php endif; ?>
				<?php endforeach;?>		
			</tbody>
		</table>
		<?php if( $check == 0):?>
			<p>No contributor awaiting verification.</p>
		<?php endif;?>
		
		<!-- Existing Users-->
		<h3>Existing Contributors</h3>
			<table id="admin">
			<thead><tr>
				<th>Name</th>
				<th>Email</th>
				<th>Affiliation</th>
				<th>City</th>
				<th>Country</th>
				<th>Options</th>
			</tr></thead>

			<tbody>		
				<?php $check2 = 0;?>
				<?php foreach($users as $contributor) : ?>
					<?php if($contributor['verified'] == 1):?>
						<?php $check2 = 1;?>
						<tr>
						<td><!-- Member name -->
							<p><?=$contributor['first_name'].' '.$contributor['last_name'];?></p>
						</td>
						<td><!-- email -->
							<p><a href="mailto:<?=$contributor['email'];?>"><?=$contributor['email'];?></a></p>					
						</td>
						<td><!-- Member Affiliation -->	
							<p><?=$contributor['affiliation'];?></p>
						</td>
						<td><!-- Member City -->	
							<p><?=$contributor['city'];?></p>
						</td>
						<td><!-- Member Country -->	
							<p><?=$contributor['country'];?></p>
						</td>
						<td>
							<div class="row" id="admin_table_options">
								<div class="col-md-5">
							<?php echo form_open('admin/set_users'); ?>
							<?php echo form_hidden('pk_contributor_id', $contributor['pk_contributor_id']);?>
							<?php echo form_hidden('user_type', 'contributor');?>
							<?php echo form_submit(array('name'=>'submit','value'=>'Edit User','class'=>'btn btn-info')); ?>
							<?php echo form_close();?>
							</div>
								<div class="col-md-2">
							<?=form_open('admin/users');?>
								<?=form_hidden('pk_contributor_id', $contributor['pk_contributor_id']);?>
								<div id="delete"><?=form_submit('submit', 'Delete');?></div>
							<?=form_close();?>
							</div>
								
							
							</div>
						</td>
						</tr>
					<?php endif; ?>
				<?php endforeach;?>		
			</tbody>
		</table>
		<?php if( $check2 == 0):?>
			<p>No contributor available.</p>
		<?php endif;?>
	<?php else : ?>
		<p>No contributor exist.</p>
	<?php endif; ?>
	
	</div>
	
</div>