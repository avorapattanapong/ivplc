<div class="content" id="submit_personal">

	<h2 class="title" id="title">Submit Personal Information</h2>

	<?php if(validation_errors()) : ?>
		<div class="errors"><?=validation_errors();?></div>
	<?php endif; ?>
	
	<?=form_open('ctrl_members/registration', array('id'=>'submit_general'));?>
		<?=form_fieldset();?>
			<?=form_label('<span>*</span> Email', 'email');?>
			<?=form_input(array('name'=>'email', 'maxlength'=>'50', 'value'=>set_value('email'), 'placeholder' => 'Enter Email'));?>
			
			<?=form_label('<span>*</span> Password', 'password');?>
			<?=form_password(array('name'=>'password', 'maxlength'=>'50', 'placeholder' => 'Enter Password'));?>
			
			<?=form_label('<span>*</span> Confirm Password', 'cpassword');?>
			<?=form_password(array('name'=>'cpassword', 'maxlength'=>'50', 'placeholder' => 'Confirm Your Password'));?>
			
			<?=form_label('<span>*</span> First Name', 'first');?>
			<?=form_input(array('name'=>'first', 'maxlength'=>'30', 'value'=>set_value('first'), 'placeholder' => 'Enter Your First Name'));?>
		
			<?=form_label('<span>*</span> Last Name', 'last');?>
			<?=form_input(array('name'=>'last', 'maxlength'=>'30', 'value'=>set_value('last'), 'placeholder' => 'Enter Your Last Name'));?>
			
			<?=form_label('<span>*</span> Affiliation', 'affiliation');?>
			<?=form_input(array('name'=>'affiliation', 'maxlength'=>'100', 'value'=>set_value('affiliation'), 'placeholder' => 'Enter Affiliation'));?>
			
			<?=form_label('City', 'city');?>
			<?=form_input(array('name'=>'city', 'maxlength'=>'30', 'value'=>set_value('city'), 'placeholder' => 'Enter City'));?>
			
			<?=form_label('Country', 'country');?>
			<?=form_input(array('name'=>'country', 'maxlength'=>'30', 'value'=>set_value('country'), 'placeholder' => 'Enter Country'));?>

			<?=form_submit('submit', 'Submit');?>
		<?=form_fieldset_close();?>
	<?=form_close();?><!-- End Personal Form -->

</div><!-- end of Content // Begin Footer File-->