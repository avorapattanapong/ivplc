<div class="content">
	
	<h2 class="title">
		<div class="row">
		<div class="col-md-8">
			Edit Vehicle details and Add Components
		</div>
		</div>
		<div class="row">	
		<div class="col-md-8 col-md-offset-4" id="mini_nav_long">
			<ul class="nav nav-pills">
			 	<li><?php echo anchor('edit/edit_vehicle?id='.$vehicle['pk_vehicle_id'],'Manage Files', array('class'=>'mini_nav_button'))?></li>
			  	<li class="active"><a href="" class="mini_nav_button">Edit Vehicle details and add Components</a></li>
			  	<li><?php echo anchor('edit/vehicle_add_measure?id='.$vehicle['pk_vehicle_id'],'Add Measurements', array('class'=>'mini_nav_button')); ?></li>
			</ul>	
		</div>
		</div>
	</h2>
				
			
	<?php if(validation_errors()) : ?>
		<div class="errors"><?=validation_errors();?></div>
	<?php endif; ?>
	
	<?php echo $msg;?>
	<!-- Display a success/fail message-->
	
	<?=form_open_multipart('edit/vehicle_add_comp', array('id'=>'submit_general'),$hidden = array('id'=> $id));?>
	
		<!-- VEHICLE INFORMATION -->
		<?=form_fieldset('Vehicle Information');?>
			
			<h6 class="col-md-6" id="label">Edit Vehicle Information</h6>
			<h6 class="col-md-6" id="label"> Original Vehicle Information</h6>
			
			<div class="row-fluid">
				<div class="col-md-6">
					<?=form_label('<span>*</span> Manufacturer', 'manufacturer');?>
					<?=form_dropdown('manufacturer', $manufacturers, set_value('manufacturer'),'id=drop'); ?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well_manu">
						<?php echo $vehicle['manufacturer']; ?>
					</div>
				</div>
			</div>
			
			<script>
				$('#drop').prop('selectedIndex', -1)
			</script>
				
			<div class="row-fluid">
				<div class="col-md-6">	
					<?=form_label('<span>*</span> Model', 'model');?>
					<?=form_input(array('name'=>'model', 'maxlength'=>'40', 'value'=>set_value('model')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $vehicle['model']; ?>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="col-md-6">	
					<?=form_label('<span>*</span> Year', 'year');?>
					<?=form_input(array('name'=>'year', 'maxlength'=>'4', 'value'=>set_value('year')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $vehicle['year']; ?>
					</div>
				</div>
			</div>
		<?=form_fieldset_close();?>
		
		<?=form_fieldset('Images');?>	
			<div class="controls">
				<a class="add" id="incrementImage" href="#">Add Image</a>
				<a class="remove" id="decrementImage" href="#">Remove Image</a>
			</div>
			
			<div class="group" id="images">
				<div class="group_member" id="image1">
					<?=form_label('Image 1', 'image1');?>
					<?=form_upload(array('name'=>'image[]'));?>
				</div>
			</div>
		<?=form_fieldset_close();?>

		<div class="row-fluid">
		<!-- COMPONENTS -->
		<h4> Current Component of this vehicle: </h4>
		<?php $index = 1;?>
		<?php foreach($vehicle['components'] as $component) { ?>
			<ul>
				<li>
					<div class="row-fluid">
						<div class="well well-small" id="data_well_comp">
							<?php echo "Component ".$index.": ".$component['name']; ?>
						</div>
					</div>
				</li>
			</ul>
			<?php $index++; ?>	
		<?php }
					$count_comp=$index;?>	
		<script>
			$compCounter2 = <?php echo $index;?>;
		</script>
		</div>
		<div class="row-fluid">
		<?=form_fieldset('Add Components');?>	
			<div class="controls">
				<a class="add" id="incrementComponent2" href="#">Add Component</a>
				<a class="remove" id="decrementComponent2" href="#">Remove Component</a>
			</div>
			
			<p class="note">Please provide the names of each component, and attach any files associated with that singular point (ie: noise function file).</p>
			
			<div class="group" id="components">
					<div class="group_member component" id="component".<?php $count_comp;?>>
					</div>
			</div>
		<?=form_fieldset_close();?>
	</div>
		<!-- TERMS AND CONDITIONS -->
		<?=form_fieldset('Terms and Conditions');?>
			<div class="clear"></div>
			<p id="terms">I am the owner of the submitted content, and hereby grant the IVPLC Group of UBC permission to display the submitted content on the IVPLC Group website. If I am not the legal owner of the content, I take on full liability and accept all responsibility and penalty for any infringement that ensues.</p>
			<p id="confirm">
			<?=form_checkbox('agreement', 'agree', set_value('agreement'));?>
			I have read and agree to the Terms and Conditions</p>

		<?=form_fieldset_close();?>
		<?=form_fieldset('');?>
			<?=form_submit('submit', 'Submit');?>
		<?=form_fieldset_close();?>
	

</div><!-- end of Content // Begin Footer File-->

<script>
	/* COMPONENTS & MEASUREMENTS INCR/DECR FOR EDITS*/

	$('#incrementComponent2').click(function(){
		if($compCounter2 <= 10){
			/* INCR & DECR */
			if($compCounter2 <= <?php echo $count_comp+1; ?>){
				$('#decrementComponent2').stop().animate({'opacity':'1'}).css({'cursor':'pointer'});
			}
			
			/* COMPONENTS */
			$component = '<div class="group_member component" id="component'+$compCounter2+'">';
			$component += '<label for="component_name">Component '+$compCounter2+'</label>';
			$component += '<input type="text" name="component_name[]" maxlength="20"/>';
			$component += '<input type="file" name="component[]"/>';
			$component += '</div>';
			
			$('#components').append($component);
			$('#component'+$compCounter2).css({'display':'none'}).slideDown(300);

			$compCounter2++;
		}
		
		return false;
	});
	
	$('#decrementComponent2').click(function(){
		if($compCounter2 > <?php echo $count_comp; ?>){
			$compCounter2--;
			
			/*COMPONENTS*/
			$('#component'+$compCounter2).slideUp(300, function(){ $(this).remove(); });
			
			/*INCR & DECR*/
			if($compCounter2 < <?php echo $count_comp; ?>){
				$('#decrementComponent').stop().animate({'opacity':'0.25'}).css({'cursor':'default'});
			} else {
				$('#decrementComponent').stop().animate({'opacity':'1'}).css({'cursor':'pointer'});
			}
		}
		
		return false;
	});

</script>