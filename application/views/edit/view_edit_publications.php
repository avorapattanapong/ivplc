<div class="content" id="submit_publication">
	
	<h2 class="title">Edit Publication</h2>
	
	<?php if(validation_errors()) : ?>
		<div class="errors_pub"><?=validation_errors();?></div>
	<?php endif; ?>
	
	<?php echo $msg; ?>
	<?php echo br(1); ?>
	<?=form_open('edit/edit_publications', array('id'=>'submit_epublications'),$hidden = array('id' => $id));?>
	
		<!-- PUBLICATION INFORMATION -->
		
		<?=form_fieldset('Publication Information');?>
			
			<div class="row-fluid">
				<h6 class="col-md-6" id="label">Edit Publication Information</h6>
				<h6 class="col-md-6" id="label"> Original Publication Information</h6>
			</div>
			<div class="row-fluid">
				<div class="col-md-6">
					<?=form_label("Title", 'title');?>
					<?=form_input(array('name'=>'title', 'maxlength'=>'100', 'value'=>set_value('title')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $publication['title'];?>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="col-md-6">
					<?=form_label('Publisher', 'affiliation');?>
					<?=form_input(array('name'=>'affiliation', 'maxlength'=>'100', 'value'=>set_value('affiliation')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $publication['affiliation'];?>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="col-md-6">
					<?=form_label('Publication Year', 'date');?>
					<?=form_input(array('name'=>'date', 'maxlength'=>'4', 'value'=>set_value('date')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $publication['date'];?>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="col-md-6">
					<?=form_label('URL', 'url');?>
					<?=form_input(array('name'=>'url', 'maxlength'=>'400', 'value'=>set_value('url')));?>
				</div>
				<div class="col-md-6">
					<div class="well well-sm" id="data_well">
						<?php echo $publication['url'];?>
					</div>
				</div>
			</div>
		<?=form_fieldset_close();?>


		<!-- AUTHORS -->
		<?=form_fieldset('Authors');?>
		<p class="note">Check the boxes beside the name of the Author you want to delete. 
			Note: Keep in mind that there must 
			be at least one author per publication but you can delete all authors and add at least one new author on this 
			same page before clicking "submit".</p>
			
			<ul>
				<?php foreach($publication['authors'] as $author){ 				?>
					<li>
				<?php	echo $author['name']; 									?>
				<?php	echo form_checkbox('rm_authors[]',$author['author_id']);?>
					</li>
					
				<?php }															?>
			</ul>
			<ul>
			<?php if( !empty($awaiting_verification_authors) ): ?>
				<?php foreach($awaiting_verification_authors as $author){ 				?>
					<li>
						<?php	echo $author['first_name'].' '.$author['last_name']; 									?>
						<?php	echo form_checkbox('rm_authors[]',$author['pk_author_id']);?>
						<div class="label label-info await_text">awaiting verification</div>
					</li>
				<?php }															?>
			</ul>
			<?php endif; ?>
			
			<?php if( !empty($rejected_authors) ): ?>
			<ul>
				<?php foreach($rejected_authors as $author){ 				?>
					<li>
						<?php	echo $author['first_name'].' '.$author['last_name']; 									?>
						<?php	echo form_checkbox('rm_authors[]',$author['pk_author_id']);?>
						<div class="label label-important reject_text">content rejected</div>
					</li>
				<?php }															?>
			</ul>
			<?php endif; ?>
			

			<h3 >Add Authors</h3>

			<div class="controls_edit" >
				<a class="add" id="incrementAuthor" href="#">Add Author</a>
				<a class="remove" id="decrementAuthor" href="#">Remove Author</a>
			</div>
			
			<div class="clear"></div>
			
			<div class="group" id="authors">
				<div class="group_member" id="author1">
					
				</div>
			</div>
		<?=form_fieldset_close();?>
		
		<!-- RELATED VEHICLES -->
		
	
	
		<!-- TERMS AND CONDITIONS -->
		<?=form_fieldset('Terms and Conditions');?>
			<div class="clear"></div>
			<p id="terms">I am the owner of the submitted content, and hereby grant the IVPLC Group of UBC permission to display the submitted content on the IVPLC Group website. If I am not the legal owner of the content, I take on full liability and accept all responsibility and penalty for any infringement that ensues.</p>

			<?=form_checkbox('agreement', 'agree', set_value('agreement'));?>
			<p id="confirm">I have read and agree to the Terms and Conditions</p>
			
			<?=form_submit('submit', 'Submit');?>
		<?=form_fieldset_close();?>
		
	<?=form_close();?><!-- End Publication Form -->
</div><!-- end of Content // Begin Footer File-->