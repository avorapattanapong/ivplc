<div class="content">

	<h2 class="title">
		<div class="row">
		<div class="col-md-4">
			Add Measurements
		</div>
		<div class="col-md-8" id="mini_nav">
			<ul class="nav nav-pills">
			 	<li><?php echo anchor('edit/edit_vehicle?id='.$vehicle['pk_vehicle_id'],'Manage Files',array('class'=>'mini_nav_button'))?></li>
			  	<li><?php echo anchor('edit/vehicle_add_comp?id='.$vehicle['pk_vehicle_id'],'Edit Vehicle details and add Components', array('class'=>'mini_nav_button')); ?></li>
			  	<li class="active"><a href="" class="mini_nav_button">Add Measurements</a></li>
			</ul>
		</div>
		</div>
	</h2>

	<?php echo $msg;?>
	
	<?=form_open_multipart('edit/vehicle_add_measure', array('id'=>'submit_general'),$hidden = array('id'=>$id));?>
	
		<!-- MEASUREMENTS -->
		<?=form_fieldset('Add Measurements');?>
		<div class="container">
		<div class="row">
			<p class="note">Please attach the s-parameter files associated between the two referenced points.</p>
		</div>
		<?php $num_comp = 1; ?>	
		<!-- Keeps track of number of current component for display-->
		<?php $max_measure = 0; ?>	
		<!-- Keeps track of maximum number of measurement for display-->
		
		<?php foreach( $vehicle['components'] as $component_arr ):?>
			<?php $max_measure = count($vehicle['components']) - $num_comp;?>
			<?php $num_measure = 0; ?>	
		<!-- Keeps track of number of measurement for display-->
			<div class="row">
			<h3 class="component "<?php.$num_comp; ?>Component <?php echo $num_comp;?> </h3>
			</div>
			<!-- EXISTING COMPONENT AND STATUS-->
			<div class="row">
				<!-- COMPONENTS LIST-->
				<div class="col-md-5">
					<div class="panel panel-primary" id="panel_add_measure_main">
						<div class="panel-heading">
							Existing Measurement Files in this Component:
						</div>
						<ul class="list-group">
							<?php foreach ($vehicle['measurements'] as $measurement_arr): ?>	
								<?php if($this->model_edit->isTransferFile($vehicle['pk_vehicle_id'],$measurement_arr['pk_measurement_id'])== TRUE): ?>
									<?php if($measurement_arr['fk_componentA_id'] == $component_arr['pk_component_id']): ?>
										<li class="list-group-item">		
											<?php echo $vehicle['components'][$measurement_arr['fk_componentB_id']]['name'];?>
										</li>
										<?php $num_measure++;?>
									<?php endif;?>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			
			
			<!-- STATUS LIST-->
			<div class="col-md-3">
				<div class="panel panel-primary" id="panel_add_measure_status">
					<div class="panel-heading">
						Status
					</div>
					<ul class="list-group">
						<?php foreach ($vehicle['measurements'] as $measurement_arr): ?>	
							<?php if($this->model_edit->isTransferFile($vehicle['pk_vehicle_id'],$measurement_arr['pk_measurement_id'])== TRUE): ?>
								<?php if($measurement_arr['fk_componentA_id'] == $component_arr['pk_component_id']): ?>
									<li class="list-group-item">		
										<?php if($measurement_arr['verified'] == 0 && $measurement_arr['reject'] == 0):?>
											<div class="label label-default await_text">awaiting verification</div>
										<?php elseif($measurement_arr['verified'] == 0 && $measurement_arr['reject'] == 1):?>
											<div class="label label-danger reject_text">content rejected</div>
										<?php else:?>
											<span class="text_placeholder">.</span>
										<?php endif; ?>	
									</li>
									<?php $num_measure++;?>
								<?php endif;?>
							<?php endif;?>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
			</div>
			
			<div class="row">
				<div class="reference">
					<h5> Add measurement files to available slots: </h5>
					<?php $rm_measure = $max_measure - $num_measure;?>
					<?php if($rm_measure == 0): ?>
						<div class="note">None available.<br></div>
					<?php endif?>
					<?php $m = $this->model_edit->missing_measurement($vehicle,$num_comp);
						  foreach($m as $num){
							  echo $component_arr['pk_component_id'].'-'.$num;
							  echo br(1);
						  }?>
					<?php for($i=0;$i< count($m);$i++): ?>
						<?php echo form_label($component_arr['name'].'-'.$this->model_edit->return_comp_name($m[$i]), 
						  	  'measurement', array('class'=>'component'.($i+1)));?>
						<?php echo form_upload('measurement[]');?>
						<?php echo form_hidden('compA[]', $component_arr['pk_component_id']); ?>
						<?php echo form_hidden('compB[]', $m[$i]);?>
					<?php endfor; ?>
				</div>
			</div>				
			<?php $num_comp++; ?>
		<?php endforeach;?>
		<?=form_submit('submit', 'Submit');?>
	<?=form_fieldset_close();?>
	
	
		<!-- TERMS AND CONDITIONS -->
	<div class="clear">
		<h5>TERMS AND CONDITIONS</h5>
		<p id="terms">By uploading a file and clicking on "submit" you are hereby agreeing to the following terms:</p>
		<p id="terms">I am the owner of the submitted content, and hereby grant the IVPLC Group of 
			UBC permission to display the submitted content on the IVPLC Group website.
			 If I am not the legal owner of the content, I take on full liability and accept all 
			 responsibility and penalty for any infringement that ensues.</p>
	</div>
	</div>		
</div><!-- end of Content // Begin Footer File-->