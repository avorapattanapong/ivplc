<div class="content" id="measurements_index">
	
	<h2 class="title" id="title"> Choose a Vehicle to Edit</h2>
	<?php echo $msg;?>
	<div class="row-fluid">
		<p class="note">Click on a vehicle to begin edting. </br>
			Check in the checkboxes beside a vehicle you want to delete. </br>
			Note: Vehicles awaiting verification or rejected will not be available for editing.
		</p>
	</div>
	
	<div class="row-fluid">
		<?php if(!empty($cars)):?>
			<form action="list_vehicle"method="post">	
				<div class="row">
					<?php foreach($cars as $field): ?>
						<?php if($field['view'] == 1 && $field['reject'] == 0):?>
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">
											<a class="vehicles" href="edit_vehicle?id=<?=$field['pk_vehicle_id'];?>">
												<div class="mask">
													<?php if($field['images'][0] != '') : ?>
														<img class="hero" src="<?=base_url() . $field['images'][0]['url'];?>"/>
													<?php else : ?>
														<img class="hero" src="<?=base_url();?>resources/styles/images/car.png"/>
													<?php endif; ?>
												</div>
												<p class="model"><?=$field['manufacturer'] . ' ' . $field['model'];?></p>
												<p class="year"><?=$field['year'];?></p>
											</a>
											<?php echo form_checkbox(array('name'=>'del_vehicle[]', 'value' => $field['pk_vehicle_id'],'id' => 'del_vehicle')); ?>
										</div>
									</div>
								</div>
							</div>
						<?php elseif($field['view'] == 0 && $field['reject'] == 0):?>
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">
											<div class="vehicles">
												<div class="mask">
													<?php if($field['images'][0] != '') : ?>
														<img class="hero" src="<?=base_url() . $field['images'][0]['url'];?>"/>
													<?php else : ?>
														<img class="hero" src="<?=base_url();?>resources/styles/images/car.png"/>
													<?php endif; ?>
												</div>
												<p class="model"><?=$field['manufacturer'] . ' ' . $field['model'];?></p>
												<p class="year"><?=$field['year'];?></p>
												<div class="label label-default await_text">awaiting verification</div>
											</div>
											<?php echo form_checkbox(array('name'=>'del_vehicle[]', 'value' => $field['pk_vehicle_id'],'id' => 'del_vehicle')); ?>
										</div>
									</div>
								</div>
							</div>
						<?php elseif($field['view'] == 0 && $field['reject'] == 1):?>
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">
											<div class="vehicles">
												<div class="mask">
													<?php if($field['images'][0] != '') : ?>
														<img class="hero" src="<?=base_url() . $field['images'][0]['url'];?>"/>
													<?php else : ?>
														<img class="hero" src="<?=base_url();?>resources/styles/images/car.png"/>
													<?php endif; ?>
												</div>
												<p class="model"><?=$field['manufacturer'] . ' ' . $field['model'];?></p>
												<p class="year"><?=$field['year'];?></p>
												<div class="label label-danger reject_text">content rejected</div>
											</div>
											<?php echo form_checkbox(array('name'=>'del_vehicle[]', 'value' => $field['pk_vehicle_id'],'id' => 'del_vehicle')); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<div class="row">
					<?php echo form_submit(array('name' => 'submit', 'value' => 'Delete Selected', 'class' => 'btn btn-danger', 'id'=>'del_selected')); ?>
				</div>
			</form>
		<?php	else:	?>
			<p> No Vehicles Available. </p>
		<?php endif; ?>
	</div>
</div>