<!DOCTYPE html>
<html lang = "en">
	<div class="content">

	<h2 class="title" id="title">Personal Information</h2>

<?php echo $msg; ?>
<div class="errors_pub"><?php echo validation_errors(); ?></div>

<?php echo form_open("edit",$attri = array('id' => 'submit_general')); ?>

	
	<?php if($this->session->userdata('isAdmin') == 0){ ?>	
		<?=form_fieldset('Personal Information');?>
		<div class="row-fluid">
			<h6 class="col-md-6" id="label">Edit Personal Information</h6>
			<h6 class="col-md-6" id="label">Original Personal Information</h6>
		</div>
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("First Name", "fname");
				$data = array(
					"name" => "fname",
					"id" => "fname",
					"value" => '',
					"placeholder" => "Enter a new First Name"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $first_name;?></div>
 			</div>
		</div>
	
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Last Name", "lname");
				$data = array(
					"name" => "lname",
					"id" => "lname",
					"value" => '',
					"placeholder" => "Enter a new Last Name"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $last_name;?></div>
 			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Affiliation", "affiliation");
				$data = array(
					"name" => "affiliation",
					"id" => "affiliation",
					"value" => '',
					"placeholder" => "Enter a new Affiliation"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $affiliation;?></div>
 			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("City", "city");
				$data = array(
					"name" => "city",
					"id" => "city",
					"value" => '',
					"placeholder" => "Enter a new City"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $city;?></div>
 			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Country", "country");
				$data = array(
					"name" => "country",
					"id" => "country",
					"value" => '',
					"placeholder" => "Enter a new Country"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $country;?></div>
 			</div>
		</div>
	<?=form_fieldset_close();?>

	<?=form_fieldset('Login Information');?>
		<div class="row-fluid">
			<h6 class="col-md-6" id="label">Edit Login Information</h6>
			<h6 class="col-md-6" id="label">Original Login Information</h6>
		</div>
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Email", "email");
				$data = array(
					"name" => "email",
					"id" => "email",
					"value" => '',
					"placeholder" => "Enter a new Email"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $email;?></div>
 			</div>
		</div>
	
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Password", "password");
				$data = array(
					"name"=> "password", 
					"maxlength"=>"50",
					"value" => '',
					"placeholder" => "Enter a new Password"
				);
				echo form_password($data); ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("*Confirm Password", "cpassword");
				$data = array(
					"name"=> "cpassword", 
					"maxlength"=>"50",
					"value" => '',
					"placeholder" => "Confirm the new Password"
				);
				echo form_password($data); ?>
			</div>
		</div>
		
	<?=form_fieldset_close();?>	
	
		
<?php }
	else{ ?>
		
		<?=form_fieldset('Personal Information');?>
	
		<div class="row-fluid">
			<h6 class="col-md-6" id="label">Edit Login Information</h6>
			<h6 class="col-md-6" id="label">Original Login Information</h6>
		</div>
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Name", "name");
				$data = array(
					"name" => "name",
					"id" => "name",
					"value" => '',
					"placeholder" => "Enter a new Name"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $name;?></div>
 			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Email", "email");
				$data = array(
					"name" => "email",
					"id" => "email",
					"value" => '',
					"placeholder" => "Enter a new Email"
				);
				echo form_input($data); ?>
			</div>
			<div class="col-md-6">	
				<div class="well well-sm" id="data_well"><?php echo $email;?></div>
 			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("Password", "password");
				$data = array(
					"name"=> "password", 
					"maxlength"=>"50",
					"value" => '',
					"placeholder" => "Enter a new Password"
				);
				echo form_password($data); ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-6">	
				<?php echo form_label("*Confirm Password", "cpassword");
				$data = array(
					"name"=> "cpassword", 
					"maxlength"=>"50",
					"value" => '',
					"placeholder" => "Confirm the new Password"
				);
				echo form_password($data); ?>
			</div>
		</div>
		
	<?=form_fieldset_close();?> 

	
<?php }
	echo form_submit("submit","Submit");?>
	
	<?php echo form_close(); ?>
	
	
	
	
</div>
</html>
