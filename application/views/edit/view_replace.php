<div class="content" id="replace">
	
	<h2 class="title" id="title"> Replace a File or Change the Name of a Component</h2>
	<?php echo form_open_multipart('edit/replace_comp_measure', array('id'=>'submit_general', "onsubmit" => "return confirm('Warning: Deleting a component will delete any transfer functions and noise files associated with the component. Would you like to proceed?');")); ?>
	<?php if($type == 'IMAGE'): ?>

		<?php echo form_fieldset('Replace an Image'); ?>
		<p class="note">Upload a new image file to upload and replace the chosen file.</p>
		<?php echo form_label('Image', 'image')?>
		<?php echo form_upload('image[]')?>
		<?php echo form_hidden('pk_image_id', $file_id); ?>
		<?php echo form_hidden('type', 'IMAGE'); ?>
		<?php echo form_hidden('fk_vehicle_id', $fk_vehicle_id); ?>
		<?php echo form_fieldset_close(); ?>
	
	<?php elseif($type == 'COMPONENT'): ?>

		<?php echo form_fieldset('Replace a Noise File'); ?>
		<p class="note">Upload a new noise file and replace the chosen file.</p>
		<?php echo form_label('Noise File', 'component')?>
		<?php echo form_upload('component[]')?>
		<?php echo form_hidden('pk_component_id', $file_id); ?>
		<?php echo form_hidden('type', 'COMPONENT'); ?>
		<?php echo form_hidden('fk_vehicle_id', $fk_vehicle_id); ?>
		<?php echo form_fieldset_close(); ?>
		
	<?php elseif($type == 'COMPONENT_NAME'): ?>
		
		<?php echo form_fieldset('Change a Component Name or Add a Noise File', $attri = array('id' => 'submit_general')); ?>
		<p class="note">Upload a new noise file or change the chosen component name.</p>
		<?php echo form_label("Component", "comp_name");
				$data = array(
					"name" => "component_name",
					"id" => "component_name",
					"value" => '',
					"placeholder" => "Enter a new Component Name"
				);
				echo form_input($data); ?>
		<?php if ($isNoise == FALSE): ?>
			<?php echo form_label('Noise File', 'component')?>
			<?php echo form_upload('component[]')?>
			
		<?php endif; ?>
		<div class="del_comp">
			<?php echo form_submit(array("name"=>"submit", "value" => "Delete", "id" => "dcomp") ); ?>
			
		</div>
		<?php echo form_hidden('pk_component_id', $file_id); ?>
		<?php echo form_hidden('type', 'COMPONENT_NAME'); ?>
		<?php echo form_hidden('fk_vehicle_id', $fk_vehicle_id); ?>
		<?php echo form_fieldset_close(); ?>
		
	<?php elseif($type == 'MEASURE'): ?>
		
		<?php echo form_fieldset('Replace a Transfer Function File'); ?>
		<p class="note">Upload a new transfer function file and replace the chosen file.</p>
		<?php echo form_label('Transfer Function', 'measurement')?>
		<?php echo form_upload('measurement[]')?>
		<?php echo form_hidden('pk_measurement_id', $file_id); ?>
		<?php echo form_hidden('compA', $compA); ?>
		<?php echo form_hidden('compB', $compB); ?>
		<?php echo form_hidden('type', 'MEASURE'); ?>
		<?php echo form_hidden('fk_vehicle_id', $fk_vehicle_id); ?>
		<?php echo form_fieldset_close(); ?>
	
	<?php endif; ?>
	<?php echo form_submit('submit', 'Submit'); ?>
</div>
