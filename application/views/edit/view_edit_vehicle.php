<div class="content" id="measurements_vehicle">

<?php if(isset($vehicle) && $vehicle != '') : ?>	
	<!-- Vehicle information -->
		<h2 class="title">
			<div class="row">
				<div class="col-md-4">
					<?=$vehicle['manufacturer'] . ' ' . $vehicle['model'] . '&mdash;' . $vehicle['year'];?>
				</div>
				<div class="col-md-8" id="mini_nav">
						<ul class="nav nav-pills">
				 			<li class="active"><a href="" class="mini_nav_button">Manage Files</a></li>
				  			<li><?php echo anchor('edit/vehicle_add_comp?id='.$vehicle['pk_vehicle_id'],'Edit Vehicle details and add Components', array('class'=>'mini_nav_button')); ?></li>
				  			<li><?php echo anchor('edit/vehicle_add_measure?id='.$vehicle['pk_vehicle_id'],'Add Measurements', array('class'=>'mini_nav_button')); ?></li>
						</ul>
				</div>
			</div>
		</h2>
		
	<?php echo $msg_noise.' '.$msg_transfer.' '.$msg_image;?>
	<?php echo $msg_readme; ?>
	
	<!-- Photo Gallery -->
	<div id="slider" class="nivoSlider">
		<?php foreach($vehicle['images'] as $image) : ?>
			<img src="<?=base_url() . $image['url'];?>" width="100%" height="300px" />
		<?php endforeach;?>
	</div>
	<div class="row-fluid">
		<p class="note">Upload a new Vehicle Description File below.</p>
	</div>
	<?php echo form_open_multipart('edit/edit_vehicle_removed','',$hidden = array('id' => $vehicle['pk_vehicle_id'], 'pk_desc_id' => $readme['pk_desc_id'])); ?>
	
	<div class="row-fluid">
		<div class="col-md-3">
			<?=form_upload('readme');?>
		</div>
		<?php $this->load->model('model_readme');?>
		<?php $r_array = $this->model_readme->check_verify_readme($readme['pk_desc_id']); ?>
		<?php if( !empty($r_array) ):?>
			<?php $verified = $r_array['verified'];
				  $reject = $r_array['reject']; ?>
					
			<?php if($verified  == 0 && $reject == 0): ?>
				<div class="col-md-2">
					<div class="label label-default await_text">awaiting verification</div>
				</div>
			<?php elseif($verified  == 0 && $reject == 1):?>
				<div class="col-md-2">
					<div class="label label-danger reject_text">content rejected</div>
				</div>
			<?php endif; ?>
			
		<?php else: ?>
			<div class="erros_pub">Error: Readme File could not be found.</div>
		<?php endif; ?> 
	
		<div class="col-md-3">
		<?php 	echo form_submit(array('name'=>'submit', 'value'=>'Submit', 'class'=>'btn btn-primary', 'id' => 'submit_readme'));
				echo form_close();?>
		</div>	
	</div>
	<!-- Vehicle measurements -->
	<div class="row-fluid">
	<p class="note">Click on a component name to replace or delete the associated noise or transfer file.<br> 
		Check the checkboxes beside the file name you want to delete then click on "Delete Selected" button below to delete multiple files at once.</p>
	</div>
	

	<div class="data">
		<form action="edit_vehicle_removed" id="deleteForm" method="post">
			<!-- Separating Noise files and transfer functions for easier understanding-->
			<div class ="row">
				<div class="col-md-2">
					<div class="panel panel-danger" id="panel_manage_files_checkbox">
						<div class="panel-heading"  id="list_small">Delete</div>
						<ul class="list-group">
							<?php foreach($vehicle['images'] as $image): ?>
								<?php if($this->model_edit->isImageFile($vehicle['pk_vehicle_id'],$image['pk_image_id'])== TRUE):
								//Add checkboxes for multiple delete option
									$id = $image['pk_image_id'];
									$vehicle_id = $vehicle['pk_vehicle_id'];?>
						 			<li class="list-group-item" id="item_checkbox">
										<?php echo form_checkbox(array('name'=>'im[]','value' =>$image['pk_image_id'], 'class' => 'checkbox_edit'));?>
									</li>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
				<div class="list-group col-md-4" id="panel_manage_files">
	  				<div class="list-group-item active">
	    				Image Files
	  				</div>
					<?php foreach($vehicle['images'] as $image): ?>
						<?php if($this->model_edit->isImageFile($vehicle['pk_vehicle_id'],$image['pk_image_id'])== TRUE):
						//Add checkboxes for multiple delete option
							$id = $image['pk_image_id'];
							$vehicle_id = $vehicle['pk_vehicle_id'];?>	
							<?php echo anchor("edit/replace_comp_measure?type=IMAGE&id=$id&vehicle=$vehicle_id",substr($image['url'],20),array('class' => 'list-group-item'));?>
						<?php endif;?>
					<?php endforeach;?>
				</div>
				<div class="col-md-2">
					<div class="panel panel-primary" id="panel_manage_files_status">
  					<!-- Default panel contents -->
  						<div class="panel-heading"  id="list_small">Status</div>
  						<ul class="list-group">
							<?php foreach($vehicle['images'] as $image): ?>
								<?php if($this->model_edit->isImageFile($vehicle['pk_vehicle_id'],$image['pk_image_id'])== TRUE):
									$id = $image['pk_image_id'];
									$vehicle_id = $vehicle['pk_vehicle_id'];?>
									<li class="list-group-item">
										<?php if($image['verified'] == 0 && $image['reject'] == 0): ?>
											<span class="label label-default await_text">awaiting verification</span>
										<?php elseif($image['verified'] == 0 && $image['reject'] == 1):?>
											<span class="label label-danger reject_text">content rejected</span>
										<?php else: ?>
											<span class="text_placeholder">.</span>
										<?php endif; ?>	
									</li>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-2">
					<div class="panel panel-danger" id="panel_manage_files_checkbox">
						<div class="panel-heading"  id="list_small">Delete</div>
						<ul class="list-group">
							<?php foreach($vehicle['components'] as $component): ?>
								<?php if($this->model_edit->isNoiseFile($vehicle['pk_vehicle_id'],$component['pk_component_id'])== TRUE):
								//Add checkboxes for multiple delete option
									$id = $component['pk_component_id'];
									$vehicle_id = $vehicle['pk_vehicle_id'];?>
									<li class="list-group-item" id="item_checkbox">
										<?php echo form_checkbox(array('name'=>'noise[]','value'=>$component['pk_component_id'],'class'=>'checkbox_edit'));?>
									</li>
								<?php endif; ?>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
				<div class="list-group col-md-4" id="panel_manage_files">
	  				<div class="list-group-item active">
	    				Noise Files
	  				</div>
					<?php foreach($vehicle['components'] as $component): ?>
						<?php if($this->model_edit->isNoiseFile($vehicle['pk_vehicle_id'],$component['pk_component_id'])== TRUE):
						//Add checkboxes for multiple delete option
							$id = $component['pk_component_id'];
							$vehicle_id = $vehicle['pk_vehicle_id'];?>
							<?php echo anchor("edit/replace_comp_measure?type=COMPONENT&id=$id&vehicle=$vehicle_id",$component['name'],array('class' => 'list-group-item'));?>
						<?php endif;?>
					<?php endforeach;?>
				</div>
				<div class="col-md-2">
					<div class="panel panel-primary" id="panel_manage_files_status">
  					<!-- Default panel contents -->
  						<div class="panel-heading"  id="list_small">Status</div>
  						<ul class="list-group">
							<?php foreach($vehicle['components'] as $component): ?>
								<?php if($this->model_edit->isNoiseFile($vehicle['pk_vehicle_id'],$component['pk_component_id'])== TRUE):
									$id = $component['pk_component_id'];
									$vehicle_id = $vehicle['pk_vehicle_id'];?>
									<li class="list-group-item">
										<?php if( $component['verified'] == 0 && $component['reject'] == 0): ?>
											<div class="label label-default await_text">awaiting verification</div>
										<?php elseif($component['verified'] == 0 && $component['reject'] == 1): ?>
											<div class="label label-danger reject_text">content rejected</div>
										<?php else: ?>
											<span class="text_placeholder">.</span>
										<?php endif;?>
									</li>
								<?php endif;?>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
			</div>
		
		<!-- Transfer Function Section-->

			<div class="page-header" >
				<h3 id="title_edit_measurement">Transfer Functions</h3> 
			</div>

		<div class="row-fluid">			
		<?php foreach($vehicle['components'] as $component) : ?>
			<div class="component_edit">
				<?php $id = $component['pk_component_id'];
						$vehicle_id = $vehicle['pk_vehicle_id']; ?>
						
				<!-- COMPONENTS NAME-->
				<div class="row">
					<div class="col-md-4">
				<h4><?php echo anchor("edit/replace_comp_measure?type=COMPONENT_NAME&id=$id&vehicle=$vehicle_id",$component['name'],array('class' => 'replace_link'));?></h4>
					</div>
				<!-- COMPONENTS STATUS-->
				<div class="col-md-6">
				<?php if($component['verified'] == 0 && $component['reject'] == 0): ?>
					<div class="label label-default await_text_comp">awaiting verification</div>
				<?php elseif($component['verified'] == 0 && $component['reject'] == 1): ?>
					<div class="label label-danger reject_text_comp">content rejected</div>
				<?php endif; ?>
				</div>
				</div>
				<!-- LIST OF MEASUREMENTS ASSOCIATED WITH THE COMPONENT	-->
				
				<ul class="measurements_edit">
					<?php if(isset($vehicle['measurements'])) : foreach($vehicle['measurements'] as $measurement) : ?>
						<?php if($measurement['fk_componentA_id'] == $component['pk_component_id']) : ?>
							<?php if($measurement['url'] != NULL) :?>
								<li>
									<?php if($this->model_edit->isTransferFile($vehicle['pk_vehicle_id'],$measurement['pk_measurement_id'])== TRUE): ?>
											<!-- Add checkboxes for multiple delete option -->
											<?php $id = $measurement['pk_measurement_id'];
											$vehicle_id = $vehicle['pk_vehicle_id'];
											$compA = $measurement['fk_componentA_id'];
											$compB = $measurement['fk_componentB_id'];?>
											<?php echo form_checkbox(array('name' => 'transfer[]', 'value' => $measurement['pk_measurement_id'], 'class'=>'checkbox_edit_measurements'));?>
											<?php echo anchor("edit/replace_comp_measure?type=MEASURE&id=$id&vehicle=$vehicle_id&compA=$compA&compB=$compB",$vehicle['components'][$measurement['fk_componentB_id']]['name'],array('class' => 'replace_link')); ?>
											<?php if($measurement['verified'] == 0 && $measurement['reject'] == 0): ?>
												<div class="label label-default await_text_measure">awaiting verification</div>
											<?php elseif($measurement['verified'] == 0 && $measurement['reject'] == 1): ?>
												<div class="label label-danger reject_text_measure">content rejected</div>
											<?php endif; ?>
									<?php endif; ?>
								</li>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; endif; ?>
				</ul>
			</div>
		<?php endforeach; ?>
		</div>
		<div class="row-fluid">	
			<?php echo form_hidden('id', $vehicle['pk_vehicle_id']); ?>
			<?php echo form_submit(array('name' => 'submitDelete', 'value' => 'Delete Selected', 'class' => 'btn btn-danger btn-lg'));?>
		</div>
		</form>
<?php else : ?>
	<?php echo $msg;?>
	<p>Vehicle doesn't exist. <a href="<?=site_url();?>measurements">Back</a></p>
<?php endif; ?>

</div>
</div>