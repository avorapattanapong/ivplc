<!DOCTYPE html>
<html>
	
	<div class="container">
	<div class="nav_edit">
		<h2 class="head_title" id="nav_title"> Edit Your Profile</h2>
		<ul class="nav nav-tabs custom">
			<li class="<?=($editPane == 'userinfo') ? 'active' : '';?>" ><a id="edit_nav" href="<?=site_url();?>edit">Personal Information</a></li>
			<li class="<?=($editPane == 'vehicle') ? 'active' : '';?>" ><a id="edit_nav" href="<?=site_url();?>edit/list_vehicle">Vehicle</a></li>
			<li class="<?=($editPane == 'publications') ? 'active' : '';?>" ><a id="edit_nav" href="<?=site_url();?>edit/edit_publications_list">Publications</a></li>
		</ul>
		</div>
		</div>
</html>