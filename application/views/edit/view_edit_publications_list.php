<div class="content" id="publications">
	<h2 class="title">Publications</h2>
	<?php echo $msg;?>
	
	<div class="container">
		<div class="row">
			<p class="note">Click on a publication to begin edting. </br>
							Check in the checkboxes beside a publication you want to delete. </br>
							Note: Publications awaiting verification or rejected will not be available for editing.
			</p>
		</div>
		
		
		<?php if( $publications[0]!= NULL || $awaiting_approval_pub[0] != NULL || $rejected_pub[0] != NULL): ?>
			<form action="edit_publications_list" method="post">
				<!-- Verified Publication-->
				<div class="row">
					<?php foreach($publications as $pub) : ?>
						<?php if($pub['fk_contributor_id'] == $this->session->userdata('id')):?>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">	
											<a class="edit_publication" href="edit_publications?id=<?php echo $pub['pk_pub_id'];?>">
												<div class="icon"></div>
												<h5><?=$pub['title'];?></h5>
												<p class="affiliation"><?=$pub['affiliation'];?> &mdash; Published: <?=$pub['date'];?></p>
												<p class="author"><?=implode($pub['authors'], ', ');?></p>
											</a>
										</div>
										<?php echo form_checkbox(array('name' => 'del_pub[]', 'value' => $pub['pk_pub_id'], 'class' => 'delete_pub'));?>
									</div>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
					
					<!-- PUBS AWAITING VERIFICATION-->
					<?php foreach($awaiting_approval_pub as $unpub) : ?>
						<?php if($unpub['fk_contributor_id'] == $this->session->userdata('id')):?>	
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">	
											<div class="unpublication"href="edit_publications?id=<?php echo $unpub['pk_pub_id'];?>">
												<div class="icon"></div>
												<h5><?=$unpub['title'];?></h5>
												<p class="affiliation"><?=$unpub['affiliation'];?> &mdash; Published: <?=$unpub['date'];?></p>
												<p class="author"><?=implode($unpub['authors'], ', ');?></p>
											</div>
											<div class="label label-default await_text_pub">awaiting verification</div>
											<?php echo form_checkbox(array('name' => 'del_pub[]', 'value' => $unpub['pk_pub_id'], 'class' => 'delete_pub'));?>
										</div>
									</div>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
			
					<?php foreach($rejected_pub as $reject_pub) : ?>
						<?php if( $reject_pub['fk_contributor_id'] == $this->session->userdata('id')):?>	
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="container">	
											<div class="unpublication"href="edit_publications?id=<?php echo  $reject_pub['pk_pub_id'];?>">
												<div class="icon"></div>
												<h5><?= $reject_pub['title'];?></h5>
												<p class="affiliation"><?= $reject_pub['affiliation'];?> &mdash; Published: <?= $reject_pub['date'];?></p>
												<p class="author"><?=implode( $reject_pub['authors'], ', ');?></p>
											</div>
											<?php echo form_checkbox(array('name' => 'del_pub[]', 'value' =>  $reject_pub['pk_pub_id'], 'class' => 'delete_pub'));?>
											<div class="label label-danger reject_text_pub">publication rejected</div>
										</div>
									</div>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach;?>
				</div>
				<div class="row">
					<?php echo form_submit(array('name' => 'submit','value' => 'Delete Selected', 'class' => 'btn btn-danger'))?>
				</div>
			</form>
		<?php else: ?>
			<div class="row">
				<p> No Publications Available. </p>
			</div>
		<?php endif; ?>
	</div>
</div>