<div class="content" id="home">

	<h2 class="title" id="title">Welcome</h2>

	<!-- Introduction Paragraph -->
	<div class="row">
		<div class="col-lg-6" id="intro">
				<p>Welcome to the homepage of In-Vehicle Power Line Communication project at the University of British Columbia.</p>
				<p>The site provides open-source access to channel measurement data from this project. We also invite other researchers to upload and post their measurements at this site to help create a rich and publicaly available repository for channel measurements for power line communications in vehicles. </p>
		 		<p>To begin, please proceed to navigation bar above to view any verified submission or login/register to start submitting your own work.</p>
		</div>
			
		<div class="col-lg-6" id="car">
			<!-- Home page image -->
				<img src="<?=base_url();?>/resources/styles/images/car_and_people.jpg" height="300"></img>
		</div>
	</div>
</div>