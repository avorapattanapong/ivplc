<div class="content" id="groups">

	<!-- List Supervisors -->
	<div id="supervisors">
		<h2 class="title">Supervisors</h2>
		
		<?php foreach($supervisors as $member) : ?>

			<div class="member">
				<div class="mask">
					<img src="<?=base_url() . substr($member['url'],1);?>"/>
				</div>
				
				<div class="info">
					<div class="details">
						<h5><?=$member['name'];?></h4>
						<div id="email">
							<a href="mailto:<?=$member['email'];?>"><?=$member['email'];?></a></h3>
						</div>
					</div>
					<p><?=$member['bio'];?></br></p>
				</div>
			</div>
		<?php endforeach;?>
	
	</div>


	<!-- List Research Assistants -->
	<div id="assistants">
		<h2 class="title">Research Assistants</h2>
		
		<?php foreach($assistants as $member) : ?>
			<div class="member">
				<div class="mask">
					<?php if($member['url'] != NULL): ?>
						<img src="<?=base_url() . substr($member['url'],1);?>"/>
					<?php else: ?>
						<img src="<?=base_url() . '/resources/styles/defaults/groups.png';?>"/>
					<?php endif; ?>
				</div>
				
				<div class="info">
					<div class="details">
						<h5><?=$member['name'];?></h5>
						<div id="email">
							<a href="mailto:<?=$member['email'];?>"><?=$member['email'];?></a></h3>
						</div>
					</div>
					<?php if($member['bio']) : ?>
						<p><?=$member['bio'];?></br></p>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach;?>
	</div>
</div>