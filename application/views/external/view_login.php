<!DOCTYPE html>
<html lang="en">
		<div class="content">
			<h2 class="title" id="title">Members Login</h2>
			
			<div class="errors_pub">
			<?php echo $msg; ?>
			<?php echo validation_errors(); ?>
			</div>
			<!--print errors if form validation is not pass(wrong password or ommiting required fields-->
				
			<?php echo form_open('external/login_validation', array("class"=>"form-horizontal") ); ?>
			<div class="form-group" >
				<?php echo form_label('Email', 'email',array('class'=>'col-lg-2 control-label', 'id' => 'login_username')); ?>
				<div class= "col-lg-10">
					<?php echo form_input(array('name' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'id' => 'login_username' ));?>
				</div>
			</div>
			<div class="form-group" >	
				<?php echo form_label('Password', 'password', array('class'=> 'col-lg-2 control-label','id' => 'login_password')); ?>
					<div class= "col-lg-10">
						<?php echo form_password(array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'id' => 'login_password' )); ?>
					</div>
			</div>
			<div class = "form-group">
				<div class="col-lg-offset-2 col-lg-10">	
					<?php echo form_submit(array('name'=>'submit', 'value'=>'Login', 'class'=> 'btn btn-default', 'id' => 'login')); ?>
				</div>		
			</div>
			<div class="form-group">
				<div class="col-lg-offset-2 col-lg-10">
					<?php echo anchor('ctrl_members/registration', 'Click here to register',array('class'=> 'btn btn-default', 'id' => 'register'));?>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
</html>