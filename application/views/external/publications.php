<div class="content" id="publications">
	<h2 class="title">Publications</h2>

	
	<?php foreach($publications as $pub) : ?>	
		<?php $tempA = $this->publications->return_verified_authors($pub['pk_pub_id']);?>
		<a class="publication ext" href="<?=$pub['url'];?>">
			<div class="icon"></div>

			<h5><?=$pub['title'];?></h5>
			
			<p class="affiliation"><?=$pub['affiliation'];?> &mdash; Published: <?=$pub['date'];?></p>
	
			<p class="author"><?=implode($tempA, ', ');?></p>
		</a>
	<?php endforeach;?>
</div>