<div class="content" id="submit_chooser">

	<h2 class="title" id="title">Submission</h2>

	<?php if($this->session->userdata('isAdmin') == 1){
				$fname = $this->session->userdata('name');
			}
			else {
				$fname = $this->session->userdata('fname');
			}?>
	<?php if(isset($message) && $message != '') : ?>
		<?php if(isset($error_msg) && $error_msg != ''): ?>
	
			<?php foreach($error_msg as $error): ?>
				<div class="errors_sub"><?php echo $error;?></div>
			<?php endforeach; ?>
		<?php endif ?>
		<h3><?=$message;?></h3>
	<?php else : ?>
		<h3>Welcome back, <?=$fname;?>!</h3>
	<?php endif; ?>

	<?php if($verified == 'TRUE'):?>
		<h3>What would you like to submit?</h3>
		<div id="selections">
			<a id="vehicle" href="<?=site_url();?>submit/vehicle">
				<span class="text">Submit Vehicle</span>
				<span class="icon" id="vehicle"></span>
			</a>
			<a id="publication" href="<?=site_url();?>submit/publication">
				<span class="text">Submit Publication</span>
				<span class="icon" id="publication"></span>
			</a>
		</div>
	<?php elseif($verified == 'FALSE'):?>
		<h5>Before you can submit any work, please allow 5 to 7 days for your account approval. </h5>
	<?php elseif($verified == 'ADMIN'):?>
		<h5>Please login as a contributor to submit. </h5>
	<?php endif; ?>
	
	
</div><!-- end of Content // Begin Footer File-->