<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class External extends CI_Controller {

	function __construct(){
	    parent::__construct();
	}
	
	public function index(){
		$data = array(
			'parent' => 'home',
			'page' => 'home-index',
			'title' => 'Home Index'
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('external/index', $data);
		$this->load->view('footer', $data);
	}
	
	public function group(){		
		$data = array(
			'parent' => 'group',
			'page' => 'group',
			'title' => 'Group',
			'supervisors' => $this->group->return_supervisors(),
			'assistants' => $this->group->return_assistants()
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('external/group', $data);
		$this->load->view('footer', $data);
	}
	
	public function publications(){
		$data = array(
			'parent' => 'publications',
			'page' => 'publications',
			'title' => 'Publications',
			'publications' => $this->publications->return_publications()
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('external/publications', $data);
		$this->load->view('footer', $data);
	}
	
	/* AUTHENTICATE */
	public function login(){
		//loads login page: linked from nav
		$data = array(
			'parent' => 'login',
			'page' => 'login',
			'title' => 'Login',
			'msg' => ''
		);
		
		if(isset($_GET['success'])){
			if($_GET['success'] == 'TRUE'){
				$data['msg'] = 'Registration successfully completed. Please login to begin.';
			}
			else if($_GET['success'] == 'FALSE'){
				$data['msg'] = 'Registration failed. Please try again';
			}
		}
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('external/view_login', $data);
		$this->load->view('footer', $data);
		
	}

	public function login_error(){
		//loads login page: linked from nav
		$data = array(
			'parent' => 'login',
			'page' => 'login',
			'title' => 'Login',
			'msg' => 'Please Login or Register Before Proceeding'
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('external/view_login', $data);
		$this->load->view('footer', $data);
	}
	
	function login_validation(){
		//validate input and run password check callback
		$this->load->model('model_login_authenticate');
		$this->load->model('model_edit');
		
		$this->form_validation->set_rules('email','Email', 'required|trim|xss_clean|callback_login_authen');
		$this->form_validation->set_rules('password','Password','required|md5|trim');

		if( $this->form_validation->run()){
			$data = $this->model_edit->get_all_info_login($this->input->post('email'));
			if(empty($data)){	
				$data = $this->model_edit->get_all_infoAdmin_login($this->input->post('email'));
				//Grab Admin info
				if( $data['supervisor'] == 1){
					$data_session = array(
						'email' 		=> $this->input->post('email'),
						'id'			=> $data['pk_group_id'],
						'name'			=> $data['name'],
						'isSuper'		=> $data['supervisor'],
						'is_logged_in'  => '1',
						'isAdmin'		=> '1'
					
					);
					$this->session->set_userdata($data_session);
					redirect('/admin');
				}
				else{
					//Only users from the group that has supervisor priviledges can login
					//Non supervisor 'group' users must register as a contributor
					$data = array(
					'parent' => 'login',
					'page' => 'login',
					'title' => 'Login',
					'msg' => 'Please login as an Administrator, Contributor or register before proceeding.'
					);
					
					$this->load->view('header', $data);
					$this->load->view('nav', $data);
					$this->load->view('external/view_login');
					$this->load->view('footer', $data);
				}
			}
			else{				
				$data_session = array(
					'email' 		=> $this->input->post('email'),
					'id'			=> $data['pk_contributor_id'],
					'fname'			=> $data['first_name'],
					'lname'			=> $data['last_name'],
					'is_logged_in'  => '1',
					'isAdmin'		=> '0'
				
				);
				$this->session->set_userdata($data_session);
				redirect('members');
			}
		}
		else{
			$data = array(
				'parent' => 'login',
				'page' => 'login',
				'title' => 'Login',
				'msg' => ''
			);
			
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('external/view_login');
			$this->load->view('footer', $data);
			
		}
		
	}

	function login_authen(){
		//runs authentication model
		$this->load->model('model_login_authenticate');
		
		if($this->model_login_authenticate->login_correct()){
			return TRUE;			
		}
		else {
			$this->form_validation->set_message('login_authen','Incorrect login information.');
			return FALSE;
		}
	}

	
}