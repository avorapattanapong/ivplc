<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submissions extends CI_Controller {

	function __construct(){
	    parent::__construct();
	}
	public function index(){
		if($this->session->userdata('is_logged_in') != 1){
			redirect('login_error');
		}
		else {
			$this->submit_logged();
		}
			
		
	}	
	function submit_logged(){
		$data = array(
			'parent' => 'submit',
			'page' => 'submit',
			'title' => 'Submit'
		);
		/*
		if($this->input->post()) :
			$email = $this->input->post('email');
			$this->session->set_userdata('email', $email);
			
			$contributor = $this->contributors->check_email($email);
			$verified = $this->contributors->check_verified($email);
			
			if($contributor) :
				if($verified) : 
					$this->session->set_userdata('contributor', $contributor['pk_contributor_id']);
					$this->session->set_userdata('verified', 'true');
					redirect('submit/chooser');  //verified contributor
				else :			
					$this->session->set_userdata('contributor', $contributor['pk_contributor_id']);
					$this->session->set_userdata('verified', 'false');
					redirect('submit/vehicle');  //unverified contributor
				endif;
			else :				
				$this->session->set_userdata('email', $email);
				redirect('submit/personal');     //new contributor
			endif;
		endif;
		*/
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('submissions/index', $data);
		$this->load->view('footer', $data);
	}
	
	
	
	public function submit_chooser(){
			$data = array(
				'parent' => 'submit',
				'page' => 'chooser',
				'title' => 'Submit Chooser',
				//'contributor' => $this->session->userdata('contributor'),
				'name' => $this->session->userdata('fname')
			);
			$verified = 'TRUE';
			if($this->session->userdata('isAdmin') == 0){
				$contributor = $this->contributors->return_contributor($this->session->userdata('id'));
				if($contributor[0]['verified'] == 0){
					$verified = 'FALSE';
				}
			}
			else{
				$verified = 'ADMIN';
			}
			$data['verified'] = $verified;
			$data['error_msg'] = array();			

			if(isset($_GET['success'])){
				//Setting the correct message through GET 
					if($_GET['success']== 'TRUE'){
						$data['message'] = 'Thank you for your submission, please allow 5 to 7 days for approval.</br>Would you like to submit something else?';
					}
					else if( $_GET['success'] == 'FALSE'){
						$data['message'] = 'Error in submission. Please try again.';
					}
			}
			else{
				$data['message'] = '';
			}
			if( isset($_GET['img']) || isset($_GET['nse']) || isset($_GET['trn']) || isset($_GET['rdm'])){
				$ret_err = array();
				//retrieves error array
				 $error["image"] = $_GET['img'];
				 $error["noise"] = $_GET['nse'];
				 $error["transfer"] = $_GET['trn'];
				 $error["readme"] = $_GET['rdm'];
				 
			
				//IMAGE ERRORS
				if($error["image"] != '' && $error["image"] != 1){
					$ret_err["image"] = $error["image"];
				}
				if($error["noise"] != '' && $error["noise"] != 1){
					$ret_err["noise"] = $error["noise"];
				}
				if($error["transfer"] != '' && $error["transfer"] != 1){
					$ret_err["transfer"] = $error["transfer"];
				}
				if($error["readme"] != '' && $error["readme"] != 1){
					$ret_err["readme"] = $error["readme"];
				}

				 $data['error_msg'] = $ret_err;
				
			}
			
			
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('submissions/chooser', $data);		
			$this->load->view('footer', $data);

	}
	
	public function submit_vehicle(){		
		$this->form_validation->set_rules('manufacturer', 'Manufacturer', 'required');
		$this->form_validation->set_rules('model', 'Model', 'required');
		$this->form_validation->set_rules('year', 'Year', 'required|min_length[4]|numeric');
		$this->form_validation->set_rules('component_name[]', 'Component Name', 'required');
		$this->form_validation->set_rules('agreement', 'Agreement', 'required');
		
		$this->load->model('model_readme');
		$this->load->model('model_upload_validation');

		//if($this->session->userdata('contributor')) :
			$data = array(
				'parent' => 'submit',
				'page' => 'vehicle',
				'title' => 'Submit Vehicle',
				'contributor' => $this->session->userdata('id'),
				'manufacturers' => $this->manufacturers->return_manufacturers(),
				'file' => $this->session->userdata('file'),
			);
			
			if($this->form_validation->run()==FALSE) :
				$this->load->view('header', $data);
				$this->load->view('nav', $data);
				$this->load->view('submissions/vehicle', $data);		
				$this->load->view('footer', $data);
			else :
				if($this->input->post()) :
					$post = $this->input->post();
					
					$err_img = $this->model_upload_validation->validate_images(FALSE);
					$err_nse = $this->model_upload_validation->validate_noise(FALSE);
					$err_trn = $this->model_upload_validation->validate_transfer(FALSE);
					$err_rdm = $this->model_upload_validation->validate_readme(FALSE);
					if( $err_img != 1 ||$err_nse != 1 || $err_trn != 1 || $err_rdm != 1 ){
						redirect("submit/chooser?success=FALSE&img=$err_img&nse=$err_nse&trn=$err_trn&rdm=$err_rdm");
					}
					$vehicle = $this->vehicles->create_vehicle($post);
		
					$img = $this->file_parser->images_parser();
					$cmp = $this->file_parser->components_parser();
					$msr = $this->file_parser->measurements_parser();
					$readme['readme']['name'] =  $_FILES['readme']['name'];
					$readme['readme']['type'] =  $_FILES['readme']['type'];
					$readme['readme']['size'] =  $_FILES['readme']['size'];
					$readme['readme']['tmp_name'] =  $_FILES['readme']['tmp_name'];
					
					$rd = $this->model_readme->upload_readme($vehicle, $readme);
					$images = $this->images->upload_images($vehicle, $img);
					$components = $this->components->upload_components($vehicle, $cmp, $post['component_name']);
					$measurements = $this->measures->upload_measurements($vehicle, $msr, $components);

					
					if($vehicle) :
					//Setting Get for right message
						redirect("submit/chooser?success=TRUE&msg=$msg");
					else :
						redirect("submit/chooser?success=FALSE&msg=$msg");
					endif;

				endif;
			endif;
		//else :
			//redirect('submit');
		//endif;
					  
					 
	}
	
	public function submit_publication(){		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('affiliation', 'Publisher', 'min_length[2]|');
		$this->form_validation->set_rules('date', 'Publication year', 'min_length[4]|numeric');
		$this->form_validation->set_rules('url', 'URL', 'required|prep_url');
		$this->form_validation->set_rules('author_first[]', 'First Initial', 'required');
		$this->form_validation->set_rules('author_last[]', 'Last Name', 'required|alpha_dash');
		$this->form_validation->set_rules('agreement', 'Agreement', 'required');	
		
		
		$data = array(
			'parent' => 'submit',
			'page' => 'publications',
			'title' => 'Submit Publication',
			'contributor' => $this->session->userdata('id')
		);
		
		if($this->form_validation->run()==FALSE) :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('submissions/publication', $data);		
			$this->load->view('footer', $data);
		else :
			if($this->input->post()) :
				//Checks if publication is successfully submitted
				$post = $this->input->post();
				$publication = $this->publications->create_publication($post);
				$authors = $this->publications->create_authors($post, $publication);
				
				if($publication) :
					//Setting Get for right message
					redirect('submit/chooser?success=TRUE');
				else :
					redirect('submit/chooser?success=FALSE');
				endif;

				
			endif;
		endif;
	}
		
}