<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit extends CI_Controller {

	function __construct(){
	    parent::__construct();
	}
	
	public function index(){
		if($this->session->userdata('isAdmin') != 1){
			$this->form_validation->set_rules('fname', 'First Name', 'alpha|trim');
			$this->form_validation->set_rules('lname', 'Last Name', 'alpha|trim');
			$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|xss_clean');
			$this->form_validation->set_rules('affiliation', 'Affiliation', 'alpha|trim');
			$this->form_validation->set_rules('city', 'City', 'alpha|trim');
			$this->form_validation->set_rules('country', 'Country', 'alpha|trim');
			$this->form_validation->set_rules('password', 'Password', 'trim');
			if(isset($_POST['password']) && !empty($_POST['password'])){
				$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
			}
		}
		else{
			$this->form_validation->set_rules('name', 'Name', 'trim');
			$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim');
			if(isset($_POST['password']) && !empty($_POST['password'])){
				$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
			}
		}
		
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-index',
			'title' => 'Edit Your Profile',		//Title on tabs
			'editPane' => 'userinfo'
		);
		$this->load->model('model_edit');
		$this->load->helper('security');
		
		if($this->session->userdata('isAdmin') == 1){
			//admin case
			$userData = $this->model_edit->get_all_infoAdmin($this->session->userdata('id'));
		}
		else{
			//other user case
		$userData = $this->model_edit->get_all_info($this->session->userdata('id'));
		}
		
		$userData['msg'] = '';
		if(isset($_GET['success'])){
			if($_GET['success'] == 'TRUE'){
				$userData['msg'] = 'Update Successful.';
			}
			else if($_GET['success'] == 'FALSE'){
				$userData['msg'] = 'Update Failed.';
			}
			else if($_GET['success'] == 'EMPTY'){
				$userData['msg'] = 'All fields are empty. No Update.';
			}
		}
		
		if($this->form_validation->run()==FALSE){
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('edit/view_edit_nav', $data);
			$this->load->view('edit/index',$userData);
			$this->load->view('footer', $data);
		}
		else {
			$success = 'EMPTY';
			if( !empty($_POST['email']) || !empty($_POST['name']) || !empty($_POST['password']) || !empty($_POST['fname']) 
			|| !empty($_POST['lname']) || !empty($_POST['affiliation']) || !empty($_POST['city']) 
			|| !empty($_POST['country'])){
				if($this->session->userdata('isAdmin') != 1){
					//member update user info
					$form_userinfo_data = array (
						//'pk_contributor_id'			=> NULL,
						'email'						=> $this->input->post('email'),
						'first_name'				=> $this->input->post('fname'),
						'last_name'					=> $this->input->post('lname'),

						'affiliation'				=> $this->input->post('affiliation'),
						'city'						=> $this->input->post('city'),
						'country'					=> $this->input->post('country')
						//'verified'					=> NULL
					);
					if(!empty($_POST['password'])){
						$form_userinfo_data['password']	= do_hash($this->input->post('password'), 'md5');
					}
					if($this->model_edit->update_userinfo($form_userinfo_data, $this->session->userdata['id'])!=-1){
						//if data is updated correctly
						$session_data = array();
						if(isset($_POST['fname']) && $_POST['fname'] != ''){
							$session_data['fname'] = $this->input->post('fname');
						}
						if(isset($_POST['lname']) && $_POST['lname'] != ''){
							$session_data['lname'] = $this->input->post('lname');
						}
						if(isset($_POST['email']) && $_POST['email'] != ''){
							$session_data['email'] = $this->input->post('email');
						}
						$this->session->set_userdata($session_data);
						
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}
				}
				else{
					//admin update userinfo
					$form_userinfo_data = array (
						//'pk_contributor_id'			=> NULL,
						'email'						=> $this->input->post('email'),
						'name'						=> $this->input->post('name')
						//'verified'					=> NULL
					);
					if(!empty($_POST['password'])){
						$form_userinfo_data['password']	= do_hash($this->input->post('password'), 'md5');
					}
					//$msg = $this->model_edit->update_userinfo_admin($form_userinfo_data, $this->session->userdata('id'));
					//if(TRUE){
					if($this->model_edit->update_userinfo_admin($form_userinfo_data, $this->session->userdata('id'))!=-1){
						//if data is updated correctly
						$session_data = array();
						if(isset($_POST['name']) && !empty($_POST['name'])){
							$session_data['name'] = $this->input->post('name');
						}
						$this->session->set_userdata($session_data);
						
						$success = 'TRUE';
					}
					else{
						$success = 'FALSE';
					}
				}

			}
			redirect("edit?success=$success");
		}
		
		
	}
/*	
	public function main_content_success(){
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-index',
			'title' => 'Edit Your Profile',		//Title on tabs
			'editPane' => 'userinfo'
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav', $data);
		$this->get_db_info_success();
		$this->load->view('footer', $data);
		
		
	}
	
	function get_db_info_success(){
		$this->load->model('model_edit');
		
		$userEmail = $this->session->userdata('email');
		$tempID = $this->model_edit->get_prime_key($userEmail);
		foreach($tempID as $row){
		
			$userData['id'] = $row->pk_contributor_id;
		}

		$tempData = $this->model_edit->get_all_info($userData['id']);
		foreach($tempData as $row){
		
			$userData['fName'] = $row->first_name;
			$userData['lName'] = $row->last_name;
			$userData['email'] = $row->email;
			$userData['userAff'] = $row->affiliation;
			$userData['userCity'] = $row->city;
			$userData['userCountry'] = $row->country;
		}
	
		$this->load->view('edit/index_success',$userData);
		
	}
	
	function submit_changes(){
		//Submits fieds enter into edit form to database
		//$this->load->library('form_validation');
		$this->load->model('model_edit');
		
		
		$this->form_validation->set_rules('fname', 'First Name', 'alpha|trim');
		$this->form_validation->set_rules('lname', 'Last Name', 'alpha|trim');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|xss_clean');
		$this->form_validation->set_rules('affiliation', 'Affiliation', 'alpha|trim');
		$this->form_validation->set_rules('city', 'City', 'alpha|trim');
		$this->form_validation->set_rules('country', 'Country', 'alpha|trim');
		
		
		$userEmail = $this->session->userdata('email');
		$tempID = $this->model_edit->get_prime_key($userEmail); //Grabs Primary Key: pk_contributor_id
		foreach($tempID as $row){
		
			$userData['id'] = $row->pk_contributor_id;
		}
		
		
		if($this->form_validation->run()){
			//if validation is passed 
			$form_userinfo_data = array (
				//'pk_contributor_id'			=> NULL,
				'email'						=> $this->input->post('email'),
				'first_name'				=> $this->input->post('fname'),
				'last_name'					=> $this->input->post('lname'),
				'affiliation'				=> $this->input->post('affiliation'),
				'city'						=> $this->input->post('city'),
				'country'					=> $this->input->post('country'),
				'verified'					=> NULL
			);
			
			if($this->model_edit->update_userinfo($form_userinfo_data, $userData['id'])!=-1){
				//if data is updated correctly
				$session_data = array(
					'fname' => $this->input->post('fname'),
					'lname' => $this->input->post('lname'),
				);

				$this->session->set_userdata($session_data);
				$this->main_content_success();
			}
			else {
				//else display database error
				$this->load->view('edit/view_database_error_update');
			}
			
		}
		else 
			$this->get_db_info(); //if not validated redirect to same page with validation error
	}
	*/
	function list_vehicle(){
		//List all contributed vehicles
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle List',		//Title on tabs
			'editPane' => 'vehicle',
			'msg' => ''
		);

		$this->load->model('model_edit');
		
		if(isset($_POST['del_vehicle'])){
			if(!empty($_POST['del_vehicle'])){
				$del_pub = $this->model_edit->remove_vehicles($_POST);
				if($del_pub){
					$data['msg'] = 'Delete Successful.';
				}
				else{
					$data['msg'] = 'Delete Failed.';
				}
			}
		}
		
		$vehicles['cars'] = $this->model_edit->search_vehicle_list($this->session->userdata('id'));
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav', $data);
		$this->load->view('edit/view_edit_vehiclelist',$vehicles);
		$this->load->view('footer', $data);
	}
	function edit_vehicle(){
		//grabs existing vehicle files chosen through list_vehicle and allow edits
		//Note: There are 2 methods I used throughout the whole project:
		//This way with two functions and another with one. This is the only one with the first method
		//With time constraints, I could not edit this in time. FYI
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle',		//Title on tabs
			'editPane' => 'vehicle'
		);
		$this->load->model('model_edit');
		$this->load->model('model_readme');
		$vehicle_list = $this->model_edit->search_vehicle_list($this->session->userdata('id'));
		//loads all vehicle data from this user
		$vehicle_editor['msg_noise'] = '';
		$vehicle_editor['msg_transfer'] = '';
		$vehicle_editor['msg_image'] = '';
		$vehicle_editor['msg_readme'] = '';
		/////////////////////////////// RETURN FROM REPLACE PAGE///////////////////////////////////
		if(isset($_GET['success']) && isset($_GET['type'])){
			if($_GET['type'] == 'IMAGE'){
				if($_GET['success'] == 'TRUE'){
					$vehicle_editor['msg_image'] = 'Image Replace Successful';
				}
				else if($_GET['success'] == 'FALSE'){
					$vehicle_editor['msg_image'] = 'Image Replace Unsuccessful';
				}
				else if($_GET['success'] == 'EMPTY'){
					$vehicle_editor['msg_image'] = 'Nothing replaced';
				}
			}
			else if($_GET['type'] == 'COMPONENT'){
				if($_GET['success'] == 'TRUE'){
					$vehicle_editor['msg_noise'] = 'Component Replace Successful';
				}
				else if($_GET['success'] == 'FALSE'){
					$vehicle_editor['msg_noise'] = 'Component Replace Unsuccessful';
				}
				else if($_GET['success'] == 'EMPTY'){
					$vehicle_editor['msg_noise'] = 'Nothing replaced';
				}
			}
			else if($_GET['type'] == 'COMPONENT_NAME'){
				if($_GET['success'] == 'TRUE'){
					$vehicle_editor['msg_noise'] = 'Component Name Replace Successful';
				}
				else if($_GET['success'] == 'FALSE'){
					$vehicle_editor['msg_noise'] = 'Component Name Replace Unsuccessful';
				}
				else if($_GET['success'] == 'EMPTY'){
					$vehicle_editor['msg_noise'] = 'Nothing replaced';
				}
			}
			else if($_GET['type'] == 'DEL_COMP'){
				if($_GET['success'] == 'TRUE'){
					$vehicle_editor['msg_noise'] = 'Component Delete Successful';
				}
				else if($_GET['success'] == 'FALSE'){
					$vehicle_editor['msg_noise'] = 'Component Delete Unsuccessful';
				}
			}
			else if($_GET['type'] == 'MEASURE'){
				if($_GET['success'] == 'TRUE'){
					$vehicle_editor['msg_transfer'] = 'Measurement Replace Successful';
				}
				else if($_GET['success'] == 'FALSE'){
					$vehicle_editor['msg_transfer'] = 'Measurement Replace Unsuccessful';
				}
				else if($_GET['success'] == 'EMPTY'){
					$vehicle_editor['msg_transfer'] = 'Nothing replaced';
				}
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////

		$vehicle_editor['vehicle'] = NULL;
		for($i = 0; $i < count($vehicle_list) ;$i++){
				if($vehicle_list[$i]['pk_vehicle_id']==$_GET["id"])
					$vehicle_editor['vehicle'] = $vehicle_list[$i];
		} 
		$vehicle_editor['readme'] = $this->model_readme->return_readme($_GET['id']);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav', $data);
		$this->load->view('edit/view_edit_vehicle',$vehicle_editor);
		$this->load->view('footer', $data);
	}
	function replace_comp_measure(){
	//Runs a page that allows an upload to one particular file to replace it
		$this->load->model('model_edit');
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle',		//Title on tabs
			'editPane' => 'vehicle'						
		);
		$id=0;
		$success = '';
		if( isset($_GET['compA']) && isset($_GET['compB'])){
			$data['compA'] = $_GET['compA'];
			$data['compB'] = $_GET['compB'];
		}
		if(isset($_GET['type']) && isset($_GET['id']) && isset($_GET['vehicle'])){
			$data['type'] = $_GET['type'];
			$data['file_id'] = $_GET['id'];
			//generic name for ids so it can be reused
			$data['fk_vehicle_id'] = $_GET['vehicle']; 
			$data['isNoise'] = '';
			if( $_GET['type'] == 'COMPONENT_NAME'){
				$data['isNoise'] = $this->model_edit->isNoiseFile($_GET['vehicle'], $_GET['id']);  
			}
			
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('edit/view_edit_nav', $data);
			$this->load->view('edit/view_replace',$data);
			$this->load->view('footer', $data);
		}
		else{
			$msg = '';
			///////////////////////////////    IMAGES    //////////////////////
			if(isset($_POST['type']) && isset($_POST['pk_image_id']) && isset($_POST['fk_vehicle_id'])){
				$vehicle_id = $_POST['fk_vehicle_id'];
				$type = $_POST['type'] ;
				$msg = 'image';
				if( $type == 'IMAGE' && isset($_FILES['image'])){
					if($_FILES["image"]["error"][0] != UPLOAD_ERR_NO_FILE){
						if($this->model_edit->remove_image($vehicle_id , $_POST['pk_image_id'])){
							$img = $this->file_parser->images_parser();
							$image = $this->images->replace_image($vehicle_id,$_POST['pk_image_id'],$img);
							$msg = $image;
							if($image){
								$success = 'TRUE';
							}
							else{
								$success = 'FALSE';
							}
						}
						else{
							$success = 'FALSE';
						}
					}
					else{
						$success = 'EMPTY';
					}
				}
			}
			///////////////////////////////    COMPONENTS    //////////////////////
			if(isset($_POST['type']) && isset($_POST['pk_component_id']) && isset($_POST['fk_vehicle_id'])){
				$vehicle_id = $_POST['fk_vehicle_id'];
				$type = $_POST['type'];
				$msg = 'component';
				if( $type == 'COMPONENT' && isset($_FILES['component'])){
					$msg = 'component2';
					if($_FILES["component"]["error"][0] != UPLOAD_ERR_NO_FILE){
						if($this->model_edit->remove_noise_no_db($vehicle_id, $_POST['pk_component_id'])){
							$cmp = $this->file_parser->components_parser();
							$comp = $this->components->upload_replace_components($vehicle_id, $cmp, $_POST['pk_component_id']);
							if($comp){
								$success = 'TRUE';
							}
							else{
								$success = 'FALSE';
							}
						}
						else{
							$success = 'FALSE';	
						}
					}
					else{
						$success = 'EMPTY';	
					}
				}
			}
			///////////////////////////////    COMPONENTS NAME    //////////////////////
			if(isset($_POST['type']) && isset($_POST['pk_component_id']) && isset($_POST['fk_vehicle_id']) && isset($_POST['component_name'])){
				$vehicle_id = $_POST['fk_vehicle_id'];
				$type = $_POST['type'];
				if( $type == 'COMPONENT_NAME'){
					$msg = 'blah';
					if(isset($_POST['submit']) && $_POST["submit"] == "Delete"){
						$type = 'DEL_COMP';
						$del = $this->components->delete_component($_POST['pk_component_id'],$vehicle_id);
						if($del){
							$success = 'TRUE';
						}
						else{
							$success = 'FALSE';
						}
						redirect("edit/edit_vehicle?success=$success&id=$vehicle_id&msg=$msg&type=$type");
						
					}
					if(isset($_FILES['component'])){
						if($_FILES["component"]["error"][0] == UPLOAD_ERR_NO_FILE && empty($_POST['component_name'])){
							$success = 'EMPTY';
						}
						else{
							if( $_FILES["component"]["error"][0] != UPLOAD_ERR_NO_FILE  ){
								$cmp = $this->file_parser->components_parser();
								$comp = $this->components->upload_replace_components($vehicle_id, $cmp, $_POST['pk_component_id']);
								if($comp){
									$success = 'TRUE';
								}
								else{
									$success = 'FALSE';
								}
							}
							if( !empty($_POST['component_name']) ){
								$comp_name = $this->components->change_comp_name($_POST['pk_component_id'], $_POST['component_name']);
								if($comp_name){
									$success = 'TRUE';
								}
								else{
									$success = 'FALSE';
								}
							}
						}
					}
					else{
						if(empty($_POST['component_name'])){
							$success = 'EMPTY';
						}
						else{
							if( !empty($_POST['component_name']) ){
								$comp_name = $this->components->change_comp_name($_POST['pk_component_id'], $_POST['component_name']);
								if($comp_name){
									$success = 'TRUE';
								}
								else{
									$success = 'FALSE';
								}
							}
						}
					}
				}
			}

			///////////////////////////////    MEASURMENTS    //////////////////////
			if(isset($_POST['type']) && isset($_POST['pk_measurement_id']) && isset($_POST['compA']) && isset($_POST['compB']) && isset($_POST['fk_vehicle_id'])){
				$vehicle_id = $_POST['fk_vehicle_id'];
				$type = $_POST['type'];
				$msg = 'blah';
				if( $type == 'MEASURE' && isset($_FILES["measurement"])){
					if($_FILES["measurement"]["error"][0] != UPLOAD_ERR_NO_FILE){
						if($this->model_edit->remove_measurement($vehicle_id, $_POST['pk_measurement_id'])){
							//original file must be deleted first in case the file extension is different
							$msr = $this->file_parser->measurements_parser();
							$measure = $this->measures->upload_replace_measurements($vehicle_id, $msr,$_POST['compA'],$_POST['compB'], $_POST['pk_measurement_id']);
							$msg = $measure;
							if($measure ){
								$success = 'TRUE';
							}
							else{
								$success = 'FALSE';
							}
						}
						else{
							$success = 'FALSE';	
						}
					}
					else{
						$success = 'EMPTY';	
					}
				}
			}
			
			redirect("edit/edit_vehicle?success=$success&id=$vehicle_id&msg=$msg&type=$type");
		}
	}

	function edit_vehicle_removed(){
		//grabs existing vehicle files chosen through list_vehicle and allow edits
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle',		//Title on tabs
			'editPane' => 'vehicle'
		);
		$this->load->model('model_edit');
		$this->load->model('model_readme');
		$vehicle_list = $this->model_edit->search_vehicle_list($this->session->userdata('id'));
		//Grab all vehicle submitted by this user from db
		$vehicle_editor['vehicle'] = NULL;
		$curr_vehicle = $_POST['id'];
		//current vehicle id we're working on
		$vehicle_editor['readme'] = $this->model_readme->return_readme($_POST['id']);
		$vehicle_editor['msg_noise'] = '';
		$vehicle_editor['msg_transfer'] = '';
		$vehicle_editor['msg_image'] = '';
		$vehicle_editor['msg_readme'] = '';
		
		
		
		
		//////////////////////////// REMOVING PROCESS /////////////////////////
		if(isset($_POST["noise"])){
			$noise = $_POST["noise"];
			foreach( $noise as $noise_id){
				if($this->model_edit->remove_noise($curr_vehicle,$noise_id)){
					$vehicle_editor['msg_noise']="Noise File Successfully Removed.";
				}
				else{
					$vehicle_editor['msg_noise']="Noise File Failed";
				}
			}
		}
		if(isset($_POST["transfer"])){
			$transfer = $_POST["transfer"];
			//$vehicle_editor['msg'] = $transfer[0];
			
			foreach( $transfer as $transfer_id){
				if($this->model_edit->remove_measurement($curr_vehicle,$transfer_id)){
					$this->measures->delete_measurement($transfer_id);
					$vehicle_editor['msg_transfer']="Transfer File Successfully Removed.";
				}
				else{
					$vehicle_editor['msg_transfer']="Transfer File Failed";
				}
			}
		}
		//$vehicle_editor['msg_image'] = "123";
		if(isset($_POST["im"])){
			$images = $_POST["im"];
			foreach( $images as $image_id){
				if($this->model_edit->remove_image($curr_vehicle,$image_id)){
					$this->images->delete_image($image_id);
					$vehicle_editor['msg_image']="Image File Successfully Removed.";
				}
				else{
					$vehicle_editor['msg_image']="Image File Failed";
				}
			}
		}
		if(isset($_FILES["readme"])){
			if($_FILES['readme']['error'] != UPLOAD_ERR_NO_FILE){

				$readme['readme']['name'] =  $_FILES['readme']['name'];
				$readme['readme']['type'] =  $_FILES['readme']['type'];
				$readme['readme']['size'] =  $_FILES['readme']['size'];
				$readme['readme']['tmp_name'] =  $_FILES['readme']['tmp_name'];
				
				$rd = $this->model_readme->upload_edit_readme($curr_vehicle,$_POST['pk_desc_id'], $readme);
				if($rd){
					$vehicle_editor['msg_image'] = 'Vehicle Description update successful';
				}
				else{
					$vehicle_editor['msg_image'] = 'Vehicle Description update unsuccessful';
				}
			}
		}
		//////////////////////////// END OF REMOVING PROCESS /////////////////////////
		
		for($i = 0; $i < count($vehicle_list) ;$i++){
			//Search for the vehicle to edit chosen by the user -> for display
				if($vehicle_list[$i]['pk_vehicle_id']==$curr_vehicle)
					$vehicle_editor['vehicle'] = $vehicle_list[$i];
		} 
		
		

		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav', $data);
		$this->load->view('edit/view_edit_vehicle',$vehicle_editor);
		$this->load->view('footer', $data);
	}
	
	function vehicle_add_comp(){
		if( !empty($_POST['component_name'])){
			//If a file is uploaded, a name is required(Coomponents)
			$this->form_validation->set_rules('component_name[]', 'Component Name', 'required');
		}
		$this->form_validation->set_rules('year', 'Year', 'min_length[4]|numeric');
		if(!empty($_POST['manufacturer']) || !empty($_POST['model']) || !empty($_POST['year'])
		 || (isset($_POST['component_name']) && !empty($_POST['component_name']))){
		 	//if any of the field is entered, users are required to agree with terms
			$this->form_validation->set_rules('agreement', 'Agreement', 'required');
		 }
		
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle',		//Title on tabs
			'editPane' => 'vehicle',
			'msg' => ''						//message to display in the event of success/fail update
		);
		
		$this->load->model('model_edit');
		$vehicle_list = $this->model_edit->search_vehicle_list($this->session->userdata('id'));
		//loads all vehicle data from this user

		$vehicle_editor['vehicle'] = NULL;
		$vehicle_editor['manufacturers'] = $this->manufacturers->return_manufacturers();
		
		if(isset($_GET['success'])&& isset($_GET['id'])){
			//if success is set then it means user has submitted some changes and are being rediredted 
			//Fail or success will print a different message on the same page
			$id = $_GET['id'];
			if($_GET['success'] == 'TRUE'){
				$data['msg'] = 'Update Successful. <br> Thank you for your submission, please allow 5 to 7 days for approval.';
			}
			else if($_GET['success'] == 'FALSE'){
				$data['msg'] = 'Update Failed';		
			}
			else if($_GET['success'] == 'EMPTY'){
				$data['msg'] = 'All fields are empty. No Update.';
			}
		}
		else if( isset($_GET['id'])){
			//If only the GET id is set then the user came from the list of publications
			$id = $_GET['id'];
		}
		else if( isset($_POST['id'])){
			//This is after user has submitted any changes
			$id = $_POST['id'];
		}
		
		for($i = 0; $i < count($vehicle_list) ;$i++){
				if($vehicle_list[$i]['pk_vehicle_id']==$id)
					$vehicle_editor['vehicle'] = $vehicle_list[$i];
		} 
		$vehicle_editor['id'] = $id;
		if($this->form_validation->run()==FALSE) :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('edit/view_edit_nav', $data);
			$this->load->view('edit/view_edit_vehicle_add',$vehicle_editor);
			$this->load->view('footer', $data);
		
		else: 
			$success='TRUE';
			//Set inital message parameter
			$msg = '';

			
			if($this->input->post()){
				//Checks if publication is successfully submitted
				$post = $this->input->post();
			}
						
			if(empty($_POST['manufacturer']) && empty($_POST['model']) && empty($_POST['year'])&&
			empty($_POST['component_name']) && $_FILES['image']['error'][0] == UPLOAD_ERR_NO_FILE){
					//all field empty
					$msg = $_FILES['image']['error'][0];
					redirect("edit/vehicle_add_comp?success=EMPTY&id=$id&msg=$msg");
			}else{
				if(!empty($_POST['manufacturer']) || !empty($_POST['model']) || !empty($_POST['year'])){
					$vehicle_info = $this->model_edit->update_vehicle_info($post);
					$msg = $_POST['manufacturer'];
					if($vehicle_info){
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}		
				}
				if(isset($_FILES["image"])){
					if($_FILES["image"]["error"][0] != UPLOAD_ERR_NO_FILE){
						$img = $this->file_parser->images_parser();
						$image = $this->images->upload_edit_images($id, $img);
						if($image){
							$success = 'TRUE';
						}
						else {
							$success = 'FALSE';
						}
					}
				}
				if(isset($_POST['component_name']) && !empty($_POST['component_name'])){
					$cmp = $this->file_parser->components_parser();
					$components = $this->components->upload_edit_components($id, $cmp, $post['component_name']);

					if($components){
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}
				}
			}
			redirect("edit/vehicle_add_comp?success=$success&id=$id&msg=$msg");
		endif;
		
		
			
	}
	
	function vehicle_add_measure(){
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-vehicle',
			'title' => 'Your Vehicle',		//Title on tabs
			'editPane' => 'vehicle'			
		);
		
		$this->load->model('model_edit');
		$vehicle_list = $this->model_edit->search_vehicle_list($this->session->userdata('id'));
		//loads all vehicle data from this user

		$vehicle_editor['vehicle'] = NULL;
		$vehicle_editor['manufacturers'] = $this->manufacturers->return_manufacturers();
		$vehicle_editor['msg'] = '';
		
		if(isset($_GET['success'])&& isset($_GET['id'])){
			//if success is set then it means user has submitted some changes and are being rediredted 
			//Fail or success will print a different message on the same page
			$id = $_GET['id'];
			if($_GET['success'] == 'TRUE'){
				$vehicle_editor['msg'] = 'Update Successful. <br> Thank you for your submission, please allow 5 to 7 days for approval.';
			}
			else if($_GET['success'] == 'FALSE'){
				$vehicle_editor['msg'] = 'Update Failed';		
			}
			else if($_GET['success'] == 'EMPTY'){
				$vehicle_editor['msg'] = 'All fields are empty. No Update.';
			}
		}
		else if( isset($_GET['id'])){
			//If only the GET id is set then the user came from the list of publications
			$id = $_GET['id'];
		}
		else if( isset($_POST['id'])){
			//This is after user has submitted any changes
			$id = $_POST['id'];
		}
		
		for($i = 0; $i < count($vehicle_list) ;$i++){
				if($vehicle_list[$i]['pk_vehicle_id']==$id)
					$vehicle_editor['vehicle'] = $vehicle_list[$i];
		} 
		$vehicle_editor['id'] = $id;

		if(isset($_GET['id'])){
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('edit/view_edit_nav', $data);
			$this->load->view('edit/view_edit_vehicle_m',$vehicle_editor);
			$this->load->view('footer', $data);
		}
		else {
			$success='';
			//Set inital message parameter
	
			if($this->input->post()){
				//Checks if publication is successfully submitted
				$post = $this->input->post();
				
			}
			//$msg = count($_FILES["measurement"]["name"]);		
			//$msg = $_FILES["measurement"]["name"][0];
			//$msg = $_POST["compA"][3];
			if(isset($_FILES["measurement"])){
				$measure_arr = array();
				$check = 0;
				$counter = 0;
				foreach($_FILES["measurement"]["error"] as $error){
					if($error != UPLOAD_ERR_NO_FILE){
						$measure_arr['measurement']['name'][] =  $_FILES['measurement']['name'][$counter];
						$measure_arr['measurement']['type'][] =  $_FILES['measurement']['type'][$counter];
						$measure_arr['measurement']['size'][] =  $_FILES['measurement']['size'][$counter];
						$measure_arr['measurement']['tmp_name'][] =  $_FILES['measurement']['tmp_name'][$counter];
						$check = 1;
					}
					$counter++;
				}
				if($check == 1){
					$msr = $this->file_parser->measurements_edit_parser($measure_arr);
					$measurements = $this->measures->upload_edit_measurements($id, $msr, $_POST['compA'], $_POST['compB']);
					//$msg = $measurements;
					//$measurements = TRUE;
					if($measurements){
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}
				}
				else{
					redirect("edit/vehicle_add_measure?success=EMPTY&id=$id&msg=$msg");
				}
			} 
			
			redirect("edit/vehicle_add_measure?success=$success&id=$id&msg=$msg");	
		}
			
	}

	

	function edit_publications_list(){
		//grabs existing publications files and allow edits
		$this->load->model('model_edit');
		$data = array(							//variables passed into view
			'parent' => 'edit',
			'page' => 'edit-publications',
			'title' => 'Your Publications',		//Title on tabs
			'editPane' => 'publications',
			'publications' => $this->publications->return_publications(),
			'awaiting_approval_pub' => $this->publications->return_publications_awaiting_approval(),
			'rejected_pub' => $this->publications->return_publications_rejected(),
			'msg' =>''
		);
		
		//$publications_editor['publications'] =  $this->model_edit->search_publications($this->session->userdata('id'));
		if(isset($_POST['del_pub'])){
			if(!empty($_POST['del_pub'])){
				$del_pub = $this->model_edit->remove_publications($_POST);
				if($del_pub){
					$data['msg'] = 'Delete Successful.';
				}
				else{
					$data['msg'] = 'Delete Failed.';
				}
			}
		}
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav', $data);
		$this->load->view('edit/view_edit_publications_list',$data);
		$this->load->view('footer', $data);
	}
	
	function edit_publications(){
		//$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('affiliation', 'Publisher', 'min_length[2]|');
		$this->form_validation->set_rules('date', 'Publication year', 'min_length[4]|numeric');
		$this->form_validation->set_rules('url', 'URL', 'prep_url');
		if(isset($_POST['author_first'])){
			$this->form_validation->set_rules('author_first[]', 'First Initial', 'required');
			$this->form_validation->set_rules('author_last[]', 'Last Name', 'required|alpha_dash');
		}

		if(!empty($_POST['title']) || !empty($_POST['affiliation']) || !empty($_POST['date'])
		|| !empty($_POST['url'])|| !empty($_POST['author_first']) || !empty($_POST['author_last'])
		|| !empty($_POST['rm_authors'])){
			$this->form_validation->set_rules('agreement', 'Agreement', 'required');
		}
		
		$this->load->model('model_edit');
		
		$data = array(							//variables passed into view
			'parent' => 'edit',	
			'page' => 'edit-publications',
			'title' => 'Your Publications',		//Title on tabs
			'editPane' => 'publications',
			'msg' => ''
		);
		$id = NULL;
		$msg1 = '';
		$msg2 = '';
		//Checks which 'type' of page is loaded. 
		if(isset($_GET['success'])&& isset($_GET['id'])){
			//if success is set then it means user has submitted some changes and are being rediredted 
			//Fail or success will print a different message on the same page
			$id = $_GET['id'];
			if($_GET['success'] == 'TRUE'){
				$data['msg'] = 'Update Successful. <br> Thank you for your submission, please allow 5 to 7 days for approval.';
			}
			else if($_GET['success'] == 'FALSE'){
				$data['msg'] = 'Update Failed';		
			}
			else if($_GET['success'] == 'EMPTY'){
				$data['msg'] = 'All fields are empty. No Update.';
			}
			else if($_GET['success'] == 'AUTH'){
				$data['msg'] = 'Error: There must at least be 1 author for a publication';
			}
		}
		else if( isset($_GET['id'])){
			//If only the GET id is set then the user came from the list of publications
			$id = $_GET['id'];
		}
		else if( isset($_POST['id'])){
			//This is after user has submitted any changes
			$id = $_POST['id'];
		}
		
		$data['id'] = $id;
		$data['publication'] = $this->model_edit->search_publications($id);
		$data['awaiting_verification_authors'] = $this->publications->return_unverified_pub_authors($id);
		$data['rejected_authors'] = $this->publications->return_reject_pub_authors($id);
		if($this->form_validation->run()==FALSE) :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('edit/view_edit_nav', $data);
			$this->load->view('edit/view_edit_publications',$data);
			$this->load->view('footer', $data);
		else :
			$success='TRUE';
			//Set inital message parameter
			$count_authors = $this->model_edit->countAuthors($id);
			//initial author count from a publication
			
			if($this->input->post()){
				//Checks if publication is successfully submitted
				$post = $this->input->post();
			}
			
			if(empty($_POST['title']) && empty($_POST['affiliation']) && empty($_POST['date'])&&
				empty($_POST['url'])&& empty($_POST['author_first']) && empty($_POST['author_last'])
				&& empty($_POST['rm_authors'])){
					//all field empty
					redirect("edit/edit_publications?success=EMPTY&id=$id");
			}
			else{
				if(!empty($_POST['title']) || !empty($_POST['affiliation']) || !empty($_POST['date'])||
				!empty($_POST['url'])){
					//if the info fields are not empty, update them
					$publication = $this->model_edit->update_publication($post, $id);
					if($publication){
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}
				}
				if(!empty($_POST['author_first']) || !empty($_POST['author_last'])){
					//If authors are added, add them to database
					$count_authors = $count_authors + count($_POST['author_first']);
					//add count of existing records of author

					$authors = $this->publications->create_edit_authors($post, $id);
					if($authors){
						$success = 'TRUE';
					}
					else {
						$success = 'FALSE';
					}

				}
				if( isset($_POST['rm_authors'])){
					//If authors are deleted, delete them
					//However we want to keep at least 1 author per publication
					//Therefore we count existing records and add to them if authors are being added
					//Then if the number is equal to the number of authors to be deleted,
					//Nothing is deleted and error message is passed.
						$msg1 = $count_authors;
						$msg2 = count($_POST['rm_authors']);
					if(count($_POST['rm_authors']) < $count_authors){
						
						$authors_delete = $this->model_edit->remove_author($post);
	
						if($authors_delete){
							$success = 'TRUE';
						}
						else {
							$success = 'FALSE';
						}
					}
					else {
						$success = 'AUTH';
					}

				}
			}
			
					//Setting Get for right message
			redirect("edit/edit_publications?success=$success&id=$id&msg1=$msg1&msg2=$msg2");



				
			
		endif;
		
		
	}
}