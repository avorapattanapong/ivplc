<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct(){
	    parent::__construct();
		//$this->group->authenticated();
	}
	
	public function index(){
		//first page, list of available options
		$data = array(
			'parent' => 'admin',
			'page' => 'admin',
			'title' => 'Admin',
			'admin' => true,
			'publications_count' => $this->publications->awaiting_approval(),
			'vehicles_count' => $this->vehicles->awaiting_approval()
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('footer', $data);
	}

	public function admin_measurements($pk_vehicle_id = null){
		$this->load->model('model_readme');
		$data = array(
			'parent' => 'admin',
			'page' => 'Measurements admin',
			'title' => 'Measurements Admin',
			'admin' => true,
			'ApproveVehicles' => $this->vehicles->return_unverified_vehicles(),
			'ApproveComponents' => $this->components->return_unverified_vehicle_components_indexed(),
			'ApproveMeasurements' => $this->measures->return_unverified_vehicle_measurements(),
			'ApproveReadme' => $this->model_readme->return_unverified_readme(),
			'ApproveImages' => $this->images->return_unverified_images(),
			'vehicles' => $this->vehicles->return_vehicles()			
		);
		
		if($this->input->post()) :
			$submit = $this->input->post('submit');
			$chk = $this->input->post('vehicle');
			//a variable that will not be empty if vehicle submit form is submitted
			$pk_vehicle_id = $this->input->post('pk_vehicle_id');
			$fk_contributor_id = $this->input->post('fk_contributor_id');
			$pk_component_id = $this->input->post('pk_component_id');
			$pk_measurement_id = $this->input->post('pk_measurement_id');
			$pk_desc_id = $this->input->post('pk_desc_id');
			$pk_image_id = $this->input->post('pk_image_id');
			$manu_name = $this->input->post('manu');
			$msg = 'none';
			switch($submit){
				case 'Approve':
					//Approving one of the following
					//Runs function to change verified field
					//Then go back to same page
					if(!empty($chk)){
						$this->vehicles->approve($pk_vehicle_id, $fk_contributor_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_vehicle_id, 'vehicles');
						
					}
					else if(!empty($pk_component_id)){
						$this->components->approve_component($pk_component_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_component_id, 'components');
					}
					else if(!empty($pk_measurement_id)){
						$this->measures->approve_measurement($pk_measurement_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_measurement_id, 'measurements');
					}
					else if(!empty($pk_desc_id)){
						$this->model_readme->approve_readme($pk_desc_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_desc_id, 'vehicle_description');
					}
					else if(!empty($pk_image_id)){
						$this->images->approve_image($pk_image_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_image_id, 'images');
					}
					redirect("admin/measurements");
					break;
				case 'Reject' :
				
					if(!empty($chk)){
						$this->vehicles->reject_vehicle($pk_vehicle_id, $fk_contributor_id);
						$msg='vehicle';
						$this->model_logtable->add_log(12,$this->session->userdata('id'),$pk_vehicle_id, 'vehicles');
					}
					else if(!empty($pk_component_id)){
						$this->components->reject_component($pk_component_id);
						$this->model_logtable->add_log(12,$this->session->userdata('id'),$pk_component_id, 'components');
					}
					else if(!empty($pk_measurement_id)){
						$this->measures->reject_measurement($pk_measurement_id);
						$this->model_logtable->add_log(12,$this->session->userdata('id'),$pk_measurement_id, 'measurements');
					}
					else if(!empty($pk_desc_id)){
						$this->model_readme->reject_readme($pk_desc_id);
						$this->model_logtable->add_log(12,$this->session->userdata('id'),$pk_desc_id, 'vehicle_description');
					}
					else if(!empty($pk_image_id)){
						$this->images->reject_image($pk_image_id);
						$this->model_logtable->add_log(12,$this->session->userdata('id'),$pk_image_id, 'images');
					}
					redirect("admin/measurements?msg=$msg");
					break;
				case 'Delete' :
					$this->vehicles->delete($pk_vehicle_id);
					$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_vehicle_id, 'vehicles');
					redirect('admin/measurements');
					break;
				case 'Add' :
					$this->manufacturers->admin_create_manu($manu_name);
					redirect('admin/measurements');
					break;
				default :
					return false;
			}

		else :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			
			if($pk_vehicle_id != null) :
				//When users click on the vehicle picture links, redirected here
				$vehicle = $this->vehicles->return_vehicles($pk_vehicle_id,'0');
				$data['vehicle'] = $vehicle[0];
				$this->load->view('admin/vehicle', $data);
			else :
				//On first load from admin list of available options
				$data['vehicles'] = $this->vehicles->return_vehicles();
				$this->load->view('admin/measurements', $data);
			endif;
			
			$this->load->view('footer', $data);
		endif;
	}
	
	public function admin_rejects($pk_vehicle_id = null){
		//list rejected components
		$this->load->model('model_readme');
		$this->load->model('model_edit');
		$data = array(
			'parent' => 'admin',
			'page' => 'Measurements admin',
			'title' => 'Measurements Admin',
			'admin' => true,
			'RejectVehicles' => $this->vehicles->return_rejected_vehicles(),
			'RejectComponents' => $this->components->return_rejected_vehicle_components_indexed(),
			'RejectMeasurements' => $this->measures->return_rejected_vehicle_measurements(),
			'RejectReadme' => $this->model_readme->return_rejected_readme(),
			'RejectImages' => $this->images->return_rejected_images(),
			'RejectPublications' => $this->publications->return_publications_rejected(),
			'RejectAuthors' => $this->publications->return_rejected_authors()
			//'vehicles' => $this->vehicles->return_vehicles()			
		);
		
		if($this->input->post()) :
			$submit = $this->input->post('submit');
			$chk_pub = $this->input->post('pubs');
			$chk_v = $this->input->post('vehicle');
			//a variable that will not be empty if vehicle submit form is submitted
			$pk_vehicle_id = $this->input->post('pk_vehicle_id');
			$fk_contributor_id = $this->input->post('fk_contributor_id');
			$pk_component_id = $this->input->post('pk_component_id');
			$pk_measurement_id = $this->input->post('pk_measurement_id');
			$pk_desc_id = $this->input->post('pk_desc_id');
			$pk_image_id = $this->input->post('pk_image_id');
			$pk_pub_id = $this->input->post('pk_pub_id');
			$pk_author_id = $this->input->post('pk_author_id');

			
			switch($submit){
				case 'Approve':
					//Approving one of the following
					//Runs function to change verified field
					//Then go back to same page
					if(!empty($chk_v)){
						$this->vehicles->approve($pk_vehicle_id, $fk_contributor_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_vehicle_id, 'vehicles');
					}
					else if(!empty($pk_component_id)){
						$this->components->approve_component($pk_component_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_component_id, 'components');
					}
					else if(!empty($pk_measurement_id)){
						$this->measures->approve_measurement($pk_measurement_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_measurement_id, 'measurements');
					}
					else if(!empty($pk_desc_id)){
						$this->model_readme->approve_readme($pk_desc_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_desc_id, 'vehicle_description');
					}
					else if(!empty($pk_image_id)){
						$this->images->approve_image($pk_image_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_image_id, 'images');
					}
					else if(!empty($chk_pub)){
						$this->publications->approve($pk_pub_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_pub_id, 'publications');
					}
					else if(!empty($pk_author_id)){
						$this->publications->approve_author($pk_author_id);
						$this->model_logtable->add_log(11,$this->session->userdata('id'),$pk_author_id, 'authors');
					}
					redirect("admin/measurements");
					break;
				case 'Delete' :
				
					if(!empty($chk_v)){
						$this->vehicles->delete($pk_vehicle_id);
						$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_vehicle_id, 'vehicles');
					}
					else if(!empty($pk_component_id)){
						$this->model_edit->remove_noise($pk_vehicle_id,$pk_component_id);		
						$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_component_id, 'components');				
						//$this->components->delete_component($pk_component_id);
					}
					else if(!empty($pk_measurement_id)){
						if( $this->model_edit->remove_measurement($pk_vehicle_id,$pk_measurement_id) ){
							$this->measures->delete_measurement($pk_measurement_id);
							$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_measurement_id, 'measurements');
						}
					}
					else if(!empty($pk_image_id)){
						if($this->model_edit->remove_image($pk_vehicle_id, $pk_image_id) ){
							$this->images->delete_image($pk_image_id);
							$this->model_logtable->add_log(1,$this->session->userdata('id'),$pk_image_id, 'images');
						}
					}
					else if(!empty($chk_pub)){
						$this->publications->delete($pk_pub_id);
							$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_pub_id, 'publications');
					}
					else if(!empty($pk_author_id)){
						$this->publications->delete_author($pk_author_id);
						$this->model_logtable->add_log(13,$this->session->userdata('id'),$pk_author_id, 'authors');
					}
					redirect("admin/measurements");
					break;
				default :
					return false;
			}

		else :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			
			if($pk_vehicle_id != null) :
				//When users click on the vehicle picture links, redirected here
				$vehicle = $this->vehicles->return_vehicles($pk_vehicle_id,'0');
				$data['vehicle'] = $vehicle[0];
				$this->load->view('admin/vehicle', $data);
			else :
				//On first load from admin list of available options
				$data['vehicles'] = $this->vehicles->return_vehicles();
				$this->load->view('admin/view_admin_reject', $data);
			endif;
			
			$this->load->view('footer', $data);
		endif;
	}
	

	public function admin_publications(){		
		$data = array(
			'parent' => 'admin',
			'page' => 'publications admin',
			'title' => 'Publications Admin',
			'admin' => true,
			'ApprovePublications' => $this->publications->return_publications_awaiting_approval(),
			'ApproveAuthors' => $this->publications->return_unverified_authors(),
			'publications' => $this->publications->return_publications()
		);
		
		if($this->input->post()) :
			$submit = $this->input->post('submit');
			$pk_pub_id = $this->input->post('pk_pub_id');
			$pk_author_id = $this->input->post('pk_author_id');
			$chk = $this->input->post('pubs');
			
			if($submit == 'Approve') :
				if(!empty($chk)){
					$this->publications->approve($pk_pub_id);
				}
				else if(!empty($pk_author_id)){
					$this->publications->approve_author($pk_author_id);
				}
				redirect("admin/publications");
			elseif($submit == 'Reject') :
				if(!empty($chk)){
					$this->publications->reject_pub($pk_pub_id);
				}
				else if(!empty($pk_author_id)){
					$this->publications->reject_author($pk_author_id);
				}
				redirect("admin/publications");
			elseif($submit == 'Delete') :
				$this->publications->delete($pk_pub_id);
				redirect('admin/publications');
			else :
				return false;
			endif;
		else :		
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('admin/publications', $data);
			$this->load->view('footer', $data);
		endif;
	}
	
	public function admin_group(){		
		$data = array(
			'parent' => 'admin',
			'page' => 'group admin',
			'title' => 'Group Admin',
			'admin' => true,
			'supervisors' => $this->group->return_supervisors(),
			'assistants' => $this->group->return_assistants()
		);
		
		if($this->input->post()) :
			$submit = $this->input->post('submit');
			$pk_group_id = $this->input->post('pk_group_id');
			
			if($submit == 'Delete') :
				$this->group->delete_member($pk_group_id);
				redirect('admin/group');
			elseif($submit =='Add') :
				$member = $this->group->add_member($this->input->post());
				redirect('admin/group');
			else :
				return false;		
			endif;
		else :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('admin/group', $data);
			$this->load->view('footer', $data);
		endif;
	}
	
	public function admin_users(){		
		$data = array(
			'parent' => 'admin',
			'page' => 'group admin',
			'title' => 'Group Admin',
			'admin' => true,
			'users' => $this->contributors->return_all_contributor()
		);
		

		if($this->input->post()) :
			$submit = $this->input->post('submit');
			$pk_contributor_id = $this->input->post('pk_contributor_id');
			
			if($submit == 'Approve') :
				$this->contributors->approve_contributor($pk_contributor_id);
				redirect("admin/users");
			elseif($submit == 'Reject') :
				$this->contributors->delete_contributor($pk_contributor_id);
				redirect("admin/users");
			elseif($submit == 'Delete') :
				$this->contributors->delete_contributor($pk_contributor_id);
				redirect('admin/users');
			else :
				return false;
			endif;
		else :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('admin/view_users', $data);
			$this->load->view('footer', $data);
		endif;
	}
	
	public function admin_log_table(){
		//shows log table page
		$this->load->model('model_logtable');
		$this->load->helper('url');
		$data = array(
			'parent' => 'admin',
			'page' => 'group admin',
			'title' => 'Group Admin',
			'admin' => true
		);
		//initial values for search controls
		$asc = 'asc';
		$sort_list =  'date';
		$limit_entries = 2;
		$offset_entries = 0;
		
		//setting up offset for paggination
		$total_segments = $this->uri->total_segments();
		$last_segment = $this->uri->segment(3);
		if(is_numeric($last_segment)){
			$offset_entries = $last_segment;

		}
		$msg = 'none';

		if( isset($_GET['sort_method']) && !empty( $_GET['sort_method'])){
			//sort method get
			if($_GET['sort_method'] == 'asc'){
				$asc = 'asc';
			}
			else if($_GET['sort_method'] == 'dsc'){
				$asc = 'dsc';
			}
		}
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$offset_entries =$_GET['per_page'];
		}
		
		if(isset($_GET['lim']) && !empty($_GET['lim'])){
			$limit_entries = $_GET['lim'];
		}
		
		if(isset($_GET['sort_list']) && !empty($_GET['sort_list'])){
			$sort_list = $_GET['sort_list'];
		}
		
		

		if($sort_list == 'date'){
				$data['log_table_default'] = $this->model_logtable->return_log($asc, $limit_entries, $offset_entries);
		}
		else if($sort_list == 'users'){
				$data['log_table_default'] = $this->model_logtable->return_log_user($asc, $limit_entries, $offset_entries);
		}
		else if($sort_list == 'actions'){
				$data['log_table_default'] = $this->model_logtable->return_log_type($asc, $limit_entries, $offset_entries);
		}

		
		if(isset($_POST['submit']) && !empty($_POST['submit'])) :
			$submit = $this->input->post('submit');
			$pk_log_id = $this->input->post('pk_log_id');
			$lim = $_POST['num_results'];
			$sort = $_POST['sort_list'];
			$direction = $_POST['sort_method'];
			
			
			if($submit == 'delete') :
				$this->model_logtable->delete_log($pk_log_id);
				if(is_numeric($last_segment)){
					redirect("administrator/admin_log_table/$last_segment'");
				}
				else{
					redirect("administrator/admin_log_table");
				}
			elseif($submit =='Apply Filter') :
				redirect("administrator/admin_log_table?lim=$lim&sort_list=$sort&sort_method=$direction");
			elseif($submit == 'Delete All Entries'):
				$this->model_logtable->delete_all_logs();
				if(is_numeric($last_segment)){
					redirect("administrator/admin_log_table/$last_segment'");
				}
				else{
					redirect("administrator/admin_log_table");
				}
				
			else :
				return false;		
			endif;
		else :
			$this->load->library('pagination');
			
			$config['page_query_string'] = TRUE;
			$config['base_url'] = site_url().'administrator/admin_log_table?lim='.$limit_entries.'&sort_list='.$sort_list.'&sort_method='.$asc;
			$config['total_rows'] = $this->model_logtable->total_rows();
			$config['per_page'] = $limit_entries;
			$config['uri_segment'] = $total_segments;
			$config['num_links'] = 20;
			
			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul><!--pagination-->';
			$config['first_link'] = '&laquo; First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = 'Last &raquo;';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			$config['next_link'] = 'Next &rarr;';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			$config['prev_link'] = '&larr; Previous';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
			
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('admin/view_log_table', $data);
			$this->load->view('footer', $data);
		endif;
		
		
	}

	function admin_contributor_settings(){
		$user_type = 'Error';
		
		if(isset($_POST['user_type']) && !empty($_POST['user_type'])){
			$user_type = $_POST['user_type'];
		}
		if(isset($_GET['user_type'])){
			$user_type = $_GET['user_type'];
		}
		if($user_type == 'contributor'){
			$this->form_validation->set_rules('fname', 'First Name', 'alpha|trim');
			$this->form_validation->set_rules('lname', 'Last Name', 'alpha|trim');
			$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|xss_clean');
			$this->form_validation->set_rules('affiliation', 'Affiliation', 'alpha|trim');
			$this->form_validation->set_rules('city', 'City', 'alpha|trim');
			$this->form_validation->set_rules('country', 'Country', 'alpha|trim');
			$this->form_validation->set_rules('password', 'Password', 'trim');
			if(isset($_POST['password']) && !empty($_POST['password'])){
				$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
			}
		}
		else{
			$this->form_validation->set_rules('name', 'Name', 'trim');
			$this->form_validation->set_rules('email', 'Email', 'valid_email|trim|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim');
			if(isset($_POST['password']) && !empty($_POST['password'])){
				$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
			}
		}
		
		$data = array(							//variables passed into view
			'parent' => 'admin',
			'page' => 'edit-index',
			'title' => 'Edit Your Profile',		//Title on tabs
			'editPane' => 'userinfo'
		);
		$this->load->model('model_edit');
		$this->load->helper('security');
		
		if(isset($_POST['submit']) && $_POST['submit'] == 'Edit User'){
			if(isset($_POST['pk_contributor_id']) && !empty($_POST['pk_contributor_id'])){
				$userData = $this->model_edit->get_all_info($_POST['pk_contributor_id']);
			}
			else if(isset($_POST['pk_group_id']) && !empty($_POST['pk_group_id'])){
				$userData = $this->model_edit->get_all_infoAdmin($_POST['pk_group_id']);
			}
		}
		
		$userData['msg'] = '';
		
		if(isset($_GET['success'])){
			if($user_type == 'contributor'){
				$userData = $this->model_edit->get_all_info($_GET['id']);
			}
			else if($user_type == 'group'){
				$userData = $this->model_edit->get_all_infoAdmin($_GET['id']);
			}
			
			if($_GET['success'] == 'TRUE'){
				$userData['msg'] = 'Update Successful.';
			}
			else if($_GET['success'] == 'FALSE'){
				$userData['msg'] = 'Update Failed.';
			}
			else if($_GET['success'] == 'EMPTY'){
				$userData['msg'] = 'All fields are empty. No Update.';
			}
			else if($_GET['success'] == 'FALSE_ID'){
				$userData['msg'] = 'Wrong ID';
			}
			

			
		}

		$userData['user_type'] = $user_type;


		if(isset($_POST['submit']) && $_POST['submit'] == 'Submit') {
			$id = 0;
			$success = 'EMPTY';
			if( !empty($_POST['email']) || !empty($_POST['name']) || !empty($_POST['password']) || !empty($_POST['fname']) 
			|| !empty($_POST['lname']) || !empty($_POST['affiliation']) || !empty($_POST['city']) 
			|| !empty($_POST['country'])){
				if($user_type != 'Error'){
					if($user_type == 'contributor'){
						$id = $_POST['pk_contributor_id'];
						//member update user info
						$form_userinfo_data = array (
							//'pk_contributor_id'			=> NULL,
							'email'						=> $this->input->post('email'),
							'first_name'				=> $this->input->post('fname'),
							'last_name'					=> $this->input->post('lname'),
							'affiliation'				=> $this->input->post('affiliation'),
							'city'						=> $this->input->post('city'),
							'country'					=> $this->input->post('country')
							//'verified'					=> NULL
						);
						if(!empty($_POST['password'])){
							$form_userinfo_data['password']	= do_hash($this->input->post('password'), 'md5');
						}
						
						if($this->model_edit->update_userinfo($form_userinfo_data, $id)!= -1){
							$success = 'TRUE';
						}
						else{
							$success = 'FALSE';
						}
					}
					else if($user_type == 'group')
					{
						$id = $_POST['pk_group_id'];
						//admin update userinfo
						$form_userinfo_data = array (
							//'pk_contributor_id'			=> NULL,
							'email'						=> $this->input->post('email'),
							'name'						=> $this->input->post('name')
							//'verified'					=> NULL
						);
						if(!empty($_POST['password'])){
							$form_userinfo_data['password']	= do_hash($this->input->post('password'), 'md5');
						}
						
						if($this->model_edit->update_userinfo_admin($form_userinfo_data, $id)!= -1){
							$success = 'TRUE';
						}
						else{
							$success = 'FALSE';
						}
					}
				}
				else{
					$success = 'FALSE_ID';
				}
			}
			redirect("admin/set_users?success=$success&user_type=$user_type&id=$id");
		}
		else{
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('admin/view_users_settings',$userData);
			$this->load->view('footer', $data);
		}	
	}
	

}