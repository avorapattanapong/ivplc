<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctrl_Members extends CI_Controller {

	function __construct(){
	    parent::__construct();
		session_start();

	}
	
	public function index(){
		$this->load->model('model_login_authenticate');
		$this->model_login_authenticate->authenticated();
		$data = array(
			'parent' => 'members home',
			'page' => 'members-index',
			'title' => 'Members Home'
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('members/index', $data);
		$this->load->view('footer', $data);
		
		
	}
	public function registration(){
		//needs to be migrated to registration page
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[contributors.email]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');
		$this->form_validation->set_rules('first', 'First Name', 'required|min_length[2]');
		$this->form_validation->set_rules('last', 'Last Name', 'required');
		$this->form_validation->set_rules('affiliation', 'Affiliation', 'required');
		$this->form_validation->set_rules('city', 'City', 'min_length[2]|alpha_dash');
		$this->form_validation->set_rules('country', 'Country', 'min_length[2]|alpha_dash');
		
		$this->form_validation->set_message('is_unique', "This email already exists.");
			
		$data = array(
			'parent' => 'login',
			'page' => 'registration',
			'title' => 'Registration'
		);
		
		if($this->form_validation->run() == FALSE) :
			$this->load->view('header', $data);
			$this->load->view('nav', $data);
			$this->load->view('members/registration', $data);
			$this->load->view('footer', $data);
		else :
			if($this->input->post()) :
				$contributor = $this->contributors->create_contributor($this->input->post());
				
				if($contributor) :
					redirect('external/login?success=TRUE');
				else :
					redirect('external/login?success=FALSE');

				endif;
			endif;
		endif;
	}
	/*
	public function group(){
		$this->load->model('model_login_authenticate');
		$this->model_login_authenticate->authenticated();		
		$data = array(
			'parent' => 'members',
			'page' => 'members-group',
			'title' => 'Members Group',
			'supervisors' => $this->group->return_supervisors(),
			'assistants' => $this->group->return_assistants()
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('members/group', $data);
		$this->load->view('footer', $data);
	}
	
	public function publications(){
		$data = array(
			'parent' => 'members',
			'page' => 'members-publications',
			'title' => 'Members Publications',
			'publications' => $this->publications->return_publications()
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('members/publications', $data);
		$this->load->view('footer', $data);
	}
	
	function member_profile(){
		//opens profile edit page: allows userinfo and submission edition
		$data = array(
			'parent' => 'profile',
			'page' => 'members-profile',
			'title' => 'Members Profile',
			'editPane' => 'edit'
		);
		
		$this->load->view('header', $data);
		$this->load->view('nav', $data);
		$this->load->view('edit/view_edit_nav',$data);
		$this->load->view('edit/view_edit_chooser', $data);
		$this->load->view('footer', $data);
		
	}
	*/
	function logout(){
		//logout of the system
		$this->session->sess_destroy();
		redirect('index');
	}
	
	function restricted(){
		$this->load->view('members/restricted');
		
	}
}