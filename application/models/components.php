<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Components extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    /* CREATE COMPONENTS*/
    function upload_components($fk_vehicle_id, $components, $post){    
		$config = array(
			'upload_path' => './uploads/noise_files/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $components;
	    
	    for($i = 0; $i < count($components); $i++) :
			$config['file_name'] = $fk_vehicle_id . '_noise_'. $i;
			$this->upload->initialize($config); 
			
			if($_FILES[$i]['error'] != 4) :
				$this->upload->do_upload($i);
			
				$upload_data = $this->upload->data();
				$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			else :
				$relative_url = null;
			endif;
			
			$data = array(
				'fk_vehicle_id' => $fk_vehicle_id,
				'name' => $post[$i],
				'url' => str_replace('ivplc', '', $relative_url)
			);
			
			$query = $this->db->insert('components', $data);
		endfor;
		
		return $this->return_vehicle_components($fk_vehicle_id);
    }
    
	function upload_edit_components($fk_vehicle_id, $components, $post){
		//Upload components for "edits" section where you add new components    
		$config = array(
			'upload_path' => './uploads/noise_files/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $components;
	    $comp = $this->components->return_vehicle_components($fk_vehicle_id);
		$count = count($comp);

	    for($i = 0; $i < count($components); $i++) :
			$config['file_name'] = $fk_vehicle_id . '_noise_'. ($i+$count);
			//name the files with comp number
			$this->upload->initialize($config); 
			
			if($_FILES[$i]['error'] != 4) :
				$this->upload->do_upload($i);
			
				$upload_data = $this->upload->data();
				$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			else :
				$relative_url = null;
			endif;
			
			$data = array(
				'fk_vehicle_id' => $fk_vehicle_id,
				'name' => $post[$i],
				'url' => str_replace('ivplc', '', $relative_url),
				'verified' => 0,
				'reject' => 0
			);
			
			$query = $this->db->insert('components', $data);
			$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		endfor;
		
		return TRUE;
    }
    
	function upload_replace_components($fk_vehicle_id, $components, $pk_component_id){
		//Upload components for "Replace" section where you replace an existing noise files or add files
		$config = array(
			'upload_path' => './uploads/noise_files/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $components;
	    $comp = $this->components->return_vehicle_components($fk_vehicle_id);
		$name = 0;
		foreach($comp as $component){
			if($component['pk_component_id'] == $pk_component_id){
				break;
			} 
			$name++;
		}

	    for($i = 0; $i < count($components); $i++) :
			$config['file_name'] = $fk_vehicle_id . '_noise_'. ($i+$name);
			//name the files with comp number
			$this->upload->initialize($config); 
			
			if($_FILES[$i]['error'] != 4) :
				$this->upload->do_upload($i);
			
				$upload_data = $this->upload->data();
				$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			else :
				$relative_url = null;
			endif;
			
			$data = array(
				'url' => str_replace('ivplc', '', $relative_url),
				'verified' => 0,
				'reject' => 0
			);
			
			$this->db->where('pk_component_id', $pk_component_id);
			$query = $this->db->update('components', $data);
			$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		endfor;
		
		return TRUE;
    }

	function change_comp_name($pk_component_id, $name){
	// 	
		$data = array(
			'name' => $name,
			'verified' => 0,
			'reject' => 0
		);
		$this->db->where('pk_component_id', $pk_component_id);
		$this->db->update('components', $data);
		
		
					$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		
		return TRUE;
	}
	
    /* RETURN WITH VEHICLE */
    function return_vehicle_components($fk_vehicle_id){
		$query = $this->db->get_where('components', array('fk_vehicle_id'=>$fk_vehicle_id));
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components[] = $component;
			endforeach;
			
			return $components;
		endif;
    }
    
    function return_vehicle_components_indexed($fk_vehicle_id){
		$query = $this->db->get_where('components', array('fk_vehicle_id'=>$fk_vehicle_id));
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components[$component['pk_component_id']] = $component;
			endforeach;
			
			return $components;
		endif;
    }
	
	function return_unverified_vehicle_components_indexed(){
		$query = $this->db->get_where('components', array('verified' => 0, 'reject' => 0));
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components[$component['pk_component_id']] = $component;
			endforeach;
			
			return $components;
		endif;
    }
	
	function return_component($pk_component_id){
		$query = $this->db->get_where('components', array('pk_component_id' => $pk_component_id));
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components = $component;
			endforeach;
			
			return $components;
		endif;
    }

	function return_rejected_vehicle_components_indexed(){
		$query = $this->db->get_where('components', array('verified' => 0, 'reject' => 1));
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components[$component['pk_component_id']] = $component;
			endforeach;
			
			return $components;
		endif;
    }
	
	function return_components_indexed(){
		$query = $this->db->get_where('components');
		$components = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $component ) :
				$components[$component['pk_component_id']] = $component;
			endforeach;
			
			return $components;
		endif;
    }
	
	function approve_component($pk_component_id){		
		$query = $this->db->where('pk_component_id' , $pk_component_id);
    	$query = $this->db->update('components', array('verified'=>'1'));
	}
	
	function reject_component($pk_component_id){		
		$query = $this->db->where('pk_component_id' , $pk_component_id);
    	$query = $this->db->update('components', array('reject'=>1));
	}
	
	function delete_component($pk_component_id, $fk_vehicle_id){
		$this->load->model('model_edit');
		
		$measurements = $this->measures->return_vehicle_measurements($fk_vehicle_id);
		
		foreach($measurements as $measure){
			//search all measurements that has associating component id in both A and B and delete them
			if($measure['fk_componentA_id'] == $pk_component_id || $measure['fk_componentB_id'] == $pk_component_id){
				$dm = $this->model_edit->remove_measurement($fk_vehicle_id,$measure['pk_measurement_id']);
				if( $dm == FALSE ){
					return FALSE;
				}
				$this->measures->delete_measurement($measurement['pk_measurement_id']);
			}
		}
		
		$components = $this->components->return_vehicle_components_indexed($fk_vehicle_id);
		if( $components[$pk_component_id]['url'] != "" || $components[$pk_component_id]['url'] != NULL){
			//deleting any noise file if any exist before deleting the component from the database
			$dc = $this->model_edit->remove_noise_no_db($fk_vehicle_id, $pk_component_id);
		}
    	$query = $this->db->delete('components', array('pk_component_id'=>$pk_component_id));
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		
		return TRUE;
    }

}