<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    function upload_images($fk_vehicle_id, $images){    
		$config = array(
			'upload_path' => './uploads/car_images/',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $images;
		$error = '';
		
	    for($i = 0; $i < count($images); $i++) :
			$config['file_name'] = $fk_vehicle_id . '_image_'.$i;
			$this->upload->initialize($config); 
			
			$this->upload->do_upload($i);

		
			$upload_data = $this->upload->data();
			$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			
			$resize = array(
				'image_library' => 'gd2',
				'source_image' => str_replace('ivplc', '', $relative_url),
				'maintain_ratio' => TRUE,
				'width' => '920'
			);
			
			$this->image_lib->initialize($resize);
			$this->image_lib->resize();
			
			$data = array(
				'fk_vehicle_id' => $fk_vehicle_id,
				'url' => str_replace('ivplc', '', $relative_url)
			);
			
			$query = $this->db->insert('images', $data);
		endfor;
		
		if($error == ''){
			return TRUE;
		}
		else{
			return $error;
		}
    } 
	
	function upload_edit_images($fk_vehicle_id, $images){    
		$config = array(
			'upload_path' => './uploads/car_images/',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $images;

	    $img = $this->images->return_vehicle_images($fk_vehicle_id);
		$temp_url = 'asdf';
		foreach($img as $image){
			//getting the last element of images url
			$temp_url = $image['url']; 
		}
		$start = strpos($temp_url,'image_') + 6;
		$end = strpos($temp_url,'.');
		$length = $end - $start;
		$name = intval(substr($temp_url,$start,$length));
		
	    for($i = 0; $i < count($images); $i++) :
			$config['file_name'] = $fk_vehicle_id."_image_".($i+$name+1);
			$this->upload->initialize($config); 
			$this->upload->do_upload($i);
		
			$upload_data = $this->upload->data();
			$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			
			$resize = array(
				'image_library' => 'gd2',
				'source_image' => str_replace('ivplc', '', $relative_url),
				'maintain_ratio' => TRUE,
				'width' => '920'
			);
			
			$this->image_lib->initialize($resize);
			$this->image_lib->resize();
			
			$data = array(
				'fk_vehicle_id' => $fk_vehicle_id,
				'url' => str_replace('ivplc', '', $relative_url),
				'verified' => 0,
				'reject' => 0
			);
			
			$query = $this->db->insert('images', $data);
			$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		endfor;
 
		return TRUE;
    } 
	
	function replace_image($fk_vehicle_id, $pk_image_id,$images){
		$config = array(
			'upload_path' => './uploads/car_images/',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'max_size' => '2048',
			'overwrite' => true
		);
		
	    $_FILES = $images;
	    $tempImages = $this->images->return_vehicle_images($fk_vehicle_id);
		$image_file = '';
		foreach( $tempImages as $image){
			if($image['pk_image_id'] == $pk_image_id){
				$image_file = $image['url'];
			}
		}
		$start = strpos($image_file,'image_') + 6;
		$end = strpos($image_file,'.');
		$length = $end - $start;
		$name = intval(substr($image_file,$start,$length));

	    for($i = 0; $i < count($images); $i++) :
			$config['file_name'] =  $fk_vehicle_id."_image_".$name;
			$this->upload->initialize($config); 
			$this->upload->do_upload($i);
		
			$upload_data = $this->upload->data();
			$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			
			$resize = array(
				'image_library' => 'gd2',
				'source_image' => str_replace('ivplc', '', $relative_url),
				'maintain_ratio' => TRUE,
				'width' => '920'
			);
			
			$this->image_lib->initialize($resize);
			$this->image_lib->resize();
			
			$data = array(
				'url' => str_replace('ivplc', '', $relative_url),
				'verified' => 0,
				'reject' => 0
			);
			$this->db->where('pk_image_id', $pk_image_id);
			$query = $this->db->update('images', $data);
			$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		endfor;

		return TRUE;
	} 

	function return_vehicle_images($fk_vehicle_id){
		$query = $this->db->get_where('images', array('fk_vehicle_id' => $fk_vehicle_id));
		$images = array();

		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $image ) :
				$images[] = $image;
			endforeach;

			return $images;
		endif;
    }
	
	function return_image($pk_image_id){
		$query = $this->db->get_where('images', array('pk_image_id' => $pk_image));
		$images = array();

		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $image ) :
				$images = $image;
			endforeach;

			return $images;
		endif;
		
	}
	
	function return_unverified_images(){
		$query = $this->db->get_where('images', array('verified' => 0, 'reject' => 0 ));
		$images = array();

		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $image ) :
				$images[] = $image;
			endforeach;

			return $images;
		endif;
    }
	
	function return_rejected_images(){
		$query = $this->db->get_where('images', array('verified' => 0, 'reject' => 1 ));
		$images = array();

		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $image ) :
				$images[] = $image;
			endforeach;

			return $images;
		endif;
    }
	
	function approve_image($pk_image_id){		
		$query = $this->db->where('pk_image_id' , $pk_image_id);
    	$query = $this->db->update('images', array('verified'=>'1'));
	}
	
	function reject_image($pk_image_id){		
		$query = $this->db->where('pk_image_id' , $pk_image_id);
    	$query = $this->db->update('images', array('reject'=>1));
	}
	
	function delete_image($pk_image_id){
    	$query = $this->db->delete('images', array('pk_image_id'=>$pk_image_id));
    }

}