<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contributors extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    
    /* RETURN FUNCTIONS */    
    function return_contributor($pk_contributor_id){
    	$query = $this->db->get_where('contributors', array('pk_contributor_id' => $pk_contributor_id));
		$contributors = array();	
		$result = $query->result_array();
		if( $query->num_rows() > 0 ){			   
			foreach($result as $contributor){
				$contributors[] = $contributor;
			}
		}
		return $contributors;
    }
	
	function return_all_contributor(){
    	$query = $this->db->query("SELECT *
    							   FROM   contributors");
		$result = $query->result_array();		
		$contributors = array();	
		if( $query->num_rows() > 0 ){			   
			foreach($result as $contributor){
				$contributors[] = $contributor;
			}
		}
		return $contributors;
    }

	function check_email($email){
		$query = $this->db->get_where('contributors', array('email' => $email));
		return ($query->num_rows() == 1 ) ? $query->row_array() : false;
    }
    
	function check_verified($email){
		$query = $this->db->get_where('contributors', array('email' => $email, 'verified' => '1'));
		return ($query->num_rows() == 1 ) ? $query->row_array() : false;
    }
    
    function return_name($email){
    	$query = $this->db->get_where('contributors', array('email' => $email));
		
		if($query->num_rows() == 1) :
			$contributor = $query->row_array();
			return $contributor['first_name'];
		else :
			return false;
		endif;
    }
    
    
    /* ADD FUNCTIONS */
    function create_contributor($post){
    	$this->load->helper('security');
    	$data = array(
			'email' => $post['email'],
			'password' =>  do_hash($post['password'], 'md5'),
			'first_name' => $post['first'],
			'last_name' => $post['last'],
			'affiliation' => $post['affiliation'],
			'city' => $post['city'],
			'country' => $post['country'],
			'verified' => 0
		);

		$query = $this->db->get_where('contributors', array('email' => $post['email']));
		if($query->num_rows() == 0) :
			//Checks for duplicates
			$query = $this->db->insert('contributors', $data);
			return $this->db->insert_id();
		else :
			return false;
		endif;
    }
	
	function return_unverified_contributor(){
    	$query = $this->db->get_where('contributors', array('verfied' => 0));
		return ($query->num_rows() == 1 ) ? $query->row_array() : false;
    }
	
	function approve_contributor($pk_contributor_id){
    	$query = $this->db->where('pk_contributor_id' , $pk_contributor_id);
    	$query = $this->db->update('contributors', array('verified'=>'1'));
    }
	
	function delete_contributor($pk_contributor_id){
    	$query = $this->db->delete('contributors', array('pk_contributor_id'=>$pk_contributor_id));
    }

}