<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicles extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	
	
	/* RETURN FUNCTIONS */
	function return_vehicles($pk_vehicle_id = null){		
		if($pk_vehicle_id != null) :
			$query = $this->db->where('pk_vehicle_id', $pk_vehicle_id);
		endif;

		$query = $this->db->order_by('model', 'asc');
		$query = $this->db->get('vehicles');
		$vehicles = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $vehicle ) :
				$car['pk_vehicle_id'] = $vehicle['pk_vehicle_id'];
				$car['manufacturer'] = $this->manufacturers->return_vehicle_manufacturer($vehicle['fk_manufacturer_id']);
				$car['model'] = $vehicle['model'];
				$car['year'] = $vehicle['year'];
				$car['images'] = $this->images->return_vehicle_images($vehicle['pk_vehicle_id']);
				$car['components'] = $this->components->return_vehicle_components_indexed($vehicle['pk_vehicle_id']);
				$car['measurements'] = $this->measures->return_vehicle_measurements($vehicle['pk_vehicle_id']);
				$car['contributor'] = $this->contributors->return_contributor($vehicle['fk_contributor_id']);
				$car['view'] = $vehicle['view'];
				$car['verified'] = $vehicle['verified'];
				$car['reject'] = $vehicle['reject'];
				
				$vehicles[] = $car;
			endforeach;
			
			return $vehicles;
		endif;
	}
	
	function return_unverified_vehicles(){		

		$query = $this->db->where($array = array('view' => '0', 'reject' => '0'));
		$query = $this->db->order_by('model', 'asc');
		$query = $this->db->get('vehicles');
		$vehicles = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $vehicle ) :
				$car['pk_vehicle_id'] = $vehicle['pk_vehicle_id'];
				$car['manufacturer'] = $this->manufacturers->return_vehicle_manufacturer($vehicle['fk_manufacturer_id']);
				$car['model'] = $vehicle['model'];
				$car['year'] = $vehicle['year'];
				$car['images'] = $this->images->return_vehicle_images($vehicle['pk_vehicle_id']);
				$car['components'] = $this->components->return_vehicle_components_indexed($vehicle['pk_vehicle_id']);
				$car['measurements'] = $this->measures->return_vehicle_measurements($vehicle['pk_vehicle_id']);
				$car['contributor'] = $this->contributors->return_contributor($vehicle['fk_contributor_id']);
				
				$vehicles[] = $car;
			endforeach;
			
			return $vehicles;
		endif;
	}
	
	function return_rejected_vehicles(){		
		$query = $this->db->where($array = array('view' => '0', 'reject' => '1'));
		$query = $this->db->order_by('model', 'asc');
		$query = $this->db->get('vehicles');
		$vehicles = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $vehicle ) :
				$car['pk_vehicle_id'] = $vehicle['pk_vehicle_id'];
				$car['manufacturer'] = $this->manufacturers->return_vehicle_manufacturer($vehicle['fk_manufacturer_id']);
				$car['model'] = $vehicle['model'];
				$car['year'] = $vehicle['year'];
				$car['images'] = $this->images->return_vehicle_images($vehicle['pk_vehicle_id']);
				$car['components'] = $this->components->return_vehicle_components_indexed($vehicle['pk_vehicle_id']);
				$car['measurements'] = $this->measures->return_vehicle_measurements($vehicle['pk_vehicle_id']);
				$car['contributor'] = $this->contributors->return_contributor($vehicle['fk_contributor_id']);
				
				$vehicles[] = $car;
			endforeach;
			
			return $vehicles;
		endif;
	}
	
	/* ADD FUNCTIONS */
	function create_vehicle($post){		  
		$data = array(
			'fk_contributor_id' => $post['fk_contributor_id'],
			'fk_manufacturer_id' => $post['manufacturer'],
			'model' => ucwords($post['model']),
			'year' => $post['year'],
			'submitted' => Date('Y-m-d H:i:s'),
			'view' => 0,
			'agreement'=> 1,
			'verified' => 0,
			'reject' => 0
		);
		
		$query = $this->db->insert('vehicles', $data);
		$fk_vehicle_id = $this->db->insert_id();
		return $fk_vehicle_id;
	}


	/* ADMIN FUNCTIONS */	
	function awaiting_approval(){
		$query = $this->db->get_where('vehicles', array('view' => '0', 'reject' => 0));
		return $query->num_rows();
	}
	
    
    function approve($pk_vehicle_id, $fk_contributor_id){
    	$query = $this->db->where('pk_vehicle_id' , $pk_vehicle_id);
    	$query = $this->db->update('vehicles', array('view'=>'1','verified'=>'1','reject' => 0));

    	$query = $this->db->where('pk_contributor_id' , $fk_contributor_id);
    	$query = $this->db->update('contributors', array('verified'=>'1'));    	
    }
    
	function reject_vehicle($pk_vehicle_id, $fk_contributor_id){
    	$query = $this->db->where('pk_vehicle_id' , $pk_vehicle_id);
    	$query = $this->db->update('vehicles', array('reject'=>1));  	
    }
	
    function delete($pk_vehicle_id){
    	$this->load->model('model_edit');
		$this->load->model('model_readme');
    	//Deletes all files associated with the vehicle and remove them from db
    	$vehicles = $this->vehicles->return_vehicles($pk_vehicle_id);
		$readme = $this->model_readme->return_readme($pk_vehicle_id);
		//delete all physical files first then remove vehicle from db
		foreach( $vehicles as $vehicle){
		foreach($vehicle['measurements'] as $measurement){
			$this->model_edit->remove_measurement($pk_vehicle_id, $measurement['pk_measurement_id']);
		}
		foreach($vehicle['components'] as $component){
			$this->model_edit->remove_noise($pk_vehicle_id, $component['pk_component_id']);
		}
		foreach($vehicle['images'] as $image){
			$this->model_edit->remove_image($pk_vehicle_id, $image['pk_image_id']);
		}
			
		}
			$this->model_readme->delete_readme($readme['pk_desc_id']);
    	$query = $this->db->delete('vehicles', array('pk_vehicle_id'=>$pk_vehicle_id));
    }

}