<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_Upload_Validation extends CI_Model {
	//Model that contains a set of functions for validating files uploads
	
	function validate_images($required){
	//Validate images. Returns true if all tests passes. Return error notice if it fails any part of thet test
	$allowed_ext = array("jpg", "gif", "jpeg", "png");
	$error_list = array("No error", 
						"File Size exceed as defined in php.ini", 
						"File Size exceed as defined in HTML Form",
						"File that uploaded partially",
						"No file uploaded");
		if(isset($_FILES['image'])){
			//Check if the field exist
			$count = 0;
			$return_err_array = array();
			foreach($_FILES["image"]["error"] as $error){
				//Finding general error
				if($error > 0 && $error < 4 ){
					$return_err_array = "Error: ".$_FILES["image"]["name"][$count]." has ".$error_list[$error];
					return $return_err_array;
				}
				else if( $error != 0 && $error != 4){
					$return_err_array = "Error: ".$_FILES["image"]["name"][$count]." has error(s)";
					return $return_err_array;
				}
			}
			
			//Checking for specific errors
			if($required == TRUE){
				if($_FILES["image"]["error"][0] == UPLOAD_ERR_NO_FILE){
					//If this file is a required upload, first file is checked since if files are uploaded
					//at least the first file must not be empty
					return "An image is required for this submission. Please make sure to upload at least one image.";
				}
			}
			if($_FILES["image"]["error"][0] == UPLOAD_ERR_OK){
				$count = 0;
				foreach($_FILES["image"]["type"] as $type){
					//checks file extension
					$temp = explode(".", $_FILES["image"]["name"][$count]);
					$ext = end($temp);
					if(!in_array($ext,$allowed_ext)){
						return "Your uploaded file, ".$_FILES["image"]["name"][$count]. ", contains an illegal file extension. <br>
								Please make sure your image file type is one of: png, jpg, jpeg, gif only.";
					}
					$count++;
				}
				
				$count = 0;
				foreach($_FILES["image"]["size"] as $size){
					//size
					//Note: php.ini already limit file size to 2048 kB
					//so this part is only used if php.ini had different settings or 
					//specific file size is needed.
					$file_limit = 2048;
					if(($size/1024) > $file_limit){
						return "Your uploaded file, " .$_FILES["image"]["name"][$count]. " contains files larger than " .$file_limit. " kB.";
					}
					$count++;
				}
			}

		}
		else{
			//return false if the file field doest exist
			return FALSE;
		}
		//return true if there are no errors
		return TRUE;
	}
	
	function validate_noise($required){
		//Validate component files. Returns true if all tests passes. Return error notice if it fails any part of thet test
		$allowed_ext = array("xlsx","xls","zip","m","txt","rtf","doc");
		$error_list = array("No error", 
						"File Size exceed as defined in php.ini", 
						"File Size exceed as defined in HTML Form",
						"File that uploaded partially",
						"No file uploaded");
		if(isset($_FILES['component'])){
			//Check if the field exist
			$count = 0;
			$return_err_array = array();
			foreach($_FILES["component"]["error"] as $error){
				//Finding general error
				if($error > 0 && $error < 4 ){
					$return_err_array = "Error: ".$_FILES["component"]["name"][$count]." has ".$error_list[$error];
					return $return_err_array;
				}
				else if( $error != 0 && $error != 4){
					$return_err_array = "Error: ".$_FILES["component"]["name"][$count]." has error(s)";
					return $return_err_array;
				}
			}
			
			//Checking for specific errors
			if($required == TRUE){
				if($_FILES["component"]["error"][0] == UPLOAD_ERR_NO_FILE){
					//If this file is a required upload, first file is checked since if files are uploaded
					//at least the first file must not be empty
					return "A component file is required for this submission.";
				}
			}
			
			$count = 0;
			foreach($_FILES["component"]["type"] as $type){
				//checks file extension
				if($_FILES["component"]["error"][$count] == UPLOAD_ERR_OK){
					$temp = explode(".", $_FILES["component"]["name"][$count]);
					$ext = end($temp);
					if(!in_array($ext,$allowed_ext)){
						return "Your uploaded file, ".$_FILES["component"]["name"][$count]. ", contains an illegal file extension. <br>
								Please make sure your component file type is one of: xlsx, xls, zip, m, txt, rtf, doc only.";
					}
					$count++;
				}
			}
				
			$count = 0;
			foreach($_FILES["component"]["size"] as $size){
			//size
			//Note: php.ini already limit file size to 2048 kB
			//so this part is only used if php.ini had different settings or 
			//specific file size is needed.
				if($_FILES["component"]["error"][$count] == UPLOAD_ERR_OK){
					$file_limit = 2048;
					if(($size/1024) > $file_limit){
						return "Your uploaded file, " .$_FILES["component"]["name"][$count]. " contains files larger than " .$file_limit. " kB.";
					}
					$count++;
				}
			}

		}
		else{
			//return false if the file field doest exist
			return FALSE;
		}
		//return true if there are no errors
		return TRUE;
	}
	
	function validate_transfer($required){
		//Validate measurement files. Returns true if all tests passes. Return error notice if it fails any part of thet test
		$allowed_ext = array("xlsx","xls","zip","m","txt","rtf","doc");
		$error_list = array("No error", 
						"File Size exceed as defined in php.ini", 
						"File Size exceed as defined in HTML Form",
						"File that uploaded partially",
						"No file uploaded");
		if(isset($_FILES['measurement'])){
			//Check if the field exist
			$count = 0;
			$return_err_array = array();
			foreach($_FILES["measurement"]["error"] as $error){
				//Finding general error
				if($error > 0 && $error < 4 ){
					$return_err_array = "Error: ".$_FILES["measurement"]["name"][$count]." has ".$error_list[$error];
					return $return_err_array;
				}
				else if( $error != 0 && $error != 4){
					$return_err_array = "Error: ".$_FILES["measurement"]["name"][$count]." has error(s)";
					return $return_err_array;
				}
			}
			
			//Checking for specific errors
			if($required == TRUE){
				if($_FILES["measurement"]["error"][0] == UPLOAD_ERR_NO_FILE){
					//If this file is a required upload, first file is checked since if files are uploaded
					//at least the first file must not be empty
					return "A measurement file is required for this submission.";
				}
			}
			
			$count = 0;
			foreach($_FILES["measurement"]["type"] as $type){
				//checks file extension
				if($_FILES["measurement"]["error"][$count] == UPLOAD_ERR_OK){
					$temp = explode(".", $_FILES["measurement"]["name"][$count]);
					$ext = end($temp);
					if(!in_array($ext,$allowed_ext)){
						return "Your uploaded file, ".$_FILES["measurement"]["name"][$count]. ", contains an illegal file extension. <br>
								Please make sure your measurement file type is one of: xlsx, xls, zip, m, txt, rtf, doc only.";
					}
				}
				$count++;
			}
				
			$count = 0;
			foreach($_FILES["measurement"]["size"] as $size){
			//size
			//Note: php.ini already limit file size to 2048 kB
			//so this part is only used if php.ini had different settings or 
			//specific file size is needed.
				if($_FILES["measurement"]["error"][$count] == UPLOAD_ERR_OK){
					$file_limit = 2048;
					if(($size/1024) > $file_limit){
						return "Your uploaded file, " .$_FILES["measurement"]["name"][$count]. " contains files larger than " .$file_limit. " kB.";
					}
					$count++;
				}
			}
		}
		else{
			//return false if the file field doest exist
			return FALSE;
		}
		//return true if there are no errors
		return TRUE;
	}
	
	function validate_readme($required){
		//Validate readme files. Returns true if all tests passes. Return error notice if it fails any part of thet test
		$allowed_ext = array("txt","rtf","doc");
		$error_list = array("No error", 
						"File Size exceed as defined in php.ini", 
						"File Size exceed as defined in HTML Form",
						"File that uploaded partially",
						"No file uploaded");
		if(isset($_FILES['readme'])){
			//Check if the field exist
			$count = 0;
			$return_err_array = array();
			foreach($_FILES["readme"]["error"] as $error){
				//Finding general error
				if($error > 0 && $error < 4 ){
					$return_err_array = "Error: ".$_FILES["readme"]["name"][$count]." has ".$error_list[$error];
					return $return_err_array;
				}
				else if( $error != 0 && $error != 4){
					$return_err_array = "Error: ".$_FILES["readme"]["name"][$count]." has error(s)";
					return $return_err_array;
				}
			}
			
			//Checking for specific errors
			if($required == TRUE){
				if($_FILES["readme"]["error"] == UPLOAD_ERR_NO_FILE){
					//If this file is a required upload, first file is checked since if files are uploaded
					//at least the first file must not be empty
					return "A readme file is required for this submission.";
				}
			}
			if($_FILES["readme"]["error"] == UPLOAD_ERR_OK){


					//checks file extension
				$temp = explode(".", $_FILES["readme"]["name"]);
				$ext = end($temp);
				if(!in_array($ext,$allowed_ext)){
					return "Your uploaded file, ".$_FILES["readme"]["name"]. ", contains an illegal file extension. <br>
							Please make sure your readme file type is one of: txt, rtf, doc only.";
				}

				
				//size
				//Note: php.ini already limit file size to 2048 kB
				//so this part is only used if php.ini had different settings or 
				//specific file size is needed.
				$file_limit = 2048;
				if(($size/1024) > $file_limit){
					return "Your uploaded file, " .$_FILES["readme"]["name"]. " contains files larger than " .$file_limit. " kB.";
				}

			}

		}
		else{
			//return false if the file field doest exist
			return FALSE;
		}
		//return true if there are no errors
		return TRUE;
	}
	
}