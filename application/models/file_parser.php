<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_Parser extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    function images_parser(){
    	//creates array of uploaded images
	    foreach($_FILES['image'] as $key => $file) :
	        $i = 0;
	        foreach ($file as $item) :
	            $images[$i][$key] = $item;
	            $i++;
	        endforeach;
	    endforeach;
	
	    return $images;
    }

    function components_parser(){
    	//creates array of uploaded components
   	    foreach($_FILES['component'] as $key => $file) :
	        $i = 0;
	        foreach ($file as $item) :
	            $components[$i][$key] = $item;
	            $i++;
	        endforeach;
	    endforeach;
	
	    return $components;
    }
    
    function measurements_parser(){
    	//creates array of uploaded measurements
    	foreach($_FILES['measurement'] as $key => $file) :
    		$i = 0;
    		foreach ($file as $item) :
    			$measurements[$i][$key] = $item;
    			$i++;
    		endforeach;
    	endforeach;
    	
    	return $measurements;
    }
	
	function measurements_edit_parser($post){
    	//creates array of uploaded measurements
    	foreach($post['measurement'] as $key => $file) :
    		$i = 0;
    		foreach ($file as $item) :
    			$measurements[$i][$key] = $item;
    			$i++;
    		endforeach;
    	endforeach;
    	
    	return $measurements;
    }
    
}