<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class model_edit extends CI_Model {
	
	function __construct(){
        parent::__construct();
    }
    function get_all_info($id){
		//get all listed data from database table "contributors" using pk_contributor_id
		$query = $this->db->query("SELECT *
									FROM contributors
									WHERE pk_contributor_id = '$id'");
		
		$ret_info = array();
		$temp = $query->result_array();
		foreach($temp as $ret){
			$ret_info = $ret;
		}
		return $ret_info;
	}
	
	function get_all_infoAdmin($id){
		//get all data by id
		$query = $this->db->get_where('group', array('pk_group_id'=> $id));
		
		$ret_info = array();
		$temp = $query->result_array();
		foreach($temp as $ret){
			$ret_info = $ret;
		}
		return $ret_info;
	}
	
	
	function get_all_info_login($email){
		//get all data with email for login
		$query = $this->db->query("SELECT *
									FROM contributors
									WHERE email = '$email'");
		
		$ret_info = array();
		$temp = $query->result_array();
		foreach($temp as $ret){
			$ret_info = $ret;
		}
		return $ret_info;
	}
	
	function get_all_infoAdmin_login($email){
		//get all data with email for login
		$query = $this->db->get_where('group', array('email'=> $email));
		
		$ret_info = array();
		$temp = $query->result_array();
		foreach($temp as $ret){
			$ret_info = $ret;
		}
		return $ret_info;
	}
	
	function get_prime_key($email){
		//Get primary key(pk_contributor_id') from email in database table contributors
		//$this->db->select('pk_contributor_id');
		$query = $this->db->query("SELECT pk_contributor_id FROM contributors WHERE email = '$email'");
		
		return $query->result_object();
		
	}
	
	function get_prime_keyAdmin($email){
		//Get primary key(pk_group_id') from email in database table group
		$this->db->select('pk_group_id');
		$query = $this->db->get_where('group', array('email'=> $email));
		//$query = $this->db->query("SELECT pk_group_id FROM group WHERE email = '$email'");
		
		return $query->result_object();
		
	}
	
	function update_userinfo($data, $id){
		//Update userinfo fields in table "contributors" using pk_contributor_id to match records and data
		//from form
		foreach($data as $field=>$value){
			$data=array($field=>$value);
			if($value != ''){
				$this->db->update('contributors', $data, array('pk_contributor_id' => $id));
			}
			
		}
		//$this->db->update('contributors', $data, array('pk_contributor_id' => $id));
		//$this->db->where('pk_contributor_id', $id);
		//$this->db->update('contributors', $data); 
		
		
		return ($this->db->affected_rows()); // return whether update is successful
	}
	
	function update_userinfo_admin($data, $id){
		//Update userinfo fields in table "contributors" using pk_contributor_id to match records and data
		//from form
		$check = 'none';
		foreach($data as $field=>$value){
			$data=array($field=>$value);
			if($value != ''){
				$this->db->update('group', $data, array('pk_group_id' => $id));
				$check = $value;
			}
			
		}
		//$this->db->update('contributors', $data, array('pk_contributor_id' => $id));
		//$this->db->where('pk_contributor_id', $id);
		//$this->db->update('contributors', $data); 
		
		return $check;
		//return ($this->db->affected_rows()); // return whether update is successful
	}
	function search_vehicle_list($id){
		$query = $this->db->query("SELECT *
								   FROM vehicles
								   WHERE fk_contributor_id = '$id'");
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $vehicle ) :
				$car['pk_vehicle_id'] = $vehicle['pk_vehicle_id'];
				$car['manufacturer'] = $this->manufacturers->return_vehicle_manufacturer($vehicle['fk_manufacturer_id']);
				$car['model'] = $vehicle['model'];
				$car['year'] = $vehicle['year'];
				$car['images'] = $this->images->return_vehicle_images($vehicle['pk_vehicle_id']);
				$car['components'] = $this->components->return_vehicle_components_indexed($vehicle['pk_vehicle_id']);
				$car['measurements'] = $this->measures->return_vehicle_measurements($vehicle['pk_vehicle_id']);
				$car['contributor'] = $this->contributors->return_contributor($id);
				$car['view'] = $vehicle['view'];
				$car['verified'] = $vehicle['verified'];
				$car['reject'] = $vehicle['reject'];
				
				$vehicles[] = $car;
			endforeach;
			
			return $vehicles;
		else:
			return $vehicles=array();
		endif;
	}
	
	function isNoiseFile($vehicle_id,$file_id){
	 //Could have used 'url' element from $component in view_edit_vehicle ($component['url']) but database
	 //seem to not be refreshed while within the same function as the db manipulating function
	 //Therefore another function must be used to check if field is empty, same with isTransferFile
	 //Checks if noise file field is empty
		$query = $this->db->query("SELECT url
								   FROM   components
								   WHERE  pk_component_id = '$file_id'
							   	   AND    fk_vehicle_id = '$vehicle_id'");
		

		foreach($query->result() as $row){
			$temp['url'] = $row->url;
		}
		if(!empty($temp) && $temp['url'] == NULL){
			//FALSE means url field is empty
			return FALSE;
		}else{
			//True means url field is not empty
			return TRUE;
		}
	}

	function isTransferFile($vehicle_id,$file_id){
	 //Could have used 'url' element from $component in view_edit_vehicle ($component['url']) but database
	 //seem to not be refreshed while within the same function as the db manipulating function
	 //Therefore another function must be used to check if field is empty, same with isTransferFile
	 //Checks if noise file field is empty
		$query = $this->db->query("SELECT *
								   FROM   measurements
								   WHERE  pk_measurement_id = '$file_id'
							   	   AND    fk_vehicle_id = '$vehicle_id'");
		
/*
		foreach($query->result() as $row){
			$temp['url'] = $row->url;
		}*/
		$temp = $query->result();
		if(empty($temp)){
			//FALSE means url field is empty
			return FALSE;
		}else{
			//True means url field is not empty
			return TRUE;
		}
	}
	function isImageFile($vehicle_id,$file_id){
	 //Could have used 'url' element from $component in view_edit_vehicle ($component['url']) but database
	 //seem to not be refreshed while within the same function as the db manipulating function
	 //Therefore another function must be used to check if field is NULL, same with isTransferFile
	 //Checks if noise file field is null
		$query = $this->db->query("SELECT url
								   FROM   images
								   WHERE  pk_image_id = '$file_id'
							   	   AND    fk_vehicle_id = '$vehicle_id'");
		
		$temp = $query->result_array();
		if(empty($temp)){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function update_vehicle_info($post){
		$id = $post['id'];
		
		$data = array(
    		'fk_contributor_id' => $this->session->userdata('id'),
 			'fk_manufacturer_id' => $post['manufacturer'],
 			'model' => $post['model'],
			'year' => $post['year'],
			'view' => 0,
    		'agreement' => 1,
    		'verified' => 0,
    		'reject' => 0
    	);
		
		foreach($data as $field=>$value){
			$data=array($field=>$value);
			if($value != '' || is_numeric($value)){
					$this->db->update('vehicles', $data, array('pk_vehicle_id' => $id));
			}
		}
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else 
			return TRUE;
	}
	
	function return_manufacturer_id($manufacturer){
		$query = $this->db->query("SELECT pk_manufacturer_id
								   FROM manufacturers
								   WHERE  name = '$manufacturer'");
		
		$result = $query->result();
		$ret_id = 0;
		if(!empty($result)){
			foreach($result as $row){
				$ret_id = $row->pk_manufacturer_id;
			}
		}
		return $ret_id;
	}
	
	function remove_noise($vehicle_id,$file_id){
		$q = $this->db->query("SELECT url
							   FROM   components
							   WHERE  pk_component_id = '$file_id'
								   AND fk_vehicle_id = '$vehicle_id'");
		
		foreach($q->result() as $row){
			$path['url'] = $row->url;
			
		}
		if($path['url'] != ''){
			if( file_exists("C:/wamp/www/ivplc".$path['url'])){
	    		if(unlink("C:/wamp/www/ivplc".$path['url'])){
	    			$query = $this->db->query("UPDATE components
									   SET	  url = ''
									   WHERE  pk_component_id = '$file_id'
									   AND fk_vehicle_id = '$vehicle_id'");
					return ($this->db->affected_rows()); // return whether update is successful
	    		}
				else {
					return FALSE;
				}
			}
			else {
				return FALSE;
			}
		}
	}
	
	function remove_noise_no_db($vehicle_id,$file_id){
		$q = $this->db->query("SELECT url
							   FROM   components
							   WHERE  pk_component_id = '$file_id'
								   AND fk_vehicle_id = '$vehicle_id'");
		
		foreach($q->result() as $row){
			$path['url'] = $row->url;
			
		}
		if($path['url'] != ''){
			if( file_exists("C:/wamp/www/ivplc".$path['url'])){
	    		if(unlink("C:/wamp/www/ivplc".$path['url'])){
	    			return TRUE;
	    		}
				else {
					return FALSE;
				}
			}
			else {
				return FALSE;
			}
		}
	}
	
	function remove_measurement($vehicle_id,$file_id){
		$q = $this->db->query("SELECT url
							   FROM   measurements
							   WHERE  pk_measurement_id = '$file_id'
								  AND fk_vehicle_id = '$vehicle_id'");
		
		foreach($q->result() as $row){
			$path['url'] = $row->url;
			
		}
		if( file_exists("C:/wamp/www/ivplc".$path['url'])){
    		if(unlink("C:/wamp/www/ivplc".$path['url'])){
				return TRUE; // return whether update is successful
    		}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	
	function remove_image($vehicle_id, $file_id){
		$q = $this->db->query("SELECT url
							   FROM   images
							   WHERE  pk_image_id = '$file_id'
							   AND fk_vehicle_id = '$vehicle_id'");
		foreach($q->result() as $row){
			$path['url'] = $row->url;
			
		}
		if( file_exists("C:/wamp/www/ivplc".$path['url'])){
    		if(unlink("C:/wamp/www/ivplc".$path['url'])){
				return TRUE; // return whether update is successful
    		}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	
	function remove_vehicles($post){
		$id = $post['del_vehicle'];
		
		foreach( $id as $rm){
			$this->vehicles->delete($rm);
		}
								   
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function search_manufacturer($id){
		$query = $this->db->query("SELECT name
								   FROM manufacturers
								   WHERE pk_manufacturer_id = '$id'");
								   
		return $query->result();
	}
	
	function search_publications($id){
		//Returns an array of data of $id publication
		
		$query = $this->db->query("SELECT *
								   FROM   publications
								   WHERE  pk_pub_id = '$id'");

		//$ret_pubs = $query->result_array();
		
		foreach($query->result_array() as $ret){
			$ret_pubs = $ret;
		}
		$ret_pubs['authors'] = $this->model_edit->search_authors($id);

		
		return $ret_pubs;
	}
	
	function search_authors($id){
		//Return an array of data for $id publication
		$ret_authors = array();
		$query = $this->db->query("SELECT *
								   FROM   authors
								   WHERE  fk_pub_id = '$id'
								   AND verified = 1
								   AND reject = 0
								   ORDER BY pk_author_id");
		
		$temp = $query->result_array();
		for($i = 0; $i < count($temp);$i++){
			$ret_authors[$i]['name']= $temp[$i]['first_name'].' '.$temp[$i]['last_name'];
			$ret_authors[$i]['author_id'] = $temp[$i]['pk_author_id'];
			$ret_authors[$i]['pub_id'] = $temp[$i]['fk_pub_id'];
		}
		/*
		foreach($query->result() as $row){
			$ret_authors['name'] = $row->first_name.' '.$row->last_name;
			$ret_authors['pub_id'] = $row->fk_pub_id;
		 * $temp[$i]['first_name'].' '.$temp['last_name']
		}
			*/
		return $ret_authors;
	}
	
	function update_publication($post, $id){
		$data = array(
    		'fk_contributor_id' => $post['fk_contributor_id'],
    		'title' => ucwords($post['title']),
    		'affiliation' => ucwords($post['affiliation']),
    		'date' => $post['date'],
    		'url' => $post['url'],
    		'view' => 0,
    		'agreement' => 1,
    		'verified' => 0,
    		'reject' => 0
    	);
		
   	
		foreach($data as $field=>$value){
			$data=array($field=>$value);
			if($value != '' || is_numeric($value)){
				if($field == 'url'){
					$this->db->update('publications', $data, array('pk_pub_id' => $id));
					$this->db->update('publications', $temp=array('submitted' => Date('Y-m-d H:i:s')), array('pk_pub_id' => $id));
				}
				else{
					$this->db->update('publications', $data, array('pk_pub_id' => $id));
				}
			}
			
		}
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else 
			return TRUE;
		//return ($this->db->affected_rows()); // return whether update is successful
	}
	
	function remove_publications($post){
		$id = $post['del_pub'];
		
		foreach( $id as $rm){
			$query = $this->db->query("DELETE FROM publications
								   WHERE pk_pub_id = '$rm'");
		}
								   
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	function remove_author($post){
		
		$id = $post['rm_authors'];
		
		foreach( $id as $rm){
			$query = $this->db->query("DELETE FROM authors
								   WHERE pk_author_id = '$rm'");
		}
								   
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else {
			return TRUE;
		}				   
	}
	
	function countAuthors($id){
		//returns the count of authors by publication
		
		$query = $this->db->query("SELECT *
								   FROM   authors
								   WHERE  fk_pub_id = '$id'");
		$authors = $query->result_array();
		
		return count($authors);
		/*
		foreach( $authors as $author){
			foreach($post['rm_authors'] as $removed){
				if( $author['pk_author_id'] == $removed){
					$author['first_name'] = 'BLANK';
				}
			}
		}			
		
		foreach($authors as $author) {
			if($author['first_name'] != 'BLANK'){
				return TRUE;
			}
		}
		
		
		return FALSE;
		 * 
		 */
	}
	
	function missing_measurement($vehicle, $num_comp){
		//Function that returns an array of remaining measurements
		$ret_m = array();
		$ret = array();
		$comp_id = 0;
		$count = 0;
		//$msg = array();

		foreach($vehicle['components'] as $component){
			//finding component id
			if($count == $num_comp-1){
				$comp_id = $component['pk_component_id'];
				break;
			}
			$count++;
		}

		foreach($vehicle['components'] as $component){
			//Loops that finds all the candidate measurements for that certain component
			$ret_m[] = $component['pk_component_id'];	
		}
		
		array_splice($ret_m,0,($num_comp));
		
		for($i=0; $i < count($ret_m);$i++){
			foreach($vehicle['measurements'] as $measurements){
				//$msg = $comp_id;
				if($measurements['fk_componentA_id'] == $comp_id
				&& $measurements['fk_componentB_id'] == $ret_m[$i]){
					
					//unset($ret_m[$i]);
					//$ret[] = $measurements['fk_componentB_id'];
					array_splice($ret_m,$i,1);
					$i--;
					break;
				}
			}
		}
		return $ret_m;
	}
	
	function return_comp_name($id){
		$query = $this->db->query("SELECT name
						  FROM   components
						  WHERE  pk_component_id = '$id'");
						  
		$ret = array();
		foreach($query->result() as $row){
			$ret = $row->name;
		}
		
		return $ret;
	}
}
