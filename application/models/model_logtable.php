<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_logtable extends CI_Model {

    function __construct(){
        parent::__construct();
    }
	
	function add_log($type, $pk_contributor_id, $data_id, $source_table ){
		//add an entry to the log table
		//the log table will only cantain 50 entries and the oldest one will be replaced if exceed 50 entries
		$table_count = $this->db->query("SELECT COUNT(*)
								   FROM log_table");
		
		if($table_count >= 100 ){
			$query = $this->db->query("DELETE *
									   FROM log_table
									   WHERE MIN(pk_log_id)");						   
		}
		
		
		switch($type){
			//1. User created
			//2. Vehicle Submitted
			//3. Publication Submitted
			//4. Content Replaced (FILE)
			//5. Content Added (FILE)
			//6. Deleted Content (FILE)
			//7. UPDATED VEHICLE INFO
			//8. UPDATED PUBLICATION INFO
			//9. UPDATED USER INFO
			//10. UPDATED ADMIN USER INFO
			//11. ADMIN APPROVE
			//12. ADMIN REJECT
			//13. ADMIN DELETE
			//14. COMPONENT ADDED (NOT FILE)
			//15. COMPONENT DELETED (NOT FILE)
			
			case 1:
				//User Created	
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => NULL,
						"activity_type" => "USER_CREATE",
						"source_table" => "contributors",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
			case 2:
				//Vehicle Submitted
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => NULL,
						"activity_type" => "VEHICLE_SUBMITTED",
						"source_table" => "vehicles",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 3:
				//Publication Submitted
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => NULL,
						"activity_type" => "PUB_SUBMITTED",
						"source_table" => "publications",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 4:
				//Content Replaced
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => $data_id,
						"activity_type" => "REPLACED",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 5:
				//Content Added
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => $data_id,
						"activity_type" => "ADDED",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 6:
				//Deleted Content
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => $data_id,
						"activity_type" => "DELETED",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 7:
				//UPDATED VEHICLE INFO
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => $data_id,
						"activity_type" => "UPDATE_VEHICLE_INFO",
						"source_table" => "vehicles",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 8:
				//UPDATED PUBLICATION INFO
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => $data_id,
						"activity_type" => "UPDATE_PUB_INFO",
						"source_table" => "publications",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 9:
				//UPDATED USER INFO
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => NULL,
						"activity_type" => "UPDATE_USER_INFO",
						"source_table" => "contributors",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 10:
				//UPDATED ADMIN USER INFO
				$data = array(
						"fk_contributor_id" => $pk_contributor_id,
						"fk_source_id" => NULL,
						"activity_type" => "UPDATE_ADMIN_INFO",
						"source_table" => "group",
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 11:
				//ADMIN APPROVE
				$data = array(
						"fk_contributor_id" => $pk_contributor_id, //$pk_group_id in this case
						"fk_source_id" => $data_id,
						"activity_type" => "ADMIN_APPROVE",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 12:
				//ADMIN REJECT
				$data = array(
						"fk_contributor_id" => $pk_contributor_id, //$pk_group_id in this case
						"fk_source_id" => $data_id,
						"activity_type" => "ADMIN_REJECT",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
				
				case 13:
				//ADMIN DELETE
				$data = array(
						"fk_contributor_id" => $pk_contributor_id, //$pk_group_id in this case
						"fk_source_id" => $data_id,
						"activity_type" => "ADMIN_DELETE",
						"source_table" => $source_table,
						"date_time" => Date('Y-m-d H:i:s')
						);
				
				$query = $this->db->insert("log_table", $data);
				break;
		}
				
		return TRUE;
	}
	
	function delete_all_logs(){
		//delete all logs on the table
		$query = $this->db->query("DELETE *
								   FROM log_table");
								   	
	}
	
	function delete_log($pk_log_id){
		//delete a selected log
		$query = $this->db->query("DELETE *
								   FROM log_table
								   WHERE pk_log_id = '$pk_log_id'");
	}
	
	function return_single_log($pk_log_id){
		$query = $this->db->query("SELECT *
								   FROM log_table
								   WHERE pk_log_id = '$pk_log_id'");
		$ret_log = array();				   
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $log ) :
				$ret_log = $log;
			endforeach;
			
			return $ret_log;
		else:
			return FALSE;
		endif;
	}
	
	function return_log($asc, $limit, $offset){
		if($asc == 'asc'){
			
			$this->db->order_by('date_time','asc');
			$query = $this->db->get('log_table',$limit,$offset);

		}
		else{
			$this->db->order_by('date_time','desc');
			$query = $this->db->get('log_table',$limit,$offset);
		}
		
		$ret_log = array();				   
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $log ):
				$ret_log[] = $log;
			endforeach;
			
			return $ret_log;
		else:
			return FALSE;
		endif;
	}
	
	function return_log_user($asc, $limit, $offset){
		if( $asc == 'asc'){
			$this->db->order_by('fk_contributor_id','asc');
			$query = $this->db->get('log_table',$limit,$offset);
		}else{
			$this->db->order_by('fk_contributor_id','desc');
			$query = $this->db->get('log_table',$limit,$offset);
		}
		
		$ret_log = array();				   
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $log ) :
				$ret_log[] = $log;
			endforeach;
			
			return $ret_log;
		else:
			return FALSE;
		endif;
	}
	
	function return_log_type($asc, $limit, $offset){
		if($asc == 'asc'){
			$this->db->order_by('activity_type','asc');
			$query = $this->db->get('log_table',$limit,$offset);
		}
		else{
			$this->db->order_by('activity_type','desc');
			$query = $this->db->get('log_table',$limit,$offset);
		}
		$ret_log = array();				   
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $log ) :
				$ret_log[] = $log;
			endforeach;
			
			return $ret_log;
		else:
			return FALSE;
		endif;
	}
	
	function total_rows(){
		return $this->db->get('log_table')->num_rows();
	}
	
}