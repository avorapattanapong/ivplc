<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Measures extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    /* CREATE MEAUSREMENTS*/
    function upload_measurements($fk_vehicle_id, $measurements, $components){
		$config = array(
			'upload_path' => './uploads/transfer_functions/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $measurements;
	    $measurementCount = 0;

		for($i = 0; $i < count($components); $i++) :
			for($j = $i + 1; $j < count($components); $j++) :
				$config['file_name'] = $fk_vehicle_id . '_transfer_'. $components[$i]['pk_component_id'] . '_' . $components[$j]['pk_component_id'];
				$this->upload->initialize($config);
				
				if($_FILES[$measurementCount]['error'] != 4) :
					$this->upload->do_upload($measurementCount);
				
					$measurementCount++;
					
					$upload_data = $this->upload->data();
					$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
				else :
					$relative_url = null;
					$measurementCount++;
				endif;
				
				$data = array(
					'fk_vehicle_id' => $fk_vehicle_id,
					'fk_componentA_id' => $components[$i]['pk_component_id'],
					'fk_componentB_id' => $components[$j]['pk_component_id'],
					'url' => str_replace('ivplc', '', $relative_url)
				);
				
				$query = $this->db->insert('measurements', $data);
			endfor;
		endfor;
	}
	
	function upload_edit_measurements($fk_vehicle_id, $measurements, $compA, $compB){
		$config = array(
			'upload_path' => './uploads/transfer_functions/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
		$check = 'none';
	    $_FILES = $measurements;
		//$check = $_FILES["measurement"]['name'][0];
	   // $measurementCount = 0;
		
		for($i=0; $i < count($measurements); $i++){
			$config['file_name'] = $fk_vehicle_id . '_transfer_'. $compA[$i].'_'.$compB[$i];
			$this->upload->initialize($config);
			
			if($_FILES["measurement"]["error"][$i] != 4) :
				$this->upload->do_upload($i);
				$check = $this->upload->display_errors();
				//$measurementCount++;
					
				$upload_data = $this->upload->data();
				$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
				
				$data = array(
					'fk_vehicle_id' => $fk_vehicle_id,
					'fk_componentA_id' => $compA[$i],
					'fk_componentB_id' => $compB[$i],
					'url' => str_replace('ivplc', '', $relative_url),
					'verified' => 0,
					'reject' => 0
				);
				
								
				$query = $this->db->insert('measurements', $data);
				$msg = $this->db->_error_message();
				if(!empty($msg)){
					return FALSE;
				}
			 
				endif;
				
		}
		return TRUE;
	}

	function upload_replace_measurements($fk_vehicle_id, $measurements, $compA, $compB, $pk_measurement_id){
		$config = array(
			'upload_path' => './uploads/transfer_functions/',
			'allowed_types' => 'xlsx|xls|zip|m|txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
		$check = 'none';
	    $_FILES = $measurements;
		
		for($i=0; $i < count($measurements); $i++){
			$config['file_name'] = $fk_vehicle_id . '_transfer_'. $compA.'_'.$compB;
			$this->upload->initialize($config);
			

			$this->upload->do_upload($i);
			//$measurementCount++;
				
			$upload_data = $this->upload->data();
			$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
			
			$data = array(
				'url' => str_replace('ivplc', '', $relative_url),
				//update url incase the file extension is different
				'verified' => 0,
				'reject' => 0
			);
				
			$this->db->where('pk_measurement_id', $pk_measurement_id);		
			$query = $this->db->update('measurements', $data);
			$msg = $this->db->_error_message();
			if(!empty($msg)){
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/* RETURN MEASUREMENTS */
    function return_vehicle_measurements($fk_vehicle_id){
		$query = $this->db->get_where('measurements', array('fk_vehicle_id'=>$fk_vehicle_id));
		$measurements = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $measurement ) :
				$measurements[$measurement['pk_measurement_id']] = $measurement;
			endforeach;
			
			return $measurements;
		endif;
    }
	
	function return_measurement($pk_measurement_id){
		$query = $this->db->get_where('measurements', array('pk_measurement_id'=> $pk_measurement_id));
		$measurements = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $measurement ) :
				$measurements[] = $measurement;
			endforeach;
			
			return $measurements;
		endif;
    }
	
	function return_unverified_vehicle_measurements(){
		$query = $this->db->get_where('measurements', array('verified'=>0, 'reject' => 0));
		$measurements = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $measurement ) :
				$measurements[$measurement['pk_measurement_id']] = $measurement;
			endforeach;
			
			return $measurements;
		endif;
    }

	function return_rejected_vehicle_measurements(){
		$query = $this->db->get_where('measurements', array('verified'=>0, 'reject' => 1));
		$measurements = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $measurement ) :
				$measurements[$measurement['pk_measurement_id']] = $measurement;
			endforeach;
			
			return $measurements;
		endif;
    }
	
	function approve_measurement($pk_measurement_id){		
		$query = $this->db->where('pk_measurement_id' , $pk_measurement_id);
    	$query = $this->db->update('measurements', array('verified'=>'1'));
	}
	
	function reject_measurement($pk_measurement_id){		
		$query = $this->db->where('pk_measurement_id' , $pk_measurement_id);
    	$query = $this->db->update('measurements', array('reject'=> 1));
	}
	
	function delete_measurement($pk_measurement_id){
    	$query = $this->db->delete('measurements', array('pk_measurement_id'=>$pk_measurement_id));
    }
	
}