<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publications extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    /* RETURN FUNCTIONS */
	function return_publications(){
		$query = $this->db->get_where('publications', array('view' => '1'));
		$publications = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $pub ) :
				$pub['contributor'] = $this->contributors->return_contributor($pub['fk_contributor_id']);
				$pub['authors'] = $this->return_authors($pub['pk_pub_id']);
				$publications[] = $pub;
			endforeach;
			
			return $publications;
		endif;
    }
	
	function return_publications_withID($pk_pub_id){
		$query = $this->db->get_where('publications', array('pk_pub_id' => $pk_pub_id));
		$publications = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $pub ) :
				$pub['contributor'] = $this->contributors->return_contributor($pub['fk_contributor_id']);
				$pub['authors'] = $this->return_authors($pub['pk_pub_id']);
				$publications[] = $pub;
			endforeach;
			
			return $publications;
		endif;
    }
    
    function return_authors($pub_id){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('fk_pub_id' => $pub_id, 'verified' => 1, 'reject' => 0));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author['first_name'] . '. ' . $author['last_name'];
			endforeach;
			
			return $authors;
		endif;
    }
	
	function return_verified_authors($pub_id){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('fk_pub_id' => $pub_id, 'verified' => '1', 'reject'=>'0'));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author['first_name'] . '. ' . $author['last_name'];
			endforeach;
			
			return $authors;
		endif;
    }
	
	function return_unverified_pub_authors($pub_id){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('fk_pub_id' => $pub_id, 'verified' => 0, 'reject'=>0));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author;
			endforeach;
			
			return $authors;
		endif;
    }
	
	function return_reject_pub_authors($pub_id){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('fk_pub_id' => $pub_id, 'verified' => 0, 'reject'=>1));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author;
			endforeach;
			
			return $authors;
		endif;
    }
	
	function return_unverified_authors(){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('verified' => 0, 'reject' => 0 ));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author;
			endforeach;
			
			return $authors;
		endif;
    }
	
	function return_rejected_authors(){
    	$query = $this->db->order_by('pk_author_id', 'asc');
    	$query = $this->db->get_where('authors', array('verified' => 0, 'reject' => 1 ));    	
    	$authors = array();
    	
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $author ) :
				$authors[] = $author;
			endforeach;
			
			return $authors;
		endif;
    }

	function approve_author($pk_author_id){		
		$query = $this->db->where('pk_author_id' , $pk_author_id);
    	$query = $this->db->update('authors', array('verified'=>'1'));
	}
	
	function reject_author($pk_author_id){		
		$query = $this->db->where('pk_author_id' , $pk_author_id);
    	$query = $this->db->update('authors', array('reject'=>1));
	}
	
	function delete_author($pk_author_id){
    	$query = $this->db->delete('authors', array('pk_author_id'=>$pk_author_id));
    }
    
    function return_publications_awaiting_approval(){
    	$query = $this->db->get_where('publications', array('view' => '0', 'reject' => 0));
		$publications = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $pub ) :
				$pub['contributor'] = $this->contributors->return_contributor($pub['fk_contributor_id']);
				$pub['authors'] = $this->return_authors($pub['pk_pub_id']);
				$publications[] = $pub;
			endforeach;
			
			return $publications;
		else :
			return false;
		endif;
    }
	
	function return_publications_rejected(){
    	$query = $this->db->get_where('publications', array('view' => '0', 'reject' => 1));
		$publications = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $pub ) :
				$pub['contributor'] = $this->contributors->return_contributor($pub['fk_contributor_id']);
				$pub['authors'] = $this->return_authors($pub['pk_pub_id']);
				$publications[] = $pub;
			endforeach;
			
			return $publications;
		else :
			return false;
		endif;
    }
    
	
	/* ADD FUNCTIONS */
    function create_publication($post){
    	$data = array(
    		'fk_contributor_id' => $post['fk_contributor_id'],
    		'title' => ucwords($post['title']),
    		'affiliation' => ucwords($post['affiliation']),
    		'date' => $post['date'],
    		'url' => $post['url'],
    		'submitted' => Date('Y-m-d H:i:s'),
    		'view' => 0,
			'agreement'=> 1,
			'verified' => 0,
			'reject' => 0
    	);
    	
    	$query = $this->db->insert('publications', $data);
    	$fk_pub_id = $this->db->insert_id();
		return $fk_pub_id;
    }
    
    function create_authors($post, $fk_pub_id){
    	$first_name = $post['author_first'];
    	$last_name = $post['author_last'];
    	
		for($i = 0; $i < count($first_name); $i++) :
		    $data = array(
	    		'fk_pub_id' => $fk_pub_id,
	    		'first_name' => $first_name[$i],
	    		'last_name' => $last_name[$i]
	    	);
	    	
	    	$query = $this->db->insert('authors', $data);
		endfor;
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else 
			return TRUE;
    }
	
	function create_edit_authors($post, $fk_pub_id){
    	$first_name = $post['author_first'];
    	$last_name = $post['author_last'];
    	
		for($i = 0; $i < count($first_name); $i++) :
		    $data = array(
	    		'fk_pub_id' => $fk_pub_id,
	    		'first_name' => $first_name[$i],
	    		'last_name' => $last_name[$i],
	    		'verified' => 0,
	    		'reject' => 0
	    	);
	    	
	    	$query = $this->db->insert('authors', $data);
		endfor;
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else 
			return TRUE;
    }
	
	
	/* ADMIN FUNCTIONS */
    function awaiting_approval(){
    	$query = $this->db->get_where('publications', array('view' => '0'));
		return $query->num_rows();
    }
    
    function approve($pk_pub_id){
    	$query = $this->db->where('pk_pub_id' , $pk_pub_id);
    	$query = $this->db->update('publications', array('view'=>1));
		
		$query = $this->db->where('pk_pub_id' , $pk_pub_id);
    	$query = $this->db->update('publications', array('verified'=>1));
    }
	
	function reject_pub($pk_pub_id){
    	$query = $this->db->where('pk_pub_id' , $pk_pub_id);
    	$query = $this->db->update('publications', array('reject'=> 1 ));
    }
    
    function delete($pk_pub_id){
    	$query = $this->db->delete('publications', array('pk_pub_id'=>$pk_pub_id));
    }

}