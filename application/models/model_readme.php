<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_Readme extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    /* CREATE README*/
    function upload_readme($fk_vehicle_id, $readme){
		$config = array(
			'upload_path' => './uploads/readme_files/',
			'allowed_types' => 'txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $readme;
	    
		$config['file_name'] = $fk_vehicle_id . '_readme';
		$this->upload->initialize($config); 
		$this->upload->do_upload("readme");
		
		$upload_data = $this->upload->data();
		$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
		
		$data = array(
			'fk_vehicle_id' => $fk_vehicle_id,
			'url' => str_replace('ivplc', '', $relative_url),
			'verified' => 1,
			'reject' => 0
		);
			
		$query = $this->db->insert('vehicle_description', $data);
		
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else{ 
			return TRUE;
		}

	}
	
	function upload_edit_readme($fk_vehicle_id, $pk_desc_id, $readme){
		$config = array(
			'upload_path' => './uploads/readme_files/',
			'allowed_types' => 'txt|rtf|doc',
			'max_size' => '2048',
			'overwrite' => true
		);
	
	    $_FILES = $readme;
	    
		$config['file_name'] = $fk_vehicle_id . '_readme';
		$this->upload->initialize($config); 
		$this->upload->do_upload('readme');
		$upload_data = $this->upload->data();
		
		$relative_url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $upload_data['full_path']);
		
		$data = array(
			'verified' => 0,
			'reject' => 0
		);
		$this->db->where('pk_desc_id', $pk_desc_id);
		$query = $this->db->update('vehicle_description', $data);
		
		$msg = $this->db->_error_message();
		if(!empty($msg)){
			return FALSE;
		}
		else{ 
			return TRUE;
		}
}
	
	/* RETURN README FILE */
    function return_readme($fk_vehicle_id){
		$query = $this->db->get_where('vehicle_description', array('fk_vehicle_id'=>$fk_vehicle_id));
		$readme = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $txt ) :
				$readme = $txt;
			endforeach;
		endif;	
			return $readme;
		
    }
	
	function return_readme_by_id($pk_desc_id){
		$query = $this->db->get_where('vehicle_description', array('pk_desc_id'=>$pk_desc_id));
		$readme = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $txt ) :
				$readme = $txt;
			endforeach;
		endif;	
			return $readme;
		
    }

	function check_verify_readme($pk_desc_id){
		//For functions that doesn't reload the page, sometimes the database is not requeried and 
		//thus some contents are not updated
		//This function requeries the db in view
		$query = $this->db->query("SELECT verified, reject
								   FROM   vehicle_description
								   WHERE  pk_desc_id = $pk_desc_id");
		$ret = array();
		if( $query->num_rows() > 0 ){
			foreach($query->result() as $field){
				$ret['verified'] = $field->verified;
				$ret['reject'] = $field->reject;
			}
		}
		return $ret;
	}
	function return_unverified_readme(){
		//returns an array of any vehicle description file that is unverified
		$query = $this->db->get_where('vehicle_description', array('verified'=> 0, 'reject' => 0));
		$readme = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $txt ) :
				$readme[] = $txt;
			endforeach;
		endif;	
			return $readme;
		
    }
	
	function return_rejected_readme(){
		//returns an array of any vehicle description file that is unverified
		$query = $this->db->get_where('vehicle_description', array('verified'=> 0, 'reject' => 1));
		$readme = array();
		
		if( $query->num_rows() > 0 ) :   
			foreach( $query->result_array() as $txt ) :
				$readme[] = $txt;
			endforeach;
		endif;	
			return $readme;
		
    }

	function approve_readme($pk_desc_id){		
		$query = $this->db->where('pk_desc_id' , $pk_desc_id);
    	$query = $this->db->update('vehicle_description', array('verified'=>'1', 'reject' => 0 ));
		//reject 0 means not rejected while reject 1 = has been rejected by the admin
	}
	
	function reject_readme($pk_desc_id){		
		$query = $this->db->where('pk_desc_id' , $pk_desc_id);
    	$query = $this->db->update('vehicle_description', array('reject'=> 1 ));
	}
	
	function delete_readme($pk_desc_id){
		//used when deleting vehicle only. Not in admin rejections
		$q = $this->db->query("SELECT url
							   FROM   vehicle_description
							   WHERE  pk_desc_id = '$pk_desc_id'");
		foreach($q->result() as $row){
			$path['url'] = $row->url;
			
		}
		if( file_exists("C:/wamp/www/ivplc".$path['url'])){
    		if(unlink("C:/wamp/www/ivplc".$path['url'])){
    			$query = $this->db->query("DELETE FROM vehicle_description
							   				WHERE  pk_desc_id = '$pk_desc_id'");
				return ($this->db->affected_rows()); // return whether update is successful
    		}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	
}